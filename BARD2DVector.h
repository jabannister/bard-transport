//
//  BARD2DVector.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/5/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BARD2DVector : NSObject

@property (nonatomic) CLLocationDegrees x;
@property (nonatomic) CLLocationDegrees y;

- (id) init;
- (id) initWithX:(double)x andY: (double)y;
- (void) print;
- (void) plus: (BARD2DVector*) addee;
- (void) minus: (BARD2DVector*) subtractee;
- (void) scale: (double) factor;
- (void) dot: (BARD2DVector*) vector;
- (BARD2DVector*) plusCopy: (BARD2DVector*)addee;
- (BARD2DVector*) minusCopy: (BARD2DVector*)subtractee;
- (BARD2DVector*) scaleCopy: (double) factor;
- (BARD2DVector*) dotCopy: (BARD2DVector*) vector;







@end
