//
//  BARDCanvas.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/19/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface BARDCanvas : UIView

@property (strong) NSMutableDictionary* paths;
@property (strong) NSMutableArray* keys;
@property (strong) NSMutableDictionary* strokeAndFillColorPairs;



- (void) addPath: (UIBezierPath*) path withKey: (NSString*) key strokeColor: (UIColor*) strokeColor fillColor: (UIColor*) fillColor;
- (void) addPath: (UIBezierPath*) path withKey: (NSString*) key strokeHex: (NSString*) strokeHex fillHex: (NSString*) fillHex;
- (void) removeFromCanvasPathWithKey: (NSString*) key;
- (void) clearCanvas;


@end
