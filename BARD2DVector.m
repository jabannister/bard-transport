//
//  BARD2DVector.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/5/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARD2DVector.h"

@implementation BARD2DVector

- (id) init
{
    self = [super init];
    
    if(self) {
        self.x = 0;
        self.y = 0;
        return self;
    }
    return nil;
}

- (id) initWithX:(double)x andY:(double)y
{
    self = [super init];
    
    if(self) {
        self.x = x;
        self.y = y;
        return self;
    }
    return nil;
}

- (void) print
{
    NSLog(@"(%f, %f)", self.x, self.y);
}

- (void) plus:(BARD2DVector *)addee
{
    self.x = self.x + addee.x;
    self.y = self.y + addee.y;
}

- (void) minus:(BARD2DVector *)subtractee
{
    self.x = self.x - subtractee.x;
    self.y = self.y - subtractee.y;
}

- (void) scale:(double)factor
{
    self. x = self.x * factor;
    self.y = self.y * factor;
}

- (void) dot:(BARD2DVector *)vector
{
    self.x = self.x * vector.x;
    self.y = self.y * vector.y;
}



- (BARD2DVector*) plusCopy:(BARD2DVector *)addee
{
    BARD2DVector* output = [[BARD2DVector alloc] init];
    output.x = self.x + addee.x;
    output.y = self.y + addee.y;
    return output;
}

- (BARD2DVector*) minusCopy:(BARD2DVector *)subtractee
{
    BARD2DVector* output = [[BARD2DVector alloc] init];
    output.x = self.x - subtractee.x;
    output.y = self.y - subtractee.y;
    return output;
}

- (BARD2DVector*) scaleCopy:(double)factor
{
    BARD2DVector* output = [[BARD2DVector alloc] init];
    output.x = self.x * factor;
    output.y = self.y * factor;
    return output;
}

- (BARD2DVector*) dotCopy:(BARD2DVector *)vector
{
    BARD2DVector* output = [[BARD2DVector alloc] init];
    output.x = self.x * vector.x;
    output.y = self.y * vector.y;
    return output;
}

@end
