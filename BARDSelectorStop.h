//
//  BARDSelectorStop.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 11/15/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BARDShuttleStop.h"
#import "BARDCanvas.h"

@interface BARDSelectorStop : UIView


@property (strong, nonatomic) BARDShuttleStop *stop;

@property (nonatomic) BOOL selected;
@property (nonatomic) BOOL standby;
@property (nonatomic) BOOL fullyFilled;

@property (nonatomic) int index;

@property (strong, nonatomic) UIImage *icon;
@property (strong, nonatomic) UIImageView *iconView;
@property (nonatomic) double iconInset;

@property (strong, nonatomic) UIView *selectionFluid;

@property (strong, nonatomic) UIView *flagMask;
@property (strong, nonatomic) UIImage *flagImage;
@property (strong, nonatomic) UIImageView *flagView;
@property (nonatomic) double flagAspectRatio;


@property (strong, nonatomic) IBOutlet UILabel* originIndicator;
@property (strong, nonatomic) IBOutlet UILabel* destinationIndicator;




- (id) initWithStop: (BARDShuttleStop*) stop icon: (UIImage*) icon flag: (UIImage*) flag flagAspectRatio: (double) flagAspectRatio;

- (void) changeXValue: (double) xValue;
- (void) changeYValue: (double) yValue;
- (void) changeSideLength: (double) sideLength;
- (void) setIconInset:(double)iconInset;

- (void) setIconViewFrame: (CGRect) frame;
- (void) animateIconViewFrame: (CGRect) frame;
- (void) setFlagMaskFrame: (CGRect) frame;
- (void) animateFlagMaskFrame: (CGRect) frame;


- (void) fillStop;



@end
