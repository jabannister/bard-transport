//
//  BARDSelectorStop.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 11/15/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDSelectorStop.h"

@implementation BARDSelectorStop

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.iconView = [[UIImageView alloc] init];
        
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */


- (id) initWithStop:(BARDShuttleStop *)stop icon:(UIImage *)icon flag:(UIImage *)flag flagAspectRatio:(double)flagAspectRatio
{
    
    
    self = [super init];
    
    if ( self ) {
        self.stop = stop;
        self.icon = icon;
        self.flagImage = flag;
        self.flagAspectRatio = flagAspectRatio;
        self.iconInset = 0;
        self.index = -1;
        
        self.selected = NO;
        self.standby = NO;
        self.fullyFilled = NO;
        
        self.frame = CGRectMake(0, 0, 50, 50);
        
        
        self.iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, 42)];
        [self.iconView setImage:icon];
        [self addSubview:self.iconView];
        
        
        self.flagMask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 12.5)];
        self.flagMask.backgroundColor = [UIColor clearColor];
        self.flagMask.clipsToBounds = YES;
        
        self.flagView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, flagAspectRatio * 12.5, 12.5)];
        
        [self.flagView setImage:flag];
        [self.flagMask addSubview:self.flagView];
        
        
        self.originIndicator = [[UILabel alloc] initWithFrame:CGRectMake(-60, 0, 60, self.frame.size.height)];
        self.originIndicator.text = @"From:";
        self.originIndicator.font = [UIFont fontWithName:@"Avenir Heavy" size:18];
        self.originIndicator.textAlignment = NSTextAlignmentCenter;
        self.originIndicator.lineBreakMode = NSLineBreakByWordWrapping;
        self.originIndicator.numberOfLines = 0;
        self.originIndicator.layer.opacity = 0;
        [self addSubview:self.originIndicator];
        
        self.destinationIndicator = [[UILabel alloc] initWithFrame:CGRectMake(-60, 0, 60, self.frame.size.height)];
        self.destinationIndicator.text = @"To:";
        self.destinationIndicator.font = [UIFont fontWithName:@"Avenir Heavy" size:18];
        self.destinationIndicator.textAlignment = NSTextAlignmentCenter;
        self.destinationIndicator.lineBreakMode = NSLineBreakByWordWrapping;
        self.destinationIndicator.numberOfLines = 0;
        self.destinationIndicator.layer.opacity = 0;
        [self addSubview:self.destinationIndicator];
        
    }
    return self;
}


- (void) changeXValue:(double)xValue
{
    CGRect frame = self.frame;
    frame.origin.x = xValue;
    self.frame = frame;
    
}

- (void) changeYValue:(double)yValue
{
    CGRect frame = self.frame;
    frame.origin.y = yValue;
    self.frame = frame;
    
}

- (void) changeSideLength:(double)sideLength
{
    CGRect frame = self.frame;
    frame.size.width = sideLength;
    frame.size.height = sideLength;
    
    self.frame = frame;
    
    frame.origin.x = self.iconInset;
    frame.origin.y = self.iconInset;
    frame.size.width = sideLength - self.iconInset*2;
    frame.size.height = sideLength - self.iconInset*2;
    
    self.iconView.frame = frame;
    
    CGRect labelFrame = self.originIndicator.frame;
    labelFrame.size.height = self.frame.size.height;
    
    self.originIndicator.frame = labelFrame;
    self.destinationIndicator.frame = labelFrame;
}

- (void) setIconInset:(double)iconInset
{
    _iconInset = iconInset;
    CGRect frame = self.frame;
    frame.origin.x = iconInset;
    frame.origin.y = iconInset;
    frame.size.width -= 2*iconInset;
    frame.size.height -= 2*iconInset;
    
    self.iconView.frame = frame;
}





- (void) setIconViewFrame:(CGRect)frame
{
    self.iconView.frame = frame;
}

- (void) animateIconViewFrame:(CGRect)frame
{
    [UIView animateWithDuration:0.3 animations:^{
        self.iconView.frame = frame;
    }];
}

- (void) setFlagMaskFrame:(CGRect)frame
{
    self.flagMask.frame = frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = self.flagAspectRatio * frame.size.height;
    self.flagView.frame = frame;
}

- (void) animateFlagMaskFrame:(CGRect)frame
{
    CGRect newFlagFrame = frame;
    newFlagFrame.origin.x = 0;
    newFlagFrame.origin.y = 0;
    newFlagFrame.size.width = self.flagAspectRatio * frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.flagMask.frame = frame;
        self.flagView.frame = newFlagFrame;
    }];
}

@end
