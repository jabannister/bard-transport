//
//  BARDShuttleStop.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 9/27/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDShuttleStop.h"

@implementation BARDShuttleStop
@synthesize coordinate, title, subtitle;


- (id) initWithCoordinate:(CLLocationCoordinate2D)initialCoordinate title:(NSString *)initialTitle andSubtitle:(NSString *)initialSubtitle
{
    self = [super init];
    
    if(self) {
        [self setCoordinate: initialCoordinate];
        [self setTitle:initialTitle];
        [self setSubtitle:initialSubtitle];
    }
    return self;
}


+ (NSArray*) shuttleStopNames
{
    return @[@"Tivoli", @"Robbins", @"Kline", @"Red Hook", @"Hannaford"];
}

+ (NSArray*) allShuttleStopNames
{
    return @[@"Tivoli", @"Campus Road", @"Robbins", @"Ward Gate", @"Kline", @"Gahagan", @"Triangle", @"Church Street", @"Red Hook", @"MAT", @"Hannaford"];
}


@end
