//
//  BARDNavigationController.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/24/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDNavigationController.h"

int partialSlideFraction = 3.2;
int backPanTriggerThreshold = 25;
int menuOriginX = 0;
int dateRollOverHour = 4;

@interface BARDNavigationController ()
{
    CGRect onScreenFrame;
    CGRect offScreenFrameLeft;
    CGRect offScreenFrameRight;
    CGRect belowScreenFrame;
    CGRect mapOffScreenFrame;
    CGRect mapMenuOpenFrame;
    CGRect selectorMenuOpenFrame;
    CGRect selectorShadowFrame;
    
    int partialSlideAmount;
    double menuPaneBufferWidth;
    
    
    double tracker;
    
    BOOL outOfUpdateNotificationTapInitiated;
    BOOL inUpdateNotificationTapInitiated;
    
    BOOL versionNumberRetrieved;
}

@end

@implementation BARDNavigationController


- (id) init {
    
    self = [super init];
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Navigation Controller View Did Load");
    // Do any additional setup after loading the view.
    
    
    BARDAppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.navigator = self;
    
    
    
    self.versionNumber = @"2.9.3";
    
    
    versionNumberRetrieved = NO;
    
    [JABGlobalInfo getScreenDimensions];
    [JABGlobalInfo getOperatingSystem];
    
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPad"])
    {
        self.ipad = YES;
    }
    
    self.touchInProgress = 0;
    
    
    self.daysOfWeek = [BARDToolkit daysOfTheWeek];
    
    
    self.scheduleOptions = @"none";
    
    if ( [BARDToolkit currentYear] == 2014 ) {
        if ( [BARDToolkit currentMonth] == 6 || [BARDToolkit currentMonth] == 7 ) {
            self.scheduleOptions = @"summer";
        } else if ( [BARDToolkit currentMonth] == 8 ) {
            if ( [BARDToolkit currentDayOfMonth] <= 9 ) {
                self.scheduleOptions = @"summer";
            }
        }
    }
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = 20;
    
    self.mostRecentUserLocationIsValid = NO;
    
    [self.locationManager startUpdatingLocation];
    
    
    tracker = 0;
    
    self.smartTimesUnlocked = NO;
    
    
    screenWidth = [[UIScreen mainScreen] bounds].size.width;
    screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    screenWidth = screenWidth;
    
    self.partialSlideFactor = 3.2;
    self.shadowWidth = 7;
    
    partialSlideAmount = 100;
    
    menuPaneBufferWidth = 15;
    
    
    
    
    
    
    //Create Shuttle Stops
    
    
    NSArray *allShuttleStopTitles = [BARDShuttleStop allShuttleStopNames];
    
    NSArray *allShuttleStopLatitudes = @[@"42.0599000", @"42.0400000", @"42.0287648", @"42.0400000", @"42.0222182", @"42.0400000", @"42.0400000", @"42.0400000", @"41.9943290", @"42.0400000", @"41.9799000"];
    
    NSArray *allShuttleStopLongitudes = @[@"-73.9112821", @"-73.9080000", @"-73.9046179", @"-73.9080000", @"-73.9083828", @"-73.9080000", @"-73.9080000", @"-73.9080000", @"-73.8763699", @"-73.9080000", @"-73.8808082"];
    
    
    NSMutableArray *allShuttleStops = [[NSMutableArray alloc] initWithCapacity:13];
    
    for ( int i = 0; i < [allShuttleStopTitles count]; i++ ) {
        
        allShuttleStops[i] = [[BARDShuttleStop alloc] initWithCoordinate:CLLocationCoordinate2DMake([allShuttleStopLatitudes[i] floatValue], [allShuttleStopLongitudes[i] floatValue]) title:allShuttleStopTitles[i] andSubtitle:@"Shuttle Stop"];
        
        [allShuttleStops[i] setIconName:@"shuttleStopSquareRegularSmaller.png"];
        [allShuttleStops[i] setIndex:i];
    }
    
    
    self.allShuttleStops = [[NSArray alloc] initWithArray:allShuttleStops];
    
    
    
    
    
    
    
    
    
    
    NSArray *shuttleStopTitles = [BARDShuttleStop shuttleStopNames];
    
    NSArray *shuttleStopLatitudes = @[@"42.0599000", @"42.0287648", @"42.0222182", @"41.9943290", @"41.9799000"];
    
    NSArray *shuttleStopLongitudes = @[@"-73.9112821", @"-73.9046179", @"-73.9083828", @"-73.8763699", @"-73.8808082"];
    
    
    
    NSMutableArray *shuttleStops = [[NSMutableArray alloc] initWithCapacity:13];
    
    for ( int i = 0; i < [shuttleStopTitles count]; i++ ) {
        
        shuttleStops[i] = [[BARDShuttleStop alloc] initWithCoordinate:CLLocationCoordinate2DMake([shuttleStopLatitudes[i] floatValue], [shuttleStopLongitudes[i] floatValue]) title:shuttleStopTitles[i] andSubtitle:@"Shuttle Stop"];
        
        [shuttleStops[i] setIconName:@"shuttleStopSquareRegularSmaller.png"];
        [shuttleStops[i] setIndex:i];
    }
    
    
    self.shuttleStops = [[NSArray alloc] initWithArray:shuttleStops];
    
    
    
    
    
    
    BARDMapViewController* mapController = [[BARDMapViewController alloc] initWithNibName:@"BARDMapViewController" bundle:nil];
    mapController.navigator = self;
    self.mapController = mapController;
    self.mapController.view.frame = onScreenFrame;
    
    
    BARDMenuViewController* menuController = [[BARDMenuViewController alloc] initWithNibName:@"BARDMenuViewController" bundle:nil];
    menuController.navigator = self;
    self.menuController = menuController;
    
    BARDScheduleSelectorViewController* scheduleSelectorController = [[BARDScheduleSelectorViewController alloc] initWithNibName:@"BARDScheduleSelectorViewController" bundle:nil];
    self.scheduleSelectorController = scheduleSelectorController;
    
    self.scheduleSelectorController.navigator = self;
    UIView *uselessViewToLoadScheduleSelectorView = self.scheduleSelectorController.view;
    
    
    
    if ( self.ipad ) {
        self.percentageOfScreenForMenu = 0.35;
    } else {
        self.percentageOfScreenForMenu = 0.81;
    }
    
    selectorMenuOpenFrame = CGRectMake(screenWidth*self.percentageOfScreenForMenu, 0, screenWidth, screenHeight);
    
    
    
    onScreenFrame = CGRectMake(0, 0, screenWidth, screenHeight);
    offScreenFrameRight = CGRectMake(screenWidth, 0, screenWidth, screenHeight);
    offScreenFrameLeft = CGRectMake(-screenWidth, 0, screenWidth, screenHeight);
    belowScreenFrame = CGRectMake(0, screenHeight, screenWidth, screenHeight);
    mapOffScreenFrame = CGRectMake(screenWidth/partialSlideFraction, 0, screenWidth, screenHeight);
    mapMenuOpenFrame = CGRectMake((1 + self.percentageOfScreenForMenu) * (screenWidth/partialSlideFraction), 0, screenWidth, screenHeight);
    
    
    selectorShadowFrame = onScreenFrame;
    selectorShadowFrame.origin.x -= self.shadowWidth;
    
    self.closedMenuSelectorShadowFrame = selectorShadowFrame;
    
    
    scheduleSelectorController.selectorMenuOpenFrame = selectorMenuOpenFrame;
    
    
    
    
    
    [mapController.view setFrame:offScreenFrameRight];
    [self addChildViewController:mapController];
    [self.view addSubview:mapController.view];
    
    mapController.view.frame = mapOffScreenFrame;
    
    
    
    
    
    
    
    
    [scheduleSelectorController.view setFrame:offScreenFrameRight];
    [self addChildViewController:scheduleSelectorController];
    [self.view addSubview:scheduleSelectorController.view];
    scheduleSelectorController.view.frame = onScreenFrame;
    
    
    
    
    
    menuController.visibleRange = (double) screenWidth * self.percentageOfScreenForMenu;
    
    [self addChildViewController:menuController];
    [self.view insertSubview:menuController.view belowSubview:self.mapController.view];
    menuController.view.frame = onScreenFrame;
    
    
    
    
    
    
    
    
    //Update Notification Screen
    
    
    self.screenDarkener = [[UIView alloc] initWithFrame:onScreenFrame];
    self.screenDarkener.backgroundColor = [UIColor blackColor];
    self.screenDarkener.userInteractionEnabled = NO;
    self.screenDarkener.layer.opacity = 0;
    [self.view addSubview:self.screenDarkener];
    
    self.updateNotificationScreen = [[UIView alloc] initWithFrame:CGRectMake(screenWidth*(1.0/8.0), screenHeight*(1.0/3.75), screenWidth*(6.0/8.0), screenHeight*(1.75/3.75))];
    self.updateNotificationScreen.backgroundColor = [UIColor whiteColor];
    self.updateNotificationScreen.layer.cornerRadius = 4;
    self.updateNotificationScreen.layer.opacity = 0;
    self.updateNotificationScreen.userInteractionEnabled = NO;
    self.updateNotificationScreen.clipsToBounds = YES;
    [self.view addSubview:self.updateNotificationScreen];
    
    double updateButtonSizeNumerator = 1.0;
    double updateButtonSizeDenominator = 5.5;
    
    self.updateButton = [[UIView alloc] initWithFrame:CGRectMake(0, self.updateNotificationScreen.frame.size.height * ((updateButtonSizeDenominator - updateButtonSizeNumerator)/updateButtonSizeDenominator), self.updateNotificationScreen.frame.size.width, self.updateNotificationScreen.frame.size.height * (updateButtonSizeNumerator/updateButtonSizeDenominator))];
    self.updateButton.backgroundColor = [UIColor lightGrayColor];
    self.updateButton.layer.opacity = 0.7;
    self.updateButton.userInteractionEnabled = NO;
    [self.updateNotificationScreen addSubview:self.updateButton];
    
    
    self.updateButtonLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.updateNotificationScreen.frame.size.height * ((updateButtonSizeDenominator - updateButtonSizeNumerator)/updateButtonSizeDenominator), self.updateNotificationScreen.frame.size.width, self.updateNotificationScreen.frame.size.height * (updateButtonSizeNumerator/updateButtonSizeDenominator))];
    self.updateButtonLabel.text = @"UPDATE";
    self.updateButtonLabel.font = [UIFont fontWithName:@"Avenir" size:20];
    self.updateButtonLabel.textAlignment = NSTextAlignmentCenter;
    [self.updateNotificationScreen addSubview:self.updateButtonLabel];
    
    
    CGRect topLineFrame = self.updateButton.frame;
    topLineFrame.size.height = 1;
    topLineFrame.origin.y--;
    self.updateButtonTopLine = [[UIView alloc] initWithFrame:topLineFrame];
    self.updateButtonTopLine.backgroundColor = [UIColor blackColor];
    [self.updateNotificationScreen addSubview:self.updateButtonTopLine];
    
    
    UILabel *updateNotificationMessageMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.updateNotificationScreen.frame.size.width, self.updateNotificationScreen.frame.size.height * ((updateButtonSizeDenominator - updateButtonSizeNumerator)/updateButtonSizeDenominator))];
    updateNotificationMessageMessage.font = [UIFont fontWithName:@"Avenir" size:16];
    updateNotificationMessageMessage.text = @"ATTENTION:\n\nThis version of the app is out of date! Go to the app store to update to the latest shuttle schedule.";
    updateNotificationMessageMessage.lineBreakMode = NSLineBreakByWordWrapping;
    updateNotificationMessageMessage.numberOfLines = 0;
    updateNotificationMessageMessage.textAlignment = NSTextAlignmentCenter;
    [self.updateNotificationScreen addSubview:updateNotificationMessageMessage];
    
    
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(screenDarkenerLongPressDetected:)];
    longPressRecognizer.minimumPressDuration = 0.01;
    longPressRecognizer.allowableMovement = screenHeight * 2;
    [self.screenDarkener addGestureRecognizer:longPressRecognizer];
    
    
    
    // Lockdown notification
    
    self.lockdownNotificationScreen = [[UIView alloc] initWithFrame:CGRectMake((screenWidth - 255)/2, (screenHeight - 400)/2, 255, 400)];
    self.lockdownNotificationScreen.backgroundColor = [UIColor whiteColor];
    self.lockdownNotificationScreen.layer.cornerRadius = 4;
    self.lockdownNotificationScreen.layer.opacity = 0;
    self.lockdownNotificationScreen.userInteractionEnabled = NO;
    self.lockdownNotificationScreen.clipsToBounds = YES;
    [self.view addSubview:self.lockdownNotificationScreen];
    
    
    UILabel *lockdownNotificationMessageMessage = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.lockdownNotificationScreen.frame.size.width - 20, self.lockdownNotificationScreen.frame.size.height)];
    lockdownNotificationMessageMessage.font = [UIFont fontWithName:@"Avenir" size:16];
    lockdownNotificationMessageMessage.text = @"Dear students,\n\nThe administration has been unfortunately slow about paying the $108/year which is required to keep this app running. This minimal fee has gone unpaid for 3 years, and the app is therefore no longer available. If you never use this app anyway then pay no mind. If this inconveniences you however, please contact the school to voice your concerns and this can be swiftly reversed.\n\nBest,\nJeremy";
    lockdownNotificationMessageMessage.lineBreakMode = NSLineBreakByWordWrapping;
    lockdownNotificationMessageMessage.numberOfLines = 0;
    lockdownNotificationMessageMessage.textAlignment = NSTextAlignmentCenter;
    [self.lockdownNotificationScreen addSubview:lockdownNotificationMessageMessage];
    
    
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isAppLockedDown"];
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAppLockedDown"]) { [self lockdownApp]; }
    
    
    //    [menuController.view setBackgroundColor:[UIColor clearColor]];
    //
    //    CAGradientLayer* gradient = [CAGradientLayer layer];
    //    gradient.frame = menuController.view.bounds;
    //    NSArray* colors = @[[UIColor colorFromHexString:[BARDToolkit hexcodeFromR:254 G:255 B:255]], [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:255 B:0]]];
    //    gradient.colors = colors;
    //    [menuController.view.layer addSublayer:gradient];
    ////    [menuController.view.layer insertSublayer:gradient atIndex:0];
    //
    //    NSLog(@"%%");
    //    NSLog(@"huh %@", gradient.colors);
    //    NSLog(@"%@", [menuController.view.layer sublayers]);
    //    NSLog(@"$$");
    
    
    
    
    NSMutableArray *time1 = [[NSMutableArray alloc] initWithCapacity:2];
    [time1 addObject:[NSNumber numberWithInt:19]];
    [time1 addObject:[NSNumber numberWithInt:36]];
    
    NSMutableArray *time2 = [[NSMutableArray alloc] initWithCapacity:2];
    [time2 addObject:[NSNumber numberWithInt:18]];
    [time2 addObject:[NSNumber numberWithInt:30]];
    
    
    //Hiding GPS for 2.5
    //    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkIfProperTimeToQuery) userInfo:self repeats:YES];
    
    //    [self queryTwitter];
    //Hiding GPS for 2.5
    
    [self updateCurrentVersion];
    [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(updateCurrentVersion) userInfo:self repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(checkIfAppIsUpToDate) userInfo:self repeats:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateDateAndTime) userInfo:self repeats:YES];
    
    self.mapController.map.frame = onScreenFrame;
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) updateDateAndTime
{
    
    self.dayOfWeek = [BARDToolkit currentDayOfWeek];
    
    self.dayOfWeekIndex = [BARDToolkit indexOfString:self.dayOfWeek inArray:self.daysOfWeek];
    
    self.currentHour = [BARDToolkit currentHour];
    self.currentMinute = [BARDToolkit currentMinute];
    self.currentSecond = [BARDToolkit currentSecond];
    
    if ( self.currentHour < dateRollOverHour ) {
        
        self.currentHour += 24;
        
        if ( self.dayOfWeekIndex != 0 ) {
            self.dayOfWeekIndex--;
        } else {
            self.dayOfWeekIndex = 6;
        }
        
        
        self.dayOfWeek = self.daysOfWeek[self.dayOfWeekIndex];
    }
}


#pragma -mark Transitions

- (void) openMap
{
    NSLog(@"Opening Map");
    [self.view insertSubview:self.mapController.view aboveSubview:self.menuController.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.menuController.view setFrame:offScreenFrameLeft];
        [self.scheduleSelectorController.view setFrame:offScreenFrameLeft];
        [self.mapController.view setFrame:onScreenFrame];
        
    }];
    self.mapOpen = YES;
}



- (void) openMenu
{
    NSLog(@"Opening Menu");
    
    [self.view insertSubview:self.menuController.view aboveSubview:self.mapController.view];
    
    selectorShadowFrame = selectorMenuOpenFrame;
    selectorShadowFrame.origin.x -= self.shadowWidth;
    
    self.menuController.view.frame = onScreenFrame;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.scheduleSelectorController.view.frame = selectorMenuOpenFrame;
        self.menuController.gradientLayer.frame = selectorShadowFrame;
        self.mapController.view.frame = mapMenuOpenFrame;
        
    }];
    
    
    self.scheduleSelectorController.scheduleController.table.userInteractionEnabled = NO;
    self.scheduleSelectorController.scheduleController.dayToggle.userInteractionEnabled = NO;
    self.menuOpen = YES;
}



- (void) openSelector
{
    NSLog(@"Opening Selector");
    
    selectorShadowFrame = onScreenFrame;
    selectorShadowFrame.origin.x -= self.shadowWidth;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.menuController.view.frame = onScreenFrame;
        self.menuController.gradientLayer.frame = selectorShadowFrame;
        self.scheduleSelectorController.view.frame = onScreenFrame;
        self.mapController.view.frame = mapOffScreenFrame;
        
    }];
    
    self.scheduleSelectorController.scheduleController.table.userInteractionEnabled = YES;
    self.scheduleSelectorController.scheduleController.dayToggle.userInteractionEnabled = YES;
    self.menuOpen = NO;
    self.mapOpen = NO;
    
}




- (void) lockdownApp {
    self.isAppLockedDown = YES;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAppLockedDown"];
    if (self.isAppLockedDown) {
        [UIView animateWithDuration:0.3 animations:^{
            self.screenDarkener.layer.opacity = 0.6;
            self.screenDarkener.userInteractionEnabled = YES;
            self.lockdownNotificationScreen.layer.opacity = 1;
        }];
    }
}


- (void) openUpdateNotificationScreen
{
    [UIView animateWithDuration:0.3 animations:^{
        self.screenDarkener.layer.opacity = 0.6;
        self.screenDarkener.userInteractionEnabled = YES;
        self.updateNotificationScreen.layer.opacity = 1;
    }];
}

- (void) closeUpdateNotificationScreen
{
    [UIView animateWithDuration:0.3 animations:^{
        self.screenDarkener.layer.opacity = 0;
        self.screenDarkener.userInteractionEnabled = NO;
        self.updateNotificationScreen.layer.opacity = 0;
    }];
}


- (void) screenDarkenerLongPressDetected:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint location = [gestureRecognizer locationInView:self.view];
    if ( gestureRecognizer.state == UIGestureRecognizerStateBegan ) {
        
        if ( ![BARDToolkit rect:self.updateNotificationScreen.frame containsPoint:location] ) {
            
            outOfUpdateNotificationTapInitiated = YES;
            
        } else {
            
            self.updateButton.backgroundColor = [UIColor darkGrayColor];
            
        }
        
    } else if ( gestureRecognizer.state == UIGestureRecognizerStateChanged ) {
        
        if ( ![BARDToolkit rect:self.updateNotificationScreen.frame containsPoint:location] && [self.updateButton.backgroundColor isEqualToColor:[UIColor darkGrayColor]] ) {
            
            self.updateButton.backgroundColor = [UIColor lightGrayColor];
            
        } else if ( [BARDToolkit rect:self.updateNotificationScreen.frame containsPoint:location] && [self.updateButton.backgroundColor isEqualToColor:[UIColor lightGrayColor]] && !outOfUpdateNotificationTapInitiated ) {
            
            self.updateButton.backgroundColor = [UIColor darkGrayColor];
            
        }
        
    } else if ( gestureRecognizer.state == UIGestureRecognizerStateEnded ) {
        
        self.updateButton.backgroundColor = [UIColor lightGrayColor];
        if ( outOfUpdateNotificationTapInitiated ) {
            
            if ( ![BARDToolkit rect:self.updateNotificationScreen.frame containsPoint:location] ) {
                
                if (self.isAppLockedDown) { return; }
                [self closeUpdateNotificationScreen];
                
            }
            
        } else {
            
            if ( [BARDToolkit rect:self.updateNotificationScreen.frame containsPoint:location] ) {
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id789572922"]];
                
            }
        }
        outOfUpdateNotificationTapInitiated = NO;
        
    } else if ( gestureRecognizer.state == UIGestureRecognizerStateCancelled ) {
        
        self.updateButton.backgroundColor = [UIColor lightGrayColor];
        
    }
    
}

- (void) checkIfAppIsUpToDate
{
    if ([self.currentVersionNumber isEqualToString: @"lockDown"]) { [self lockdownApp]; return; }
    if ( [BARDToolkit versionNumber:self.versionNumber isLessThanVersionNumber:self.currentVersionNumber] && versionNumberRetrieved ) {
        [self openUpdateNotificationScreen];
    }
}

- (void) updateCurrentVersion
{
    
    versionNumberRetrieved = NO;
    NSString *urlString = @"https://twitter.com/BardAppVersion";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSString *pageString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSRange range = [pageString rangeOfString:@"Current Version: "];
        
        if ( range.location == NSNotFound ) {
            return;
        }
        
        pageString = [pageString substringFromIndex:range.location + range.length];
        range = [pageString rangeOfString:@"^**^"];
        
        if ( range.location == NSNotFound ) {
            return;
        }
        
        pageString = [pageString substringToIndex:range.location];
        
        self.currentVersionNumber = pageString;
        
        versionNumberRetrieved = YES;
    }];
    
}



- (void) checkIfProperTimeToQuery
{
    if ( [BARDToolkit timeIntervalInSecondsBetweenTime:self.timeOfLatestLocationTweet andTime:[BARDToolkit currentTime]] > 50 ) {
        [self queryTwitter];
    }
}


- (void) queryTwitter
{
    NSString *urlString = @"https://twitter.com/bardtempgps";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSString *pageString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSRange range = [pageString rangeOfString:@"Shuttle Location Update: "];
        
        if ( range.location == NSNotFound ) {
            return;
        }
        
        pageString = [pageString substringFromIndex:range.location + range.length];
        range = [pageString rangeOfString:@"^**^"];
        
        if ( range.location == NSNotFound ) {
            return;
        }
        
        NSString *secondString = [pageString substringFromIndex:range.location + range.length];
        NSRange secondRange = [secondString rangeOfString:@"!**!"];
        
        if ( secondRange.location == NSNotFound ) {
            return;
        }
        
        secondString = [secondString substringFromIndex:secondRange.location + secondRange.length];
        secondRange = [secondString rangeOfString:@"!*.*!"];
        
        if ( secondRange.location == NSNotFound ) {
            return;
        }
        
        secondString = [secondString substringToIndex:secondRange.location];
        
        
        NSArray *timeOfTweet = [secondString componentsSeparatedByString:@":"];
        
        self.timeOfLatestLocationTweet = [NSArray arrayWithObjects:[NSNumber numberWithLong:[timeOfTweet[0] integerValue]], [NSNumber numberWithLong:[timeOfTweet[1] integerValue]], [NSNumber numberWithLong:[timeOfTweet[2] integerValue]], nil];
        
        
        
        
        pageString = [pageString substringToIndex:range.location];
        
        NSArray *components = [pageString componentsSeparatedByString:@","];
        
        double latitude = [components[0] doubleValue];
        double longitude = [components[1] doubleValue];
        
        self.latestLocationTweet = CLLocationCoordinate2DMake(latitude, longitude);
        self.newLocation = YES;
    }];
}



#pragma  -mark Location Services


- (void) forceLocationUpdate
{
    [self.locationManager stopUpdatingLocation];
    [self.locationManager startUpdatingLocation];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    self.mostRecentUserLocationIsValid = NO;
}


- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if ( [[locations lastObject] coordinate].latitude < 1000 ) {
        self.mostRecentUserLocationIsValid = YES;
    } else {
        self.mostRecentUserLocationIsValid = NO;
    }
    if ( self.mostRecentUserLocation.coordinate.latitude != [[locations lastObject] coordinate].latitude || self.mostRecentUserLocation.coordinate.longitude != [[locations lastObject] coordinate].longitude ) {
        
        self.mostRecentUserLocation = [locations lastObject];
        [self.menuController updateSmartTimesPaneSentFromLocationManager:YES];
        
    }
    
    //    NSLog(@"Location Manager Did Update Location");
}




@end
