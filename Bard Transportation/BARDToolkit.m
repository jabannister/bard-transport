//
//  BARDToolkit.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/19/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDToolkit.h"

@implementation BARDToolkit



+ (BOOL) number:(double)number isBetweenFirstNumber:(double)first andSecondNumber:(double)second
{
    if ( number >= first && number <= second ) {
        return YES;
    } else if ( number >= second && number <= first) {
        return YES;
    }
    
    return NO;
}



#pragma -mark Rect Manipulations

+ (BOOL) rect:(CGRect)rect containsPoint:(CGPoint)point
{
    if ( [self number:point.x isBetweenFirstNumber:rect.origin.x andSecondNumber:rect.origin.x + rect.size.width] && [self number:point.y isBetweenFirstNumber:rect.origin.y andSecondNumber:rect.origin.y + rect.size.height]) {
        return YES;
    }
    return NO;
}




+ (double) topOfRect:(CGRect)rect
{
    return rect.origin.y;
}

+ (double) bottomOfRect:(CGRect)rect
{
    return rect.origin.y + rect.size.height;
}

+ (double) leftOfRect:(CGRect)rect
{
    return rect.origin.x;
}

+ (double) rightOfRect:(CGRect)rect
{
    return rect.origin.x + rect.size.width;
}

+ (CGPoint) centerOfRect:(CGRect)rect
{
    return CGPointMake(rect.origin.x + rect.size.width/2, rect.origin.y + rect.size.height/2);
}


+ (double) distanceFromPoint:(CGPoint)point toCenterOfRect:(CGRect)rect
{
    CGPoint centerOfRect = [self centerOfRect:rect];
    return [self distanceBetweenPoint1:point point2:centerOfRect];
}


+ (CGRect) slideRect: (CGRect) rect byAmount: (double) distance
{
    CGRect newRect = rect;
    newRect.origin.x += distance;
    return newRect;
}

+ (CGRect) raiseRect: (CGRect) rect byAmount: (double) distance
{
    CGRect newRect = rect;
    newRect.origin.y += distance;
    return newRect;
}

+ (CGRect) widenRect: (CGRect) rect byAmount: (double) amount
{
    CGRect newRect = rect;
    newRect.size.width += amount;
    return newRect;
}

+ (CGRect) heightenRect: (CGRect) rect byAmount: (double) amount
{
    CGRect newRect = rect;
    newRect.size.height += amount;
    return newRect;
}



+ (CGRect) changeXValueOfRect: (CGRect) rect toValue: (double) xValue
{
    CGRect newRect = rect;
    newRect.origin.x = xValue;
    return newRect;
}

+ (CGRect) changeYValueOfRect: (CGRect) rect toValue: (double) yValue
{
    CGRect newRect = rect;
    newRect.origin.y = yValue;
    return newRect;
}

+ (CGRect) changeWidthOfRect: (CGRect) rect toValue: (double) width
{
    CGRect newRect = rect;
    newRect.size.width = width;
    return newRect;
}

+ (CGRect) changeHeightOfRect: (CGRect) rect toValue: (double) height
{
    CGRect newRect = rect;
    newRect.size.height = height;
    return newRect;
}


+ (double) distanceBetweenPoint1:(CGPoint)point1 point2:(CGPoint)point2
{
    CGPoint shiftedPoint = CGPointMake(point2.x - point1.x, point2.y - point1.y);
    double sumOfSquares = [self raiseBase:shiftedPoint.x toPower:2] + [self raiseBase:shiftedPoint.y toPower:2];
    return sqrt(sumOfSquares);
}


+ (double) degreesToRadians:(double)degrees
{
    double pi = 3.14159;
    return degrees * (2*pi/360);
}

+ (double) radiansToDegrees:(double)radians
{
    double pi = 3.14159;
    return radians * (360/2*pi);
}



+ (double) averageOfX:(double)x AndY:(double)y
{
    return (x+y)/2;
}


+ (double) absoluteValue:(double)number
{
    if ( number < 0 ) {
        return -number;
    }
    return number;
}


+ (int) roundDownDouble:(double)number
{
    int runningSum = 0;
    BOOL done = false;
    if ( number > 0 ) {
        while ( !done ) {
            if ( runningSum > number ) {
                return (runningSum - 1);
            } else if (runningSum == number ) {
                return runningSum;
            }
            runningSum++;
        }
    } else {
        while ( !done ) {
            if ( runningSum < number ) {
                return (runningSum + 1);
            } else if ( runningSum == number ) {
                return runningSum;
            }
            runningSum--;
        }
    }
    
    return 0;
    
}


+ (int) highestPowerOfBase:(int)base thatFitsIntoNumber:(int)number
{
    if ( number == 0 ) {
        return 0;
    }
    
    BOOL done = false;
    int exp = 0;
    int copy = 1;
    
    while ( !done ) {
        
        if ( copy > number ) {
            return (exp - 1);
        } else if ( copy == number ) {
            return exp;
        }
        
        copy = copy * base;
        exp++;
        
    }
    
    return -1;
}


+ (double) minimumOfValues:(NSArray *)values
{
    double minimum = 0;
    if ( [values count] > 0 ) {
        minimum = [values[0] integerValue];
        for ( int i = 0; i < [values count]; i++ ) {
            double number = [values[i] integerValue];
            if ( number < minimum ) {
                minimum = number;
            }
        }
    }
    
    return minimum;
}

+ (double) raiseBase:(double) base toPower:(int)power
{
    double result = 1;
    if ( power >= 0 ) {
        for ( int i = 0; i < power; i++ ) {
            result = result * base;
        }
    } else {
        power = power * (-1);
        for ( int i = 0; i < power; i++ ) {
            result = result / base;
        }
    }
    
    return result;
}


+ (NSString*) hexidecimalStringFromInt: (int) number
{
    NSArray* hexidecimalDigits = @[@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"A", @"B", @"C", @"D", @"E", @"F"];
    
    int numberOfDigits = [self highestPowerOfBase:16 thatFitsIntoNumber:number] + 1;
    NSMutableArray* resultDigits = [[NSMutableArray alloc] initWithCapacity:numberOfDigits];
    
    
    int copy = number;
    
    int incrementingIndex = 0;
    
    for ( int i = numberOfDigits - 1; i >= 0; i-- ) {
        int numberValueOfCurrentDigit = [self roundDownDouble:copy/[self raiseBase:16 toPower:i]];
        resultDigits[incrementingIndex] = [hexidecimalDigits[numberValueOfCurrentDigit] copy];
        copy = copy - ([self raiseBase:16 toPower:i] * ([self roundDownDouble:copy/[self raiseBase:16 toPower:i]]));
        incrementingIndex++;
    }
    
    
    return [self joinArrayOfStrings:resultDigits];
}

+ (NSString*) hexcodeFromR:(int)r G:(int)g B :(int)b
{
    if ( (r >= 0 && r < 256) && (g >= 0 &&  g < 256) && (b >= 0 && b < 256) ) {
        NSString* code = @"";
        NSString* inspectionTable = @"";
        inspectionTable = [self hexidecimalStringFromInt:r];
        if ( [inspectionTable length] == 1 ) {
            inspectionTable = [@"0" stringByAppendingString:inspectionTable];
        }
        code = [code stringByAppendingString:inspectionTable];
        inspectionTable = @"";
        inspectionTable = [self hexidecimalStringFromInt:g];
        if ( [inspectionTable length] == 1 ) {
            inspectionTable = [@"0" stringByAppendingString:inspectionTable];
        }
        code = [code stringByAppendingString:inspectionTable];
        inspectionTable = @"";
        inspectionTable = [self hexidecimalStringFromInt:b];
        if ( [inspectionTable length] == 1 ) {
            inspectionTable = [@"0" stringByAppendingString:inspectionTable];
        }
        code = [code stringByAppendingString:inspectionTable];
        
        return code;
    }
    return @"";
}


+ (NSString*) joinArrayOfStrings:(NSArray *)array
{
    if ( [self array:array isUniformlyOfType:@"string"] ) {
        NSString* concatenation = @"";
        for ( int i = 0; i < [array count]; i++ ) {
            concatenation = [concatenation stringByAppendingString:array[i]];
        }
        return concatenation;
    }
    
    return @"";
}


+ (NSString*) capitalizeLetter:(NSString *)letter
{
    if ( [letter length] == 1 ) {
        
        NSArray* capitals = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
        
        NSArray* lowercases = @[@"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z"];
        
        int index = [self indexOfString:letter inArray:lowercases];
        
        if ( index != -1 ) {
            return capitals[index];
        } else {
            return letter;
        }
        
        
    } else {
        return @"";
    }
}

+ (NSString*) capitalizeWords:(NSString *)string
{
    NSArray* words = [string componentsSeparatedByString:@" "];
    NSMutableArray* allChars = [[NSMutableArray alloc] init];
    for ( int i = 0; i < [words count]; i++ ) {
        NSMutableArray* characters = [[NSMutableArray alloc] initWithCapacity:[words[i] length]];
        for (int j = 0; j < [string length]; j++) {
            NSString* jchar  = [NSString stringWithFormat:@"%c", [string characterAtIndex:j]];
            [characters addObject:jchar];
        }
        [allChars addObject:characters];
    }
    for ( int i = 0; i < [allChars count]; i++ ) {
        if ( [allChars[i] count] > 0 ) {
            allChars[i][0] = [self capitalizeLetter:allChars[i][0]];
        }
    }
    
    NSMutableArray* capitalizedWords = [[NSMutableArray alloc] init];
    for ( int i = 0; i < [allChars count]; i++ ) {
        [capitalizedWords addObject:[self joinArrayOfStrings:allChars[i]]];
    }
    return [self joinArrayOfStrings:capitalizedWords];
    
    
}

+ (NSArray*) daysOfWeek
{
    return @[@"monday", @"tuesday", @"wednesday", @"thursday", @"friday", @"saturday", @"sunday"];
}

+ (NSArray*) daysOfWeekCapitalized
{
    return @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
}


+ (UILabel *) copyLabel: (UILabel *) label
{
    UILabel* copy = [[UILabel alloc] initWithFrame:label.frame];
    
    [copy setText:label.text];
    [copy setFont:label.font];
    [copy setTextColor:label.textColor];
    
    return copy;
    
}


+ (int) indexOfPointInArray:(NSArray *)points closestToPoint:(CGPoint)point
{
    NSMutableArray* distances = [[NSMutableArray alloc] initWithCapacity:[points count]];
    for ( int i = 0; i < [points count]; i++ ) {
        CGPoint current = [points[i] CGPointValue];
        double x2 = [self raiseBase:(current.x - point.x) toPower:2];
        double y2 = [self raiseBase:(current.y - point.y) toPower:2];
        distances[i] = [NSNumber numberWithDouble:sqrt(x2+y2)];
    }
    
    return [self indexOfLowestNumberInArray:distances];
}

+ (double) distanceFromPoint:(CGPoint)point toClosestPointInArray:(NSArray *)points
{
    NSMutableArray* distances = [[NSMutableArray alloc] initWithCapacity:[points count]];
    for ( int i = 0; i < [points count]; i++ ) {
        CGPoint current = [points[i] CGPointValue];
        double x2 = [self raiseBase:(current.x - point.x) toPower:2];
        double y2 = [self raiseBase:(current.y - point.y) toPower:2];
        distances[i] = [NSNumber numberWithDouble:sqrt(x2+y2)];
    }
    
    return [distances[[self indexOfLowestNumberInArray:distances]] doubleValue];
}


+ (int) indexOfLowestNumberInArray:(NSArray *)array
{
    if ( [array count] == 0 ) {
        return -1;
    }
    double lowestSoFar = [array[0] doubleValue];
    int index = 0;
    for ( int i = 1; i < [array count]; i++ ) {
        double current = [array[i] doubleValue];
        if ( current < lowestSoFar ) {
            lowestSoFar = current;
            index = i;
        }
    }
    
    return index;
}

+ (BOOL) versionNumber:(NSString *)versionNumber1 isLessThanVersionNumber:(NSString *)versionNumber2
{
    NSArray* first = [versionNumber1 componentsSeparatedByString:@"."];
    NSArray* second = [versionNumber2 componentsSeparatedByString:@"."];
    
    for ( int i = 0; i < [first count]; i++ ) {
        if ( [second count] > i ) {
            if ( [first[i] integerValue] < [second[i] integerValue] ) {
                return YES;
            } else if ( [first[i] integerValue] > [second[i] integerValue] ) {
                return NO;
            }
        } else {
            return NO;
        }
    }
    if ( [first count] < [second count] ) {
        return YES;
    } else {
        return NO;
    }
    
}

+ (BOOL) array:(NSArray *)array isUniformlyOfType:(NSString *)type
{
    
    
    if ( [type isEqualToString:@"string"] ) {
        for ( int i = 0; i < [array count]; i++ ) {
            if ( ![array[i] isKindOfClass:[NSString class]] ) {
                return false;
            }
        }
    }
    
    return true;
}

+ (int) indexOfString:(NSString*)string inArray:(NSArray *)array
{
    for ( int i = 0; i < [array count]; i++ ) {
        if ( [array[i] isEqualToString:string] ) {
            return i;
        }
    }
    
    return -1;
}

+ (int) indexOfObject:(id)object inArray:(NSArray *)array
{
    for ( int i = 0; i < [array count]; i++ ) {
        if ( array[i] == object ) {
            return i;
        }
    }
    
    return -1;
}


+ (int) indexOfEarliestTimeInArray:(NSArray *)times
{
    
    NSMutableArray* parsedTimes = [[NSMutableArray alloc] initWithCapacity:[times count]];
    
    for ( int i = 0; i < [times count]; i++ ) {
        NSString* time = times[i];
        NSMutableArray* partiallyParsedString = [[NSMutableArray alloc] initWithCapacity:3];
        partiallyParsedString[0] = [time componentsSeparatedByString:@":"][0];
        partiallyParsedString[1] = [[time componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "][0];
        partiallyParsedString[2] = [[time componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "][1];
        
        partiallyParsedString[0] = [[NSNumber alloc] initWithLong:[partiallyParsedString[0] integerValue]];
        partiallyParsedString[1] = [[NSNumber alloc] initWithLong:[partiallyParsedString[1] integerValue]];
        if ( [partiallyParsedString[2] isEqualToString:@"pm"] ) {
            NSNumber* hour = [[NSNumber alloc] initWithInt:[partiallyParsedString[0] intValue] + 12];
            partiallyParsedString[0] = hour;
        }
        [partiallyParsedString removeObjectAtIndex:2];
        [parsedTimes addObject:partiallyParsedString];
    }
    
    //    for ( int i = 0; i < [times count]; i++ ) {
    //        NSLog(@"times = %f:%f", parsedTimes[i][0], parsedTimes[i][1]);
    //    }
    
    
    NSMutableArray* orderedTimes = [[NSMutableArray alloc] initWithCapacity:[times count]];
    NSMutableArray* originalIndicesOfOrderedTimes;
    [orderedTimes addObject:parsedTimes[0]];
    
    for ( int i = 0; i < [parsedTimes count]; i++ ) {
        BOOL placed = NO;
        for ( int j = 0; j < [orderedTimes count]; j++ ) {
            if ( [parsedTimes[i][0] integerValue] < [orderedTimes[j][0] integerValue] ) {
                [orderedTimes insertObject:parsedTimes[i] atIndex:j];
                [originalIndicesOfOrderedTimes insertObject:[[NSNumber alloc] initWithInt:i] atIndex:j];
                placed = YES;
            } else if ( [parsedTimes[i][0] integerValue] == [orderedTimes[j][0] integerValue] ) {
                if (  [parsedTimes[i][1] integerValue] <= [orderedTimes[j][1] integerValue] ) {
                    [orderedTimes insertObject:parsedTimes[i] atIndex:j];
                    [originalIndicesOfOrderedTimes insertObject:[[NSNumber alloc] initWithInt:i] atIndex:j];
                    placed = YES;
                }
            }
        }
        if ( !placed ) {
            [orderedTimes addObject:parsedTimes[i]];
            [originalIndicesOfOrderedTimes addObject:[[NSNumber alloc] initWithInt:i]];
        }
    }
    return (int)[originalIndicesOfOrderedTimes[0] integerValue];
}

+ (NSArray*) convertTimeStringTo24HourTime:(NSString *)time
{
    NSMutableArray* partiallyParsedString = [[NSMutableArray alloc] initWithCapacity:3];
    partiallyParsedString[0] = [time componentsSeparatedByString:@":"][0];
    partiallyParsedString[1] = [[time componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "][0];
    partiallyParsedString[2] = [[time componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "][1];
    
    partiallyParsedString[0] = [[NSNumber alloc] initWithLong:[partiallyParsedString[0] integerValue]];
    partiallyParsedString[1] = [[NSNumber alloc] initWithLong:[partiallyParsedString[1] integerValue]];
    
    if ( [partiallyParsedString[2] isEqualToString:@"pm"] ) {
        if ( [partiallyParsedString[0] integerValue] != 12 ) {
            partiallyParsedString[0] = [[NSNumber alloc] initWithInt:[partiallyParsedString[0] intValue] + 12];
        }
    } else if ( [partiallyParsedString[0] integerValue] == 12 ) {
        partiallyParsedString[0] = [NSNumber numberWithInt:0];
    }
    
    [partiallyParsedString removeObjectAtIndex:2];
    
    return partiallyParsedString;
}

+ (NSArray*) timeIntervalBetweenTime:(NSArray *)time1 andTime:(NSArray *)time2
{
    
    long firstHour = [time1[0] integerValue];
    long firstMinute = [time1[1] integerValue];
    long firstSecond = 0;
    if ( [time1 count] == 3 ) {
        firstSecond = [time1[2] integerValue];
    }
    
    long secondHour = [time2[0] integerValue];
    long secondMinute = [time2[1] integerValue];
    long secondSecond = 0;
    if ( [time2 count] == 3 ) {
        secondSecond = [time2[2] integerValue];
    }
    
    
    long hourInterval = secondHour - firstHour;
    long minuteInterval = secondMinute - firstMinute;
    long secondInterval = secondSecond - firstSecond;
    
    
    
    long totalSeconds = (hourInterval * 3600) + (minuteInterval * 60) + secondInterval;
    
    
    long hoursBetween = (totalSeconds - (totalSeconds % 3600)) / 3600;
    
    totalSeconds -= (hoursBetween * 3600);
    long minutesBetween = (totalSeconds - (totalSeconds % 60)) / 60;
    
    totalSeconds -= (minutesBetween * 60);
    long secondsBetween = totalSeconds;
    
    
    
    NSMutableArray* interval = [[NSMutableArray alloc] initWithCapacity:2];
    [interval addObject:[NSNumber numberWithLong:hoursBetween]];
    [interval addObject:[NSNumber numberWithLong:minutesBetween]];
    [interval addObject:[NSNumber numberWithLong:secondsBetween]];
    
    return interval;
    
}

+ (NSArray*) daysOfTheWeek
{
    return @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
}


+ (long)timeIntervalInSecondsBetweenTime:(NSArray *)time1 andTime:(NSArray *)time2
{
    long firstHour = [time1[0] integerValue];
    long firstMinute = [time1[1] integerValue];
    long firstSecond = 0;
    if ( [time1 count] == 3 ) {
        firstSecond = [time1[2] integerValue];
    }
    
    long secondHour = [time2[0] integerValue];
    long secondMinute = [time2[1] integerValue];
    long secondSecond = 0;
    if ( [time2 count] == 3 ) {
        secondSecond = [time2[2] integerValue];
    }
    
    
    long hourInterval = secondHour - firstHour;
    long minuteInterval = secondMinute - firstMinute;
    long secondInterval = secondSecond - firstSecond;
    
    
    
    long totalSeconds = (hourInterval * 3600) + (minuteInterval * 60) + secondInterval;
    
    
    return totalSeconds;
    
    
}

+ (long) daysSinceJanuaryFirst
{
    NSArray *dayCount = @[@"0", @"31", @"59", @"90", @"120", @"151", @"181", @"212", @"243", @"273", @"304", @"334"];
    return ([self currentDayOfMonth] + [dayCount[[self currentMonth] - 1] integerValue]);
}

+ (long) currentYear
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *today = [NSDate date];
    NSString *todayString = [dateFormatter stringFromDate:today];
    NSArray *components = [todayString componentsSeparatedByString:@"/"];
    return [components[2] integerValue];
}

+ (long) currentMonth
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *today = [NSDate date];
    NSString *todayString = [dateFormatter stringFromDate:today];
    NSArray *components = [todayString componentsSeparatedByString:@"/"];
    return [components[1] integerValue];
}

+ (long) currentDayOfMonth
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *today = [NSDate date];
    NSString *todayString = [dateFormatter stringFromDate:today];
    NSArray *components = [todayString componentsSeparatedByString:@"/"];
    return [components[0] integerValue];
}

+ (NSString*) currentDayOfWeek
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString* dayOfWeek = [dateFormatter stringFromDate:[NSDate date]];
    return dayOfWeek;
}

+ (long) currentHour
{
    NSDate* now = [NSDate date];
    NSDateFormatter* newDateFormatter = [[NSDateFormatter alloc] init];
    [newDateFormatter setDateFormat: @"HH-mm"];
    [newDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString* currentTime = [newDateFormatter stringFromDate:now];
    NSArray* currentTimeArray = [currentTime componentsSeparatedByString:@"-"];
    
    return [currentTimeArray[0] integerValue];
    
    
}

+ (long) currentMinute
{
    NSDate* now = [NSDate date];
    NSDateFormatter* newDateFormatter = [[NSDateFormatter alloc] init];
    [newDateFormatter setDateFormat: @"HH-mm"];
    [newDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString* currentTime = [newDateFormatter stringFromDate:now];
    NSArray* currentTimeArray = [currentTime componentsSeparatedByString:@"-"];
    return [currentTimeArray[1] integerValue];
}

+ (long) currentSecond
{
    NSDate* now = [NSDate date];
    NSDateFormatter* newDateFormatter = [[NSDateFormatter alloc] init];
    [newDateFormatter setDateFormat: @"HH-mm-ss"];
    [newDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString* currentTime = [newDateFormatter stringFromDate:now];
    NSArray* currentTimeArray = [currentTime componentsSeparatedByString:@"-"];
    return [currentTimeArray[2] integerValue];
}

+ (NSArray*) currentTime
{
    NSNumber *hour = [NSNumber numberWithLong:[self currentHour]];
    NSNumber *minute = [NSNumber numberWithLong:[self currentMinute]];
    NSNumber *second = [NSNumber numberWithLong:[self currentSecond]];
    NSArray *currentTime = [[NSArray alloc] initWithObjects:hour, minute, second, nil];
    return currentTime;
}

+ (void) logTime
{
    NSLog(@"%ld:%ld:%ld", [self currentHour], [self currentMinute], [self currentSecond]);
}

+ (void) logTimeWithMessage:(NSString *)message
{
    NSLog(@"%ld:%ld:%ld     %@", [self currentHour], [self currentMinute], [self currentSecond], message);
}

+ (void) printFrame:(CGRect)frame withTitle:(NSString *)title
{
    if ( title ) {
        NSLog(@"Frame of %@: origin = (%f, %f); width = %f; height = %f", title, frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
    } else {
        NSLog(@"Frame: origin = (%f, %f); width = %f; height = %f", frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
    }
    
}



@end


@implementation UIColor (BARDToolkit)

+ (UIColor*)colorFromHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (BOOL) isEqualToColor:(UIColor *)otherColor {
    CGColorSpaceRef colorSpaceRGB = CGColorSpaceCreateDeviceRGB();
    
    UIColor *(^convertColorToRGBSpace)(UIColor*) = ^(UIColor *color) {
        if(CGColorSpaceGetModel(CGColorGetColorSpace(color.CGColor)) == kCGColorSpaceModelMonochrome) {
            const CGFloat *oldComponents = CGColorGetComponents(color.CGColor);
            CGFloat components[4] = {oldComponents[0], oldComponents[0], oldComponents[0], oldComponents[1]};
            return [UIColor colorWithCGColor:CGColorCreate(colorSpaceRGB, components)];
        } else
            return color;
    };
    
    UIColor *selfColor = convertColorToRGBSpace(self);
    otherColor = convertColorToRGBSpace(otherColor);
    CGColorSpaceRelease(colorSpaceRGB);
    
    return [selfColor isEqual:otherColor];
}

@end



//
//  SPDeepCopy.m
//
//  Created by Sherm Pendley on 3/15/09.
//


@implementation NSArray (SPDeepCopy)

- (NSArray*) deepCopy {
    unsigned int count = (int)[self count];
    id cArray[count];
    
    for (unsigned int i = 0; i < count; ++i) {
        id obj = [self objectAtIndex:i];
        if ([obj respondsToSelector:@selector(deepCopy)])
            cArray[i] = [obj deepCopy];
        else
            cArray[i] = [obj copy];
    }
    
    NSArray *ret = [NSArray arrayWithObjects:cArray count:count];
    
    // The newly-created array retained these, so now we need to balance the above copies
    
    return ret;
}
- (NSMutableArray*) mutableDeepCopy {
    unsigned int count = (int)[self count];
    id cArray[count];
    
    for (unsigned int i = 0; i < count; ++i) {
        id obj = [self objectAtIndex:i];
        
        // Try to do a deep mutable copy, if this object supports it
        if ([obj respondsToSelector:@selector(mutableDeepCopy)])
            cArray[i] = [obj mutableDeepCopy];
        
        // Then try a shallow mutable copy, if the object supports that
        else if ([obj respondsToSelector:@selector(mutableCopyWithZone:)])
            cArray[i] = [obj mutableCopy];
        
        // Next try to do a deep copy
        else if ([obj respondsToSelector:@selector(deepCopy)])
            cArray[i] = [obj deepCopy];
        
        // If all else fails, fall back to an ordinary copy
        else
            cArray[i] = [obj copy];
    }
    
    NSMutableArray *ret = [NSMutableArray arrayWithObjects:cArray count:count];
    
    
    return ret;
}

@end

@implementation NSDictionary (SPDeepCopy)

- (NSDictionary*) deepCopy {
    unsigned int count = (int)[self count];
    id cObjects[count];
    id cKeys[count];
    
    NSEnumerator *e = [self keyEnumerator];
    unsigned int i = 0;
    id thisKey;
    while ((thisKey = [e nextObject]) != nil) {
        id obj = [self objectForKey:thisKey];
        
        if ([obj respondsToSelector:@selector(deepCopy)])
            cObjects[i] = [obj deepCopy];
        else
            cObjects[i] = [obj copy];
        
        if ([thisKey respondsToSelector:@selector(deepCopy)])
            cKeys[i] = [thisKey deepCopy];
        else
            cKeys[i] = [thisKey copy];
        
        ++i;
    }
    
    NSDictionary *ret = [NSDictionary dictionaryWithObjects:cObjects forKeys:cKeys count:count];
    
    // The newly-created dictionary retained these, so now we need to balance the above copies
    
    return ret;
}
- (NSMutableDictionary*) mutableDeepCopy {
    unsigned int count = (int)[self count];
    id cObjects[count];
    id cKeys[count];
    
    NSEnumerator *e = [self keyEnumerator];
    unsigned int i = 0;
    id thisKey;
    while ((thisKey = [e nextObject]) != nil) {
        id obj = [self objectForKey:thisKey];
        
        // Try to do a deep mutable copy, if this object supports it
        if ([obj respondsToSelector:@selector(mutableDeepCopy)])
            cObjects[i] = [obj mutableDeepCopy];
        
        // Then try a shallow mutable copy, if the object supports that
        else if ([obj respondsToSelector:@selector(mutableCopyWithZone:)])
            cObjects[i] = [obj mutableCopy];
        
        // Next try to do a deep copy
        else if ([obj respondsToSelector:@selector(deepCopy)])
            cObjects[i] = [obj deepCopy];
        
        // If all else fails, fall back to an ordinary copy
        else
            cObjects[i] = [obj copy];
        
        // I don't think mutable keys make much sense, so just do an ordinary copy
        if ([thisKey respondsToSelector:@selector(deepCopy)])
            cKeys[i] = [thisKey deepCopy];
        else
            cKeys[i] = [thisKey copy];
        
        ++i;
    }
    
    NSMutableDictionary *ret = [NSMutableDictionary dictionaryWithObjects:cObjects forKeys:cKeys count:count];
    
    
    return ret;
}

@end