//
//  BARDNavigatedViewController.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 11/20/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BARDNavigationController;

@protocol BARDNavigatedViewController <NSObject>

@property (strong, nonatomic) BARDNavigationController* navigator;

- (void) testMethod;


@end
