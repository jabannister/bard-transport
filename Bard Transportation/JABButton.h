//
//  JABButton.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 6/7/14.
//  Copyright (c) 2014 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JABButton : UIView

@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) UILabel *textLabel;
@property (strong, nonatomic) UIImageView *image;
@property (nonatomic) BOOL active;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) id target;
@property (nonatomic) SEL action;


- (id) initWithFrame:(CGRect) frame identifier: (NSString*) identifier text: (NSString*) text image: (UIImage*) image target: (id) target action: (SEL) action;
- (id) initWithFrame:(CGRect)frame imageFrame: (CGRect) imageFrame identifier:(NSString *)identifier image:(UIImage *)image target: (id) target action: (SEL) action;

@end
