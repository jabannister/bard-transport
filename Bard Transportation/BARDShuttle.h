//
//  BARDShuttle.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 12/13/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BARDShuttle : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* subtitle;
@property (strong, nonatomic) NSString* iconName;

@end
