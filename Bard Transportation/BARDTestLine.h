//
//  BARDTestLine.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 5/7/14.
//  Copyright (c) 2014 Bard College. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BARDTestLine : NSObject

@property (strong, nonatomic) UIView *testLineView;
@property (strong, nonatomic) UIView *superview;

@property (nonatomic) BOOL vertical;
@property (nonatomic) double position;
@property (nonatomic) double width;
@property (strong, nonatomic) UIColor *color;




- (void) initializeViewWithSuperview: (UIView*) superview position: (double) position vertical: (BOOL) vertical;

- (void) changeVertical: (BOOL)vertical;
- (void) toggleVertical;
- (void) changePosition: (double) newPosition;
- (void) changeWidth: (double) newWidth;
- (void) changeColor: (UIColor*) newColor;


@end
