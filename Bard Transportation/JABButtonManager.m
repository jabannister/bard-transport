//
//  JABButtonManager.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 6/7/14.
//  Copyright (c) 2014 Bard College. All rights reserved.
//

#import "JABButtonManager.h"
#import "BARDScheduleSelectorViewController.h"

@implementation JABButtonManager


- (id) init
{
    self = [super init];
    
    if ( self ) {
        self.controller = nil;
        self.buttons = [[NSMutableDictionary alloc] initWithCapacity:0];
        self.buttonActive = NO;
        self.identifierOfActiveButton = nil;
    }
    
    return self;
}

- (id) initWithController:(BARDScheduleSelectorViewController *)controller
{
    self = [super init];
    
    if ( self ) {
        self.controller = controller;
        self.buttons = [[NSMutableDictionary alloc] initWithCapacity:0];
        self.buttonActive = NO;
        self.identifierOfActiveButton = nil;
    }
    
    return self;
}


- (void) addButtonWithFrame:(CGRect)frame identifier:(NSString *)identifier text:(NSString *)text image:(UIImage *)image target:(id)target action:(SEL)action
{
    JABButton *button = [[JABButton alloc] initWithFrame:frame identifier:identifier text:text image:image target:target action:action];
    if ( !self.buttons[identifier] ) {
        self.buttons[identifier] = button;
        [self.controller.view addSubview:button];
    } else {
        NSLog(@"There is already a button with the identifier %@", identifier);
    }
}

- (void) addButtonWithFrame:(CGRect)frame imageFrame:(CGRect)imageFrame identifier:(NSString *)identifier image:(UIImage *)image target:(id)target action:(SEL)action
{
    JABButton *button = [[JABButton alloc] initWithFrame:frame imageFrame:imageFrame identifier:identifier image:image target:target action:action];
    if ( !self.buttons[identifier] ) {
        self.buttons[identifier] = button;
        [self.controller.view addSubview:button];
    } else {
        NSLog(@"There is already a button with the identifier %@", identifier);
    }
}

- (void) removeButtonWithIdentifier:(NSString *)identifier
{
    if ( [self.buttons[identifier] active] ) {
        [self.buttons[identifier] setActive:NO];
        self.buttonActive = NO;
        self.identifierOfActiveButton = nil;
    }
    [self.buttons[identifier] removeFromSuperview];
    [self.buttons removeObjectForKey:identifier];
    
}


- (BOOL) activateLocation:(CGPoint)location
{
    
    for ( id key in self.buttons ) {
        JABButton *button = self.buttons[key];
        if ( [BARDToolkit rect:button.frame containsPoint:location] ) {
            button.active = YES;
            self.buttonActive = YES;
            self.identifierOfActiveButton = button.identifier;
            return YES;
        }
    }
    
    return NO;
}

- (void) completeActivationWithEndLocation:(CGPoint)location
{
    if ( self.buttonActive ) {
        JABButton *button = self.buttons[self.identifierOfActiveButton];
        if ( [BARDToolkit rect:button.frame containsPoint:location] ) {
            [button.target performSelector:button.action withObject:nil];
        }
        self.buttonActive = NO;
        self.identifierOfActiveButton = nil;
    }
}

- (void) deactivateAllButtons
{
    self.buttonActive = NO;
    self.identifierOfActiveButton = nil;
    for ( id key in self.buttons ) {
        JABButton *button = self.buttons[key];
        button.active = NO;
    }
}

@end
