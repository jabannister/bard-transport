//
//  JABButton.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 6/7/14.
//  Copyright (c) 2014 Bard College. All rights reserved.
//

#import "JABButton.h"

@implementation JABButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



- (id) initWithFrame:(CGRect)frame identifier:(NSString *)identifier text:(NSString *)text image:(UIImage *)image target:(id)target action:(SEL)action
{
    self = [super initWithFrame:frame];
    if ( self ) {
        
        if ( identifier ) {
            self.identifier = identifier;
        } else {
            self.identifier = @"unidentified";
        }
        
        self.active = NO;
        self.userInteractionEnabled = NO;
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:self.textLabel];
        [self addSubview:self.image];
        if ( text ) {
            self.textLabel.text = text;
            self.type = @"text";
        } else if ( image ) {
            self.image.image = image;
            self.type = @"image";
        } else {
            self.type = @"text";
        }
        
        self.target = target;
        self.action = action;
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame imageFrame:(CGRect)imageFrame identifier:(NSString *)identifier image:(UIImage *)image target:(id)target action:(SEL)action
{
    self = [super initWithFrame:frame];
    if ( self ) {
        
        if ( identifier ) {
            self.identifier = identifier;
        } else {
            self.identifier = @"unidentified";
        }
        
        self.active = NO;
        self.userInteractionEnabled = NO;
        
        self.image = [[UIImageView alloc] initWithFrame:imageFrame];
        self.image.image = image;
        self.type = @"image";
        [self addSubview:self.image];
        
        self.target = target;
        self.action = action;
        
    }
    return self;
}



@end
