
//
//  main.m
//  Bard Transportation
//
//  Created by Muhsin King on 9/20/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BARDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BARDAppDelegate class]));
    }
}
