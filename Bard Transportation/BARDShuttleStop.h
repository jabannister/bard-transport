//
//  BARDShuttleStop.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 9/27/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BARDShuttleStop : NSObject <MKAnnotation>
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* subtitle;
@property (strong, nonatomic) NSString* iconName;
@property (nonatomic) int index;



+ (NSArray*) shuttleStopNames;
+ (NSArray*) allShuttleStopNames;
- (void) setCoordinate:(CLLocationCoordinate2D)newCoordinate;
- (id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString*) initialTitle andSubtitle:(NSString*) initialSubtitle;
@end
