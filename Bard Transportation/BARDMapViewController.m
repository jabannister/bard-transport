//
//  BARDMapViewController.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 9/20/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <MapKit/Mapkit.h>
#import "BARDMapViewController.h"
#import "BARDShuttleStop.h"
#import "BARDNavigationController.h"
#import "BARD2DVector.h"

#define LATVAL latitude
#define LONGVAL longitude
#define THE_SPAN 10

@interface BARDMapViewController () {
    
    //State Booleans
    BOOL backButtonPressed;
    
    
    //Useful Frames
    CGRect offScreenFrame;
    CGRect onScreenFrame;
    CGRect menuFrame;

    
    //Touch Locations
    CGPoint initialTouchLocation;
    CGPoint finalTouchLocation;
    
    
    //Gesture Information
    double deltaX;
    double deltaY;
    
    double previousDeltaX;
    double previousDeltaY;
}

@property (nonatomic) BARD2DVector* initialCenter;
@property (nonatomic) double initialRadius;
@property (nonatomic) int methodCallCount;

- (void) removeAllAnnotations;
/**
 Slides menu off screen when pan gesture starts left of menu.
 */

@end

@implementation BARDMapViewController


- (id) init
{
    self = [super init];
    
    if (self) {
        
    }
    
    return self;
    
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
    }
    
    return self;
}


#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSLog(@"Map View Did Load");
    
    
    //Initial State Booleans Set
    backButtonPressed = NO;
    
    
    //Initialize Useful Frames
    onScreenFrame = CGRectMake(0, 0, screenWidth, screenHeight);
    offScreenFrame = CGRectMake(screenWidth, 0, screenWidth, screenHeight);
    menuFrame = CGRectMake((self.navigator.menuController.menu.frame.size.width - screenWidth) + 15, 0, screenWidth, screenHeight);
    
    
    
    
    
    // Ensure that all visual components appear where they are supposed, regardless of future changes to auto-layout
    
    self.map.frame = onScreenFrame;
    
    
    CGRect campusButtonFrame = self.backToCampusButton.frame;
    campusButtonFrame.origin.x = screenWidth + 37;
    campusButtonFrame.origin.y = screenHeight - 50;
    self.backToCampusButton.frame = campusButtonFrame;
    
    
    CGRect switchFrame = self.followShuttleToggle.frame;
    switchFrame.origin.x = screenWidth - 90;
    switchFrame.origin.y = screenHeight - 100;
    self.followShuttleToggle.frame = switchFrame;
    
    
    CGRect switchLabelFrame = self.followShuttleToggleLabel.frame;
    switchLabelFrame.origin.x = screenWidth - 120;
    switchLabelFrame.origin.y = screenHeight - 50;
    self.followShuttleToggleLabel.frame = switchLabelFrame;
    
    
    
    // Set map delegate
    self.map.delegate = self;
    
    
    // This code causes crash if run on iOS 6
    if ( iOS >= 7.0 ) {
        self.map.rotateEnabled = NO;
        self.map.pitchEnabled = NO;
    }
    
    
    self.shadowColor = @"B5B4AE";
    

    // Create text that appears if GPS is not available
    self.noGPSNotice = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth * (1.0/5.0), 100, screenWidth * (3.0/5.0), 140)];
    self.noGPSNotice.textAlignment = NSTextAlignmentCenter;
    self.noGPSNotice.font = [UIFont fontWithName:@"Avenir" size:20];
    self.noGPSNotice.lineBreakMode = NSLineBreakByWordWrapping;
    self.noGPSNotice.numberOfLines = 0;
    self.noGPSNotice.text = @"GPS is not currently available, check the menu for details.";
    self.noGPSNotice.layer.opacity = 0;
    [self.view addSubview:self.noGPSNotice];
    
    
//    CAGradientLayer *shadow = [CAGradientLayer layer];
//    shadow.frame = CGRectMake(320, 0, 3, self.view.frame.size.height);
//    shadow.startPoint = CGPointMake(1.0, 0.5);
//    shadow.endPoint = CGPointMake(0, 0.5);
//    shadow.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:0.0 alpha:0.4f] CGColor], (id)[[UIColor colorFromHexString:self.shadowColor] CGColor], nil];
//    [self.menuController.view.layer addSublayer:shadow];
    
    //-73.88413   42.14773
    
    self.mainShuttleStopPoints = [[NSMutableArray alloc] initWithCapacity:5];
    [self.mainShuttleStopPoints addObject:[NSValue valueWithCGPoint:CGPointMake(42.059900, -73.9100421)]];
    [self.mainShuttleStopPoints addObject:[NSValue valueWithCGPoint:CGPointMake(42.0287648, -73.9046179)]];
    [self.mainShuttleStopPoints addObject:[NSValue valueWithCGPoint:CGPointMake(42.0220182, -73.9083328)]];
    [self.mainShuttleStopPoints addObject:[NSValue valueWithCGPoint:CGPointMake(41.9946578, -73.8760999)]];
    [self.mainShuttleStopPoints addObject:[NSValue valueWithCGPoint:CGPointMake(41.9799000, -73.8808082)]];
    
    
    //Create Gesture Recognizer
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressDetected:)];
    self.longPressRecognizer = longPressRecognizer;
    longPressRecognizer.delegate = self;
    longPressRecognizer.minimumPressDuration = 0.01;
    longPressRecognizer.allowableMovement = screenHeight;
    [self.map addGestureRecognizer:longPressRecognizer];
    
    
    
    BARDShuttleStop* tivoli = [[BARDShuttleStop alloc] initWithCoordinate:CLLocationCoordinate2DMake(42.0599000, -73.9100421) title:@"Tivoli" andSubtitle:@"Shuttle Stop"];
    BARDShuttleStop* robbins = [[BARDShuttleStop alloc] initWithCoordinate:CLLocationCoordinate2DMake(42.0287648, -73.9046179) title:@"Robbins" andSubtitle:@"Shuttle Stop"];
    BARDShuttleStop* kline = [[BARDShuttleStop alloc] initWithCoordinate:CLLocationCoordinate2DMake(42.0220182, -73.9083328) title:@"Kline" andSubtitle:@"Shuttle Stop"];
    BARDShuttleStop* redhook = [[BARDShuttleStop alloc] initWithCoordinate:CLLocationCoordinate2DMake(41.9946578, -73.8760999) title:@"Red Hook" andSubtitle:@"Shuttle Stop"];
    BARDShuttleStop* hannaford = [[BARDShuttleStop alloc] initWithCoordinate:CLLocationCoordinate2DMake(41.9799000, -73.8808082) title:@"Hannaford" andSubtitle:@"Shuttle Stop"];
    [tivoli setIconName:@"shuttleStopSquareRegularSmaller.png"];
    [tivoli setIndex:0];
    [robbins setIconName:@"shuttleStopSquareRegularSmaller.png"];
    [robbins setIndex:1];
    [kline setIconName:@"shuttleStopSquareRegularSmaller.png"];
    [kline setIndex:2];
    [redhook setIconName:@"shuttleStopSquareRegularSmaller.png"];
    [redhook setIndex:3];
    [hannaford setIconName:@"shuttleStopSquareRegularSmaller.png"];
    [hannaford setIndex:4];
    
    
    //Create Shuttle Object
    self.shuttle = [[BARDShuttleStop alloc] init];
    self.shuttle.coordinate = CLLocationCoordinate2DMake(42.058, -73.900);
    self.shuttle.title = @"Bard Shuttle";
    self.shuttle.subtitle = @"GPS Coming Soon";
    self.shuttle.iconName = @"shuttleIcon.png";

    
    
    //Add Map Annotations
    [self generateStaticAnnotations];
    
    
    
    //Hiding GPS for 2.5 - Delete when GPS is wanted
    [self.map removeAnnotation:self.shuttle];
    
    self.followShuttleToggle.layer.opacity = 0;
    self.followShuttleToggleLabel.layer.opacity = 0;
    //Hiding GPS for 2.5
    
    
    
    //Create initial map region
    CLLocationCoordinate2D initialCenter = CLLocationCoordinate2DMake(42.0200102, -73.8983328);
    MKCoordinateSpan initialSpan;
    initialSpan.latitudeDelta = 0.095;
    initialSpan.longitudeDelta = (screenWidth/screenHeight)*initialSpan.latitudeDelta;
    self.initialCenter = [[BARD2DVector alloc] initWithX:initialCenter.longitude andY:initialCenter.latitude];
    self.initialRegion = MKCoordinateRegionMake(initialCenter, initialSpan);
    self.initialRadius = .5;
    
    //    (center, span);
    //    center + span.latitudeDelta = right edge
    //    center - span.latitudeDelta = left edge
    //    center + span.longitudedelta = upper edge
    //    center - span.longitudedelta = lower edge
    
    
    
//    CGRect testerFrame = CGRectMake(0, 55, 320, 200);
//    UIView* tester = [[UIView alloc] initWithFrame:testerFrame];
//    [self.view addSubview:tester];
//    [tester setBackgroundColor:[UIColor lightGrayColor]];
//    [tester setAlpha:0.5];
    
    

    //Hiding GPS for 2.5
//    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(moveShuttle) userInfo:self repeats:YES];
    //Hiding GPS for 2.5
    
    NSLog(@"Map View Did Load Finished");
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
//    UITapGestureRecognizer* tapper = [[UITapGestureRecognizer alloc] init];
//    [self selectTimes:tapper];
    
    
    [self.map setRegion:self.initialRegion];
    
}

- (void) testMethod
{
    self.shuttle.subtitle = @"Testing...";
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self backToBard:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Internal



- (void) generateStaticAnnotations
{
    
    for ( int i = 0; i < [self.navigator.shuttleStops count]; i++ ) {
        
        [self.map addAnnotation:self.navigator.shuttleStops[i]];
        
    }
    
    [self.map addAnnotation:self.shuttle];
    
}

- (void) removeAllAnnotations
{
    for ( BARDShuttleStop* stop in self.navigator.shuttleStops )
    {
        [self.map removeAnnotation:stop];
    }
    
}


- (void) moveShuttle
{
    if ( self.navigator.newLocation ) {
        if ( self.navigator.latestLocationTweet.latitude == 1000 && self.navigator.latestLocationTweet.longitude == 1000 ) {
            [UIView animateWithDuration:0.3 animations:^{
                self.noGPSNotice.layer.opacity = 1;
            }];
            [self.map removeAnnotation:self.shuttle];
            self.shuttleDisplayed = NO;
        } else {
            [UIView animateWithDuration:0.3 animations:^{
                self.noGPSNotice.layer.opacity = 0;
            }];
            [self.map addAnnotation:self.shuttle];
            if ( self.followShuttleToggle.on ) {
                [self.map setCenterCoordinate:self.shuttle.coordinate animated:YES];
                MKCoordinateRegion currentRegion = self.map.region;
                currentRegion.center = self.shuttle.coordinate;
                currentRegion.span.latitudeDelta = 0.0001;
                currentRegion.span.longitudeDelta = 0.0001;
                [self.map setRegion:currentRegion animated:YES];
            }
            self.shuttle.coordinate = self.navigator.latestLocationTweet;
        }
        self.navigator.newLocation = NO;
    }
    self.shuttle.subtitle = [NSString stringWithFormat:@"%ld seconds ago", [BARDToolkit timeIntervalInSecondsBetweenTime:self.navigator.timeOfLatestLocationTweet andTime:[BARDToolkit currentTime]]];
}


#pragma mark - Actions

- (IBAction)backToBard:(id)sender
{
    [self.map setRegion:self.initialRegion animated:YES];
}




- (void) longPressDetected:(UIGestureRecognizer *)longPressRecognizer
{
    CGPoint location = [longPressRecognizer locationInView:self.navigator.view];

    if (longPressRecognizer.state == UIGestureRecognizerStateBegan) {
        
        if ( self.navigator.touchInProgress == 0 ) {
            
            self.navigator.touchInProgress = 3;
            
            backButtonPressed = NO;
            initialTouchLocation = location;
            previousDeltaY = 0;
            previousDeltaX = 0;
            
            if ( [BARDToolkit rect:CGRectMake(0, 20, 69, 53) containsPoint:location] ) {
                backButtonPressed = YES;
            }
        }
        
    } else if ( longPressRecognizer.state == UIGestureRecognizerStateChanged ) {
        
        if ( self.navigator.touchInProgress == 3 ) {
            if ( !backButtonPressed ) {
                deltaX = location.x - initialTouchLocation.x;
                deltaY = location.y - initialTouchLocation.y;
                
                
                double distanceMovedSincePreviousCallToThisMethod = deltaX - previousDeltaX;
                
                
                BARDScheduleSelectorViewController* selector = self.navigator.scheduleSelectorController;
                CGRect intermediateSelectorFrame = [selector view].frame;
                CGRect intermediateSelfFrame = self.view.frame;
                
                
                intermediateSelectorFrame.origin.x += distanceMovedSincePreviousCallToThisMethod;
                intermediateSelfFrame.origin.x += distanceMovedSincePreviousCallToThisMethod/partialSlideFraction;
                
                
                [selector.view setFrame:intermediateSelectorFrame];
                [self.view setFrame:intermediateSelfFrame];
                
                
                
                previousDeltaY = deltaY;
                previousDeltaX = deltaX;
            }
        }
        
    } else if (longPressRecognizer.state == UIGestureRecognizerStateEnded) {
        
        if ( self.navigator.touchInProgress == 3 ) {
            
            self.navigator.touchInProgress = 0;
            
            finalTouchLocation = location;
            deltaX = finalTouchLocation.x - initialTouchLocation.x;
            deltaY = finalTouchLocation.y - initialTouchLocation.y;
            
            if ( !backButtonPressed ) {
                if ( finalTouchLocation.x  > 125 ) {
                    
                    [self.navigator openSelector];
                    
                } else {
                    [self.navigator openMap];
                }
            } else {
                if ( [BARDToolkit rect:CGRectMake(0, 20, 69, 53) containsPoint:location] ) {
                    
                    [self.navigator openSelector];
                    
                }
            }
        }
    }
}



#pragma mark - MKMapViewDelegate

-(MKAnnotationView*) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[BARDShuttleStop class]])
    {
        BARDShuttleStop* anotation = annotation;
        // Try to dequeue an existing pin view first.
        MKAnnotationView*    shuttleStopView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"ShuttleStopIconAnnotationView"];
        
        if (!shuttleStopView)
        {
            // If an existing pin view was not available, create one.
            shuttleStopView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ShuttleStopIconAnnotationView"];
            shuttleStopView.canShowCallout = YES;
            
            // Add a detail disclosure button to the callout.
            //            UIButton* rightButton = [UIButton buttonWithType:
            //                                     UIButtonTypeDetailDisclosure];
            //            [rightButton addTarget:self action:@selector(myShowDetailsMethod:)
            //                  forControlEvents:UIControlEventTouchUpInside];
            //            shuttleStopView.rightCalloutAccessoryView = rightButton;
        }
        else
        {
            shuttleStopView.annotation = annotation;
        }
        [shuttleStopView setImage:[UIImage imageNamed:anotation.iconName]];
        
        return shuttleStopView;
    }
    
    return nil;
}

- (void) mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    self.methodCallCount++;
    MKCoordinateRegion newRegion = mapView.region;
    
    //    BARD2DVector* rectCenter = [[BARD2DVector alloc] initWithX:newRegion.center.longitude andY:newRegion.center.latitude];
    
    BARD2DVector* upperLeftCorner = [[BARD2DVector alloc] initWithX:(newRegion.center.longitude - newRegion.span.longitudeDelta) andY:(newRegion.center.latitude + newRegion.span.latitudeDelta)];
    
    BARD2DVector* upperRightCorner = [[BARD2DVector alloc] initWithX:(newRegion.center.longitude + newRegion.span.longitudeDelta) andY:(newRegion.center.latitude + newRegion.span.latitudeDelta)];
    
    BARD2DVector* lowerRightCorner = [[BARD2DVector alloc] initWithX:(newRegion.center.longitude + newRegion.span.longitudeDelta) andY:(newRegion.center.latitude - newRegion.span.latitudeDelta)];
    
    BARD2DVector* lowerLeftCorner = [[BARD2DVector alloc] initWithX:(newRegion.center.longitude - newRegion.span.longitudeDelta) andY:(newRegion.center.latitude - newRegion.span.latitudeDelta)];
    
    NSMutableArray* outsideCirlce = [[NSMutableArray alloc] initWithArray:@[@NO, @NO, @NO, @NO]];
    
    
    
    if(sqrt((pow([upperLeftCorner minusCopy:self.initialCenter].x, 2) + pow([upperLeftCorner minusCopy:self.initialCenter].y, 2))) / self.initialRadius > 1) {
        outsideCirlce[0] = @YES;
    }
    if(sqrt((pow([upperRightCorner minusCopy:self.initialCenter].x, 2) + pow([upperRightCorner minusCopy:self.initialCenter].y, 2))) / self.initialRadius > 1) {
        outsideCirlce[1] = @YES;
    }
    if(sqrt((pow([lowerRightCorner minusCopy:self.initialCenter].x, 2) + pow([lowerRightCorner minusCopy:self.initialCenter].y, 2))) / self.initialRadius > 1) {
        outsideCirlce[2] = @YES;
    }
    if(sqrt((pow([lowerLeftCorner minusCopy:self.initialCenter].x, 2) + pow([lowerLeftCorner minusCopy:self.initialCenter].y, 2))) / self.initialRadius > 1) {
        outsideCirlce[3] = @YES;
    }
    
    if([outsideCirlce[0] isEqualToNumber:[[NSNumber alloc] initWithInt:1]] || [outsideCirlce[1] isEqualToNumber:[[NSNumber alloc] initWithInt:1]] || [outsideCirlce[2] isEqualToNumber:[[NSNumber alloc] initWithInt:1]] || [outsideCirlce[3] isEqualToNumber:[[NSNumber alloc] initWithInt:1]]) {
        [mapView setRegion:self.initialRegion animated:YES];
    }
    
    //    if(newRegion.span.latitudeDelta > self.initialRegion.span.latitudeDelta || newRegion.span.longitudeDelta > self.initialRegion.span.longitudeDelta) {
    //        [mapView setRegion:self.initialRegion animated:YES];
    //    } else {
    //
    //        //calculate the latitude values of the top and bottom edges of the screen
    //        CLLocationDegrees upperEdge = newRegion.center.latitude + newRegion.span.latitudeDelta;
    //        if(upperEdge > upperLimit) {
    //            CLLocationCoordinate2D newCenter =  CLLocationCoordinate2DMake((-1)*(mapView.region.center.latitude - (newRegion.center.latitude + newRegion.span.latitudeDelta - upperLimit)), mapView.region.center.longitude);
    //            [mapView setRegion:MKCoordinateRegionMake(newCenter, mapView.region.span) animated:YES];
    //        } else if(newRegion.center.latitude - newRegion.span.latitudeDelta > lowerLimit) {
    //
    //        } else if(newRegion.center.longitude + newRegion.span.longitudeDelta > rightLimit) {
    //
    //        } else if(newRegion.center.longitude - newRegion.span.longitudeDelta > leftLimit) {
    //
    //        }
    //    }
    
    
    
}


- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint location = [touch locationInView:self.navigator.view];
    CGRect backButtonFrame = CGRectMake(0, 20, 69, 53);
    CGRect backPanFrame = CGRectMake(0, 0, backPanTriggerThreshold, self.view.frame.size.height);
    
    if ( [BARDToolkit rect:backButtonFrame containsPoint:location] ) {
        return YES;
    } else if ( [BARDToolkit rect:backPanFrame containsPoint:location] ) {
        return YES;
    }
    
    
    return NO;
}


@end
