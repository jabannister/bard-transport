//
//  JABButtonManager.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 6/7/14.
//  Copyright (c) 2014 Bard College. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JABButton.h"
#import "BARDToolkit.h"

@class BARDScheduleSelectorViewController;

@interface JABButtonManager : NSObject

@property (strong, nonatomic) BARDScheduleSelectorViewController *controller;
@property (strong, nonatomic) NSMutableDictionary *buttons;
@property (nonatomic) BOOL buttonActive;
@property (strong, nonatomic) NSString *identifierOfActiveButton;



- (id) initWithController: (BARDScheduleSelectorViewController*) controller;
- (void) addButtonWithFrame: (CGRect) frame identifier: (NSString*) identifier text: (NSString*) text image: (UIImage*) image target: (id) target action: (SEL) action;
- (void) addButtonWithFrame:(CGRect)frame imageFrame: (CGRect)imageFrame identifier:(NSString *)identifier image:(UIImage *)image target: (id) target action: (SEL) action;
- (void) removeButtonWithIdentifier: (NSString*) identifier;

- (BOOL) activateLocation: (CGPoint) location;
- (void) completeActivationWithEndLocation: (CGPoint) location;

- (void) deactivateAllButtons;


@end
