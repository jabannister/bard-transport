//
//  BARDAppDelegate.m
//  Bard Transportation
//
//  Created by Muhsin King on 9/20/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDAppDelegate.h"
#import "BARDNavigationController.h"

@implementation BARDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    return YES;
    
    // Not necessary. iOS takes care of everything since we use a storyboard.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    NSLog(@"resigning");
    
    if ( self.navigator ) {
        if ( self.navigator.menuOpen ) {
            [self.navigator openMenu];
        } else if ( self.navigator.mapOpen ) {
            [self.navigator openMap];
        } else {
            [self.navigator openSelector];
            if ( self.navigator.scheduleSelectorController.scheduleController ) {
                if ( self.navigator.scheduleSelectorController.scheduleOpen ) {
                    [self.navigator.scheduleSelectorController openSchedule];
                }
            }
        }
    }
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
