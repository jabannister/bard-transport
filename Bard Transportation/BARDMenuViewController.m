//
//  BARDMenuViewController.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/11/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDMenuViewController.h"
#import "BARDNavigationController.h"
#import "BARDMapViewController.h"

@interface BARDMenuViewController ()
{
    double mostRecentMenuHandleYValue;
    double panGestureStartXValue;
    double panGestureEndXValue;
    double panGestureIntermediateXValue;
    double panGestureDeltaX;
    double panGestureStartYValue;
    double panGestureEndYValue;
    double panGestureDeltaY;
    double panGestureIntermediateYValue;
    
    double menuNegativeOffset;
    double menuWidth;
    
    double menuSpacing;
    double paneCornerRadius;
    
    int tracker;
}

@end

@implementation BARDMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Menu View Did Load");
    // Do any additional setup after loading the view.
    
    
    tracker = 0;
    
    
    
    
    
    self.gradientLayer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    self.gradientLayer.userInteractionEnabled = NO;
    
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.navigator.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor colorFromHexString:[BARDToolkit hexcodeFromR:30 G:30 B:30]] CGColor], [[UIColor clearColor] CGColor], nil];
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    NSArray *locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.05], [NSNumber numberWithFloat:0.06], nil];
    
    gradient.locations = locations;
    
    [self.gradientLayer.layer insertSublayer:gradient atIndex:0];
    self.gradientLayer.frame = self.navigator.closedMenuSelectorShadowFrame;
    [self.view addSubview:self.gradientLayer];
    
    
    self.menu.contentInset = self.menu.scrollIndicatorInsets;
    self.menu.backgroundColor = [UIColor clearColor];
    
    menuSpacing = 15;
    paneCornerRadius = 10;
    
    menuNegativeOffset = self.menu.frame.size.width - 320;
    menuWidth = self.menu.frame.size.width;
    
    
    CGRect smartTimesFrame = self.smartTimesPane.frame;
    smartTimesFrame.size.width = self.visibleRange - (menuSpacing * 2) - self.navigator.shadowWidth;
    smartTimesFrame.origin.y = 15 + menuSpacing;
    self.smartTimesPane.frame = smartTimesFrame;
    
    CGRect menuPaneFrame = self.menuPane.frame;
    menuPaneFrame.size.width = self.visibleRange - (menuSpacing * 2) - self.navigator.shadowWidth;
    menuPaneFrame.size.height = screenHeight - (3 * menuSpacing) - self.smartTimesPane.frame.size.height -  20;
    self.menuPane.frame = menuPaneFrame;
    
    self.menuPane.clipsToBounds = YES;
    
    
    self.backToMainMenuButton.layer.opacity = 0;
    self.backToMainMenuButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.backToMainMenuButton.titleLabel.numberOfLines = 0;
    self.backToMainMenuButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    //    self.backToMainMenuButton.titleLabel.font = [UIFont fontWithName:@"Avenir" size:16];
    
    self.mainMenuTitle.textAlignment = NSTextAlignmentCenter;
    self.mainMenuTitle.lineBreakMode = NSLineBreakByWordWrapping;
    self.mainMenuTitle.numberOfLines = 0;
    self.mainMenuTitle.font = [UIFont fontWithName:@"Avenir" size:15];
    
    CGRect headerDividerFrame = self.menuHeaderDivider.frame;
    headerDividerFrame.size.width = self.menuPane.frame.size.width;
    headerDividerFrame.size.height = 2;
    self.menuHeaderDivider.frame = headerDividerFrame;
    
    self.smartTimesErrorMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.smartTimesErrorMessage.numberOfLines = 0;
    self.smartTimesErrorMessage.textAlignment = NSTextAlignmentCenter;
    self.smartTimesErrorMessage.font = [UIFont fontWithName:@"Avenir" size:14];
    
    CGRect smartTimesDividerFrame = self.smartTimesDivider.frame;
    smartTimesDividerFrame.size.width = self.smartTimesPane.frame.size.width - 16;
    smartTimesDividerFrame.origin.x = 8;
    self.smartTimesDivider.frame = smartTimesDividerFrame;
    
    self.northMessage.frame = CGRectMake(16, 26, 200, 50);
    self.northMessage.textAlignment = NSTextAlignmentCenter;
    self.northMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.northMessage.numberOfLines = 0;
    self.northMessage.font = [UIFont fontWithName:@"Avenir" size:14];
    
    self.southMessage.frame = CGRectMake(16, 82, 200, 50);
    self.southMessage.textAlignment = NSTextAlignmentCenter;
    self.southMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.southMessage.numberOfLines = 0;
    self.southMessage.font = [UIFont fontWithName:@"Avenir" size:14];
    
    
    self.smartTimesTapField.frame = self.smartTimesPane.frame;
    self.smartTimesTapField.layer.cornerRadius = 10;
    self.smartTimesTapField.backgroundColor = [UIColor clearColor];
    self.smartTimesTapField.userInteractionEnabled = YES;
    [self.view addSubview:self.smartTimesTapField];
    
    
    self.updatingLocationIndicatorView.layer.opacity = 0;
    self.updatingLabel.layer.opacity = 0;
    
    int widthOfMenuMessagePadding = 3;
    
    
    CGRect menuMessageFrame = CGRectMake(widthOfMenuMessagePadding, self.menuHeaderDivider.frame.origin.y + self.menuHeaderDivider.frame.size.height, self.menuPane.frame.size.width - (2*widthOfMenuMessagePadding), 0);
    self.menuItemMessage = [[UITextView alloc] initWithFrame:menuMessageFrame];
    self.menuItemMessage.textAlignment = NSTextAlignmentCenter;
    self.menuItemMessage.editable = NO;
    [self.menuItemMessage setDataDetectorTypes:UIDataDetectorTypePhoneNumber | UIDataDetectorTypeLink];
    self.menuItemMessage.showsVerticalScrollIndicator = NO;
    self.menuItemMessage.layer.opacity = 0;
    self.menuItemMessage.font = [UIFont fontWithName:@"Avenir" size:14];
    [self.menuPane addSubview:self.menuItemMessage];
    
    
    UILongPressGestureRecognizer* smartTimesTapRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(smartTimesPaneTouched:)];
    smartTimesTapRecognizer.minimumPressDuration = 0.01;
    [self.smartTimesTapField addGestureRecognizer:smartTimesTapRecognizer];
    
    [self updateSmartTimesPaneSentFromLocationManager:NO];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkIfUpdateNecessary) userInfo:self repeats:YES];
    
    self.smartTimesPane.layer.cornerRadius = paneCornerRadius;
    self.menuPane.layer.cornerRadius = paneCornerRadius;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) checkIfUpdateNecessary
{
    //    NSLog(@"Checking if Smart Times Update is necessary...");
    if ( [BARDToolkit currentSecond] == 0 ) {
        [self updateSmartTimesPaneSentFromLocationManager:NO];
    } else if ( self.hourOfMostRecentUpdate != [BARDToolkit currentHour] ) {
        [self updateSmartTimesPaneSentFromLocationManager:NO];
    } else {
        if ( self.minuteOfMostRecentUpdate != [BARDToolkit currentMinute] ) {
            [self updateSmartTimesPaneSentFromLocationManager:NO];
        }
    }
}

- (void) smartTimesPaneTouched:(UILongPressGestureRecognizer *)gestureRecognizer
{
    [self updateSmartTimesPaneSentFromLocationManager:NO];
    if ( gestureRecognizer.state == UIGestureRecognizerStateBegan ) {
        
        [self startLocationUpdateAnimation];
        [self performSelector:@selector(stopLocationUpdateAnimation) withObject:nil afterDelay:1];
    }
}

- (void) startLocationUpdateAnimation
{
    self.updatingLocationIndicatorView.layer.opacity = 1;
    self.smartTimesTapField.backgroundColor = [UIColor blackColor];
    self.smartTimesTapField.layer.opacity = 0.5;
    self.updatingLabel.layer.opacity = 1;
    [UIView transitionWithView:self.smartTimesTapField duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        
    }completion:nil];
    [self.updatingLocationIndicatorView startAnimating];
}

- (void) stopLocationUpdateAnimation
{
    self.updatingLocationIndicatorView.layer.opacity = 0;
    self.smartTimesTapField.backgroundColor = [UIColor clearColor];
    self.smartTimesTapField.layer.opacity = 1;
    self.updatingLabel.layer.opacity = 0;
    [UIView transitionWithView:self.smartTimesTapField duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        
    }completion:nil];
    [self.updatingLocationIndicatorView stopAnimating];
}


- (void) test
{
    
    //Check if location data is available
    if ( !self.navigator.mostRecentUserLocationIsValid ) {
        if ( !self.noLocationDataMessageDisplayed ) {
            [self displayNoLocationDataMessage];
        }
        return;
    }
    
    
    //Get user's current location (statement will only be reached if location is valid)
    CLLocation* location = self.navigator.mostRecentUserLocation;
    CGPoint currentLocationPoint  = CGPointMake(location.coordinate.latitude, location.coordinate.longitude);
    
    
    //Check if user is too far from campus to use Smart Times and display message if so
    if ( [BARDToolkit distanceFromPoint:currentLocationPoint toClosestPointInArray:self.navigator.mapController.mainShuttleStopPoints] > 0.1 && !self.navigator.smartTimesUnlocked ) {
        if ( !self.tooFarFromCampusMessageDisplayed ) {
            [self displayTooFarFromCampusMessage];
        }
        return;
    }
    
    
    //Find stop that is closest to user
    
    NSMutableArray *stopLocations = [[NSMutableArray alloc] initWithCapacity:13];
    
    for ( int i = 0; i < [self.navigator.shuttleStops count]; i++ ) {
        CLLocationCoordinate2D coordinate = [self.navigator.shuttleStops[i] coordinate];
        [stopLocations addObject:[NSValue valueWithCGPoint:CGPointMake(coordinate.latitude, coordinate.longitude)]];
    }
    
    int closestStopIndex = [BARDToolkit indexOfPointInArray:stopLocations closestToPoint:currentLocationPoint];
    
    BARDShuttleStop* closestStop = self.navigator.shuttleStops[closestStopIndex];
    
    
    
    
}


- (void) updateSmartTimesPaneSentFromLocationManager:(BOOL)sentFromLocationManager
{
    
    
    if ( !sentFromLocationManager ) {
        [self.navigator forceLocationUpdate];
    }
    
    
    
    
    
    if ( self.navigator.mostRecentUserLocationIsValid ) {
        CLLocation* location = self.navigator.mostRecentUserLocation;
        
        
        CGPoint currentLocationPoint  = CGPointMake(location.coordinate.latitude, location.coordinate.longitude);
        
        
        //        CGPoint currentLocationPoint;
        
        //Red Hook Point
        //        currentLocationPoint = CGPointMake(41.9946578, -73.8760999);
        
        //        if ( !self.navigator.test ) {
        //            //Tivoli point
        //            currentLocationPoint = CGPointMake(42.0593757, -73.9096421);
        //
        //            //Hannaford Point
        ////            currentLocationPoint = CGPointMake(41.9808392, -73.8811182);
        //
        ////
        //        } else {
        //            //Kline point
        //            currentLocationPoint  = CGPointMake(42.0220182, -73.9083328);
        //        }
        
        if ( [BARDToolkit distanceFromPoint:currentLocationPoint toClosestPointInArray:self.navigator.mapController.mainShuttleStopPoints] < 0.1 || self.navigator.smartTimesUnlocked ) {
            

            NSLog(@"Updating Smart Times Pane...");
            
            
            
            if ( [self.navigator.scheduleOptions isEqualToString:@"summer"] ) {
                if ( [[BARDToolkit currentDayOfWeek] isEqualToString:@"Saturday"] || [[BARDToolkit currentDayOfWeek] isEqualToString:@"Sunday"] ) {
                    [self displayNoWeekendSummerShuttlesMessage];
                    return;
                }
            }
            
            
            
            self.hourOfMostRecentUpdate = [BARDToolkit currentHour];
            self.minuteOfMostRecentUpdate = [BARDToolkit currentMinute];
            
            
            //Find stop that is closest to user
            
            NSMutableArray *stopLocations = [[NSMutableArray alloc] initWithCapacity:13];
            
            for ( int i = 0; i < [self.navigator.shuttleStops count]; i++ ) {
                CLLocationCoordinate2D coordinate = [self.navigator.shuttleStops[i] coordinate];
                [stopLocations addObject:[NSValue valueWithCGPoint:CGPointMake(coordinate.latitude, coordinate.longitude)]];
            }
            
            int closestStopIndex = [BARDToolkit indexOfPointInArray:stopLocations closestToPoint:currentLocationPoint];
            
            BARDShuttleStop* closestStop = self.navigator.shuttleStops[closestStopIndex];
            
            
            
            NSString* dayOfWeek = [BARDToolkit currentDayOfWeek];
            
            long currentHour = [BARDToolkit currentHour];
            long currentMinute = [BARDToolkit currentMinute];
            
            //            long currentHour = 19;
            //            long currentMinute = 36;
            
            
            if ( currentHour < dateRollOverHour ) {
                if ( ![dayOfWeek isEqualToString:@"Monday"] ) {
                    dayOfWeek = [BARDToolkit daysOfWeekCapitalized][[BARDToolkit indexOfString:dayOfWeek inArray:[BARDToolkit daysOfWeekCapitalized]] - 1];
                } else {
                    dayOfWeek = @"Sunday";
                }
            }
            
            
            if ( currentHour < dateRollOverHour ) {
                currentHour += 24;
            }
            
            NSArray* currentTime = @[[NSNumber numberWithLong:currentHour], [NSNumber numberWithLong:currentMinute]];
            
            BOOL goesFurtherNorth = NO;
            
            int indexOfClosestStop = [BARDToolkit indexOfString:closestStop.title inArray:[BARDShuttleStop shuttleStopNames]];
            NSString* nextNorthStop = @"";
            if ( indexOfClosestStop != 0 ) {
                nextNorthStop = [BARDShuttleStop shuttleStopNames][indexOfClosestStop - 1];
            }
            
            
            NSMutableArray *northboundTimes = [[NSMutableArray alloc] initWithCapacity:0];
            if ( ![nextNorthStop isEqualToString:@""] ) {
                northboundTimes = [BARDSchedule timeTableForDay:dayOfWeek origin:closestStop.title destination:nextNorthStop options:self.navigator.scheduleOptions];
            }
            for ( int i = 0; i < [northboundTimes count]; i++ ) {
                goesFurtherNorth = YES;
                NSMutableArray* time = [BARDToolkit convertTimeStringTo24HourTime:northboundTimes[i]];
                if ( [time[0] integerValue] < dateRollOverHour ) {
                    time[0] = [NSNumber numberWithLong:[time[0] integerValue] + 24];
                }
                northboundTimes[i] = time;
            }
            
            int indexOfNextNorthTime = -1;
            BOOL done = NO;
            int counter = 0;
            while ( !done ) {
                if ( counter == [northboundTimes count] ) {
                    done = YES;
                } else {
                    NSArray* timeInterval = [BARDToolkit timeIntervalBetweenTime:currentTime andTime:northboundTimes[counter]];
                    if ( [timeInterval[0] integerValue] >= 0 && [timeInterval[1] integerValue] >= 0 ) {
                        done = YES;
                        indexOfNextNorthTime = counter;
                    }
                }
                counter++;
            }
            
            
            NSMutableArray* currentTimeNorth = [[NSMutableArray alloc] initWithCapacity:2];
            [currentTimeNorth addObject:[NSNumber numberWithLong:[BARDToolkit currentHour]]];
            [currentTimeNorth addObject:[NSNumber numberWithLong:[BARDToolkit currentMinute]]];
            BOOL rolledOverToTomorrowNorth = NO;
            
            
            if ( indexOfNextNorthTime == -1 && goesFurtherNorth ) {
                int indexOfToday = [BARDToolkit indexOfString:dayOfWeek inArray:[BARDToolkit daysOfWeekCapitalized]];
                if (indexOfToday == 6 ) {
                    indexOfToday = -1;
                }
                NSString* tomorrow = [BARDToolkit daysOfWeekCapitalized][indexOfToday + 1];
                northboundTimes = [BARDSchedule timeTableForDay:tomorrow origin:closestStop.title destination:nextNorthStop options:self.navigator.scheduleOptions];
                for ( int i = 0; i < [northboundTimes count]; i++ ) {
                    NSMutableArray* time = [BARDToolkit convertTimeStringTo24HourTime:northboundTimes[i]];
                    if ( [time[0] integerValue] < dateRollOverHour ) {
                        time[0] = [NSNumber numberWithLong:[time[0] integerValue] + 24];
                    }
                    northboundTimes[i] = time;
                }
                indexOfNextNorthTime = 0;
                rolledOverToTomorrowNorth = YES;
            }
            
            if ( rolledOverToTomorrowNorth ) {
                if ( [currentTimeNorth[0] integerValue] >= dateRollOverHour ) {
                    int changed = (int)[currentTimeNorth[0] integerValue] - 24;
                    currentTimeNorth[0] = [NSNumber numberWithLong:changed];
                }
            } else {
                if ( [currentTimeNorth[0] integerValue] < dateRollOverHour ) {
                    int changed = (int)[currentTimeNorth[0] integerValue] + 24;
                    currentTimeNorth[0] = [NSNumber numberWithLong:changed];
                }
            }
            
            
            
            BOOL goesFurtherSouth = NO;
            NSString* nextSouthStop = @"";
            if ( indexOfClosestStop < [[BARDShuttleStop shuttleStopNames] count] - 1 ) {
                nextSouthStop = [BARDShuttleStop shuttleStopNames][[BARDToolkit indexOfString:closestStop.title inArray:[BARDShuttleStop shuttleStopNames]] + 1];
            }
            
            
            NSMutableArray *southboundTimes = [[NSMutableArray alloc] initWithCapacity:0];
            if ( ![nextSouthStop isEqualToString:@""] ) {
                southboundTimes = [BARDSchedule timeTableForDay:dayOfWeek origin:closestStop.title destination:nextSouthStop options:self.navigator.scheduleOptions];
            }
            
            
            for ( int i = 0; i < [southboundTimes count]; i++ ) {
                goesFurtherSouth = YES;
                NSMutableArray* time = [BARDToolkit convertTimeStringTo24HourTime:southboundTimes[i]];
                if ( [time[0] integerValue] < dateRollOverHour ) {
                    time[0] = [NSNumber numberWithLong:[time[0] integerValue] + 24];
                }
                southboundTimes[i] = time;
            }
            
            
            
            
            int indexOfNextSouthTime = -1;
            
            done = NO;
            counter = 0;
            while ( !done ) {
                
                if ( counter == [southboundTimes count] ) {
                    done = YES;
                } else {
                    NSArray* timeInterval = [BARDToolkit timeIntervalBetweenTime:currentTime andTime:southboundTimes[counter]];
                    if ( [timeInterval[0] integerValue] >= 0 && [timeInterval[1] integerValue] >= 0 ) {
                        done = YES;
                        indexOfNextSouthTime = counter;
                    }
                }
                counter++;
            }
            
            
            NSMutableArray* currentTimeSouth = [[NSMutableArray alloc] initWithCapacity:2];
            [currentTimeSouth addObject:[NSNumber numberWithLong:[BARDToolkit currentHour]]];
            [currentTimeSouth addObject:[NSNumber numberWithLong:[BARDToolkit currentMinute]]];
            BOOL rolledOverToTomorrowSouth = NO;
            
            if ( indexOfNextSouthTime == -1 && goesFurtherSouth ) {
                int indexOfToday = [BARDToolkit indexOfString:dayOfWeek inArray:[BARDToolkit daysOfWeekCapitalized]];
                if (indexOfToday == 6 ) {
                    indexOfToday = -1;
                }
                NSString* tomorrow = [BARDToolkit daysOfWeekCapitalized][indexOfToday + 1];
                southboundTimes = [BARDSchedule timeTableForDay:tomorrow origin:closestStop.title destination:nextSouthStop options:self.navigator.scheduleOptions];
                for ( int i = 0; i < [southboundTimes count]; i++ ) {
                    NSMutableArray* time = [BARDToolkit convertTimeStringTo24HourTime:southboundTimes[i]];
                    if ( [time[0] integerValue] < dateRollOverHour ) {
                        time[0] = [NSNumber numberWithLong:[time[0] integerValue] + 24];
                    }
                    southboundTimes[i] = time;
                }
                indexOfNextSouthTime = 0;
                rolledOverToTomorrowSouth = YES;
            }
            
            if ( rolledOverToTomorrowSouth ) {
                if ( [currentTimeSouth[0] integerValue] >= dateRollOverHour ) {
                    int changed = (int)[currentTimeSouth[0] integerValue] - 24;
                    currentTimeSouth[0] = [NSNumber numberWithLong:changed];
                }
            } else {
                if ( [currentTimeSouth[0] integerValue] < dateRollOverHour ) {
                    int changed = (int)[currentTimeSouth[0] integerValue] + 24;
                    currentTimeSouth[0] = [NSNumber numberWithLong:changed];
                }
            }
            
            NSArray* intervalToNorth;
            if ( [northboundTimes count] != 0 ) {
                intervalToNorth = [BARDToolkit timeIntervalBetweenTime:currentTimeNorth andTime:northboundTimes[indexOfNextNorthTime]];
            } else {
                intervalToNorth = @[[NSNumber numberWithLong:-100], [NSNumber numberWithLong:-100]];
            }
            
            
            NSArray* intervalToSouth;
            if ( [southboundTimes count] != 0 ) {
                intervalToSouth = [BARDToolkit timeIntervalBetweenTime:currentTimeSouth andTime:southboundTimes[indexOfNextSouthTime]];
            } else {
                intervalToSouth = @[[NSNumber numberWithLong:-100], [NSNumber numberWithLong:-100]];
            }
            
            
            [self loadSmartTimesPaneWithClosestNorthboundStop:closestStop closestSouthboundStop:closestStop hoursUntilSouth:(int)[intervalToSouth[0] integerValue] minutesUntilSouth:(int)[intervalToSouth[1] integerValue] hoursUntilNorth:(int)[intervalToNorth[0] integerValue] minutesUntilNorth:(int)[intervalToNorth[1] integerValue]];
        } else {
            
            if ( !self.tooFarFromCampusMessageDisplayed ) {
                [self displayTooFarFromCampusMessage];
            }
            
        }
        
        
    } else {
        if ( !self.noLocationDataMessageDisplayed ) {
            [self displayNoLocationDataMessage];
        }
    }
}



- (void) loadSmartTimesPaneWithClosestNorthboundStop:(BARDShuttleStop *)closestNorthboundStop closestSouthboundStop:(BARDShuttleStop *)closestSouthboundStop hoursUntilSouth:(int)hoursUntilSouth minutesUntilSouth:(int)minutesUntilSouth hoursUntilNorth:(int)hoursUntilNorth minutesUntilNorth:(int)minutesUntilNorth
{
    
    
    [self.northboundHours setText:[NSString stringWithFormat:@"%d", hoursUntilNorth]];
    
    if ( hoursUntilNorth == 1 ) {
        [self.northboundHoursLabel setText:@"Hour"];
    } else {
        [self.northboundHoursLabel setText:@"Hours"];
    }
    
    [self.northboundMinutes setText:[NSString stringWithFormat:@"%d", minutesUntilNorth]];
    
    if ( minutesUntilNorth == 1 ) {
        [self.northboundMinutesLabel setText:@"Minute"];
    } else {
        [self.northboundMinutesLabel setText:@"Minutes"];
    }
    
    [self.southboundHours setText:[NSString stringWithFormat:@"%d", hoursUntilSouth]];
    
    if ( hoursUntilSouth == 1 ) {
        [self.southboundHoursLabel setText:@"Hour"];
    } else {
        [self.southboundHoursLabel setText:@"Hours"];
    }
    
    [self.southboundMinutes setText:[NSString stringWithFormat:@"%d", minutesUntilSouth]];
    
    if ( minutesUntilSouth == 1 ) {
        [self.southboundMinutesLabel setText:@"Minute"];
    } else {
        [self.southboundMinutesLabel setText:@"Minutes"];
    }
    
    if ( hoursUntilNorth == 0 && minutesUntilNorth <= 5 ) {
        [self.northboundMinutes setTextColor:[UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:44 B:1]]];
    } else {
        [self.northboundMinutes setTextColor:[UIColor colorFromHexString:[BARDToolkit hexcodeFromR:7 G:214 B:241]]];
    }
    
    if ( hoursUntilSouth == 0 && minutesUntilSouth <= 5 ) {
        [self.southboundMinutes setTextColor:[UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:44 B:1]]];
    } else {
        [self.southboundMinutes setTextColor:[UIColor colorFromHexString:[BARDToolkit hexcodeFromR:7 G:214 B:241]]];
    }
    
    
    if ( hoursUntilNorth != -100 ) {
        
        NSArray* northboundFrames = [self labelFramesForHoursUntil:hoursUntilNorth minutesUntil:minutesUntilNorth northbound:YES];
        
        self.tooFarFromCampusMessageDisplayed = NO;
        self.noLocationDataMessageDisplayed = NO;
        self.smartTimesErrorMessage.layer.opacity = 0;
        
        self.northMessage.layer.opacity = 0;
        
        self.smartTimesDivider.layer.opacity = 1;
        
        self.northboundTitle.layer.opacity = 1;
        self.northboundOriginLabel.layer.opacity = 1;
        self.northboundHours.layer.opacity = 1;
        self.northboundHoursLabel.layer.opacity = 1;
        self.northboundMinutes.layer.opacity = 1;
        self.northboundMinutesLabel.layer.opacity = 1;
        
        [self.northboundHours setFrame:[northboundFrames[0] CGRectValue]];
        [self.northboundHoursLabel setFrame:[northboundFrames[1] CGRectValue]];
        [self.northboundMinutes setFrame:[northboundFrames[2] CGRectValue]];
        [self.northboundMinutesLabel setFrame:[northboundFrames[3] CGRectValue]];
        
        
        [self.northboundOriginLabel setText:[NSString stringWithFormat:@"from %@", closestNorthboundStop.title]];
        
        
        [self.northboundMinutes setFont:[UIFont boldSystemFontOfSize:[northboundFrames[4] floatValue]]];
        [self.northboundMinutes setFont:[UIFont boldSystemFontOfSize:[northboundFrames[4] floatValue]]];
        
        [UIView transitionWithView:self.smartTimesPane duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            
        }completion:nil];
        
        
    } else {
        
        self.northMessage.text = @"The Bard Shuttle does not go further north than Tivoli.";
        
        self.tooFarFromCampusMessageDisplayed = NO;
        self.noLocationDataMessageDisplayed = NO;
        self.smartTimesErrorMessage.layer.opacity = 0;
        
        self.smartTimesDivider.layer.opacity = 1;
        
        self.northMessage.layer.opacity = 1;
        self.northboundTitle.layer.opacity = 0;
        self.northboundOriginLabel.layer.opacity = 0;
        self.northboundHours.layer.opacity = 0;
        self.northboundHoursLabel.layer.opacity = 0;
        self.northboundMinutes.layer.opacity = 0;
        self.northboundMinutesLabel.layer.opacity = 0;
        
        [UIView transitionWithView:self.smartTimesPane duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            
        }completion:nil];
        
    }
    if ( hoursUntilSouth != -100 ) {
        
        NSArray* southboundFrames = [self labelFramesForHoursUntil:hoursUntilSouth minutesUntil:minutesUntilSouth northbound:NO];
        
        self.tooFarFromCampusMessageDisplayed = NO;
        self.noLocationDataMessageDisplayed = NO;
        self.smartTimesErrorMessage.layer.opacity = 0;
        
        self.smartTimesDivider.layer.opacity = 1;
        
        self.southMessage.layer.opacity = 0;
        
        
        self.southboundTitle.layer.opacity = 1;
        self.southboundOriginLabel.layer.opacity = 1;
        self.southboundHours.layer.opacity = 1;
        self.southboundHoursLabel.layer.opacity = 1;
        self.southboundMinutes.layer.opacity = 1;
        self.southboundMinutesLabel.layer.opacity = 1;
        
        
        [self.southboundHours setFrame:[southboundFrames[0] CGRectValue]];
        [self.southboundHoursLabel setFrame:[southboundFrames[1] CGRectValue]];
        [self.southboundMinutes setFrame:[southboundFrames[2] CGRectValue]];
        [self.southboundMinutesLabel setFrame:[southboundFrames[3] CGRectValue]];
        
        
        [self.southboundOriginLabel setText:[NSString stringWithFormat:@"from %@", closestSouthboundStop.title]];
        
        
        [self.southboundMinutes setFont:[UIFont boldSystemFontOfSize:[southboundFrames[4] floatValue]]];
        
        [UIView transitionWithView:self.smartTimesPane duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            
        }completion:nil];
        
    } else {
        
        self.southMessage.text = @"The Bard Shuttle does not go further south than Hannaford.";
        
        self.tooFarFromCampusMessageDisplayed = NO;
        self.noLocationDataMessageDisplayed = NO;
        self.smartTimesErrorMessage.layer.opacity = 0;
        
        self.smartTimesDivider.layer.opacity = 1;
        
        self.southMessage.layer.opacity = 1;
        self.southboundTitle.layer.opacity = 0;
        self.southboundOriginLabel.layer.opacity = 0;
        self.southboundHours.layer.opacity = 0;
        self.southboundHoursLabel.layer.opacity = 0;
        self.southboundMinutes.layer.opacity = 0;
        self.southboundMinutesLabel.layer.opacity = 0;
        
        [UIView transitionWithView:self.smartTimesPane duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            
        }completion:nil];
        
    }
    
}






- (void) displayNoLocationDataMessage
{
    NSLog(@"Cannot access the user's location.");
    
    self.tooFarFromCampusMessageDisplayed = NO;
    self.noLocationDataMessageDisplayed = YES;
    
    if ( !self.smartTimesErrorMessage ) {
        self.smartTimesErrorMessage = [[UILabel alloc] initWithFrame:CGRectMake(18, 0, 185, 160)];
    }
    
    self.smartTimesErrorMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.smartTimesErrorMessage.numberOfLines = 0;
    self.smartTimesErrorMessage.textAlignment = NSTextAlignmentCenter;
    
    self.smartTimesErrorMessage.text = @"Smart Times cannot access your location. To start using Smart Times enable Location Services, turn on WiFi, and ensure you are not in Airplane Mode.";
    
    [self.smartTimesErrorMessage setFont:[UIFont systemFontOfSize:12]];
    
    [self.smartTimesPane addSubview:self.smartTimesErrorMessage];
    
    [UIView animateWithDuration:1 animations:^{
        
        self.northMessage.layer.opacity = 0;
        self.northboundTitle.layer.opacity = 0;
        self.northboundOriginLabel.layer.opacity = 0;
        self.northboundHours.layer.opacity = 0;
        self.northboundHoursLabel.layer.opacity = 0;
        self.northboundMinutes.layer.opacity = 0;
        self.northboundMinutesLabel.layer.opacity = 0;
        
        self.smartTimesDivider.layer.opacity = 0;
        
        self.southMessage.layer.opacity = 0;
        self.southboundTitle.layer.opacity = 0;
        self.southboundOriginLabel.layer.opacity = 0;
        self.southboundHours.layer.opacity = 0;
        self.southboundHoursLabel.layer.opacity = 0;
        self.southboundMinutes.layer.opacity = 0;
        self.southboundMinutesLabel.layer.opacity = 0;
        
        self.smartTimesErrorMessage.layer.opacity = 1;
        
    }];
    
    
}



- (void) displayTooFarFromCampusMessage
{
    NSLog(@"User is too far from campus to use Smart Times.");
    
    self.tooFarFromCampusMessageDisplayed = YES;
    self.noLocationDataMessageDisplayed = NO;
    
    if ( !self.smartTimesErrorMessage ) {
        self.smartTimesErrorMessage = [[UILabel alloc] initWithFrame:CGRectMake(18, 0, 185, 160)];
    }
    
    self.smartTimesErrorMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.smartTimesErrorMessage.numberOfLines = 0;
    self.smartTimesErrorMessage.textAlignment = NSTextAlignmentCenter;
    
    self.smartTimesErrorMessage.text = @"Smart Times is not available because you are too far from campus. Smart Times will begin working when you are in the vicinity of one of the Bard Shuttle stops.";
    
    [self.smartTimesErrorMessage setFont:[UIFont systemFontOfSize:12]];
    
    [self.smartTimesPane addSubview:self.smartTimesErrorMessage];
    
    [UIView animateWithDuration:1 animations:^{
        
        self.northMessage.layer.opacity = 0;
        self.northboundTitle.layer.opacity = 0;
        self.northboundOriginLabel.layer.opacity = 0;
        self.northboundHours.layer.opacity = 0;
        self.northboundHoursLabel.layer.opacity = 0;
        self.northboundMinutes.layer.opacity = 0;
        self.northboundMinutesLabel.layer.opacity = 0;
        
        self.smartTimesDivider.layer.opacity = 0;
        
        self.southMessage.layer.opacity = 0;
        self.southboundTitle.layer.opacity = 0;
        self.southboundOriginLabel.layer.opacity = 0;
        self.southboundHours.layer.opacity = 0;
        self.southboundHoursLabel.layer.opacity = 0;
        self.southboundMinutes.layer.opacity = 0;
        self.southboundMinutesLabel.layer.opacity = 0;
        
        self.smartTimesErrorMessage.layer.opacity = 1;
        
    }];
}

- (void) displayNoWeekendSummerShuttlesMessage
{
    NSLog(@"It is both the summer and a weekend.");
    
    self.tooFarFromCampusMessageDisplayed = YES;
    self.noLocationDataMessageDisplayed = NO;
    
    if ( !self.smartTimesErrorMessage ) {
        self.smartTimesErrorMessage = [[UILabel alloc] initWithFrame:CGRectMake(18, 0, 185, 160)];
    }
    
    self.smartTimesErrorMessage.lineBreakMode = NSLineBreakByWordWrapping;
    self.smartTimesErrorMessage.numberOfLines = 0;
    self.smartTimesErrorMessage.textAlignment = NSTextAlignmentCenter;
    
    self.smartTimesErrorMessage.text = @"The shuttle does not run on Saturday or Sunday during the summer.";
    
    [self.smartTimesErrorMessage setFont:[UIFont systemFontOfSize:12]];
    
    [self.smartTimesPane addSubview:self.smartTimesErrorMessage];
    
    [UIView animateWithDuration:1 animations:^{
        
        self.northMessage.layer.opacity = 0;
        self.northboundTitle.layer.opacity = 0;
        self.northboundOriginLabel.layer.opacity = 0;
        self.northboundHours.layer.opacity = 0;
        self.northboundHoursLabel.layer.opacity = 0;
        self.northboundMinutes.layer.opacity = 0;
        self.northboundMinutesLabel.layer.opacity = 0;
        
        self.smartTimesDivider.layer.opacity = 0;
        
        self.southMessage.layer.opacity = 0;
        self.southboundTitle.layer.opacity = 0;
        self.southboundOriginLabel.layer.opacity = 0;
        self.southboundHours.layer.opacity = 0;
        self.southboundHoursLabel.layer.opacity = 0;
        self.southboundMinutes.layer.opacity = 0;
        self.southboundMinutesLabel.layer.opacity = 0;
        
        self.smartTimesErrorMessage.layer.opacity = 1;
        
    }];
}









- (NSArray *) labelFramesForHoursUntil:(int)hoursUntil minutesUntil:(int)minutesUntil northbound:(BOOL)northbound
{
    NSMutableArray* frames = [[NSMutableArray alloc] init];
    if ( northbound ) {
        
        if ( hoursUntil == 0 && minutesUntil < 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(117, 52, 0, 0)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 59, 0, 0)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(166, 27, 26, 45)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(158, 58, 45, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:37];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else if ( hoursUntil == 0 && minutesUntil >= 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(117, 52, 0, 0)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 59, 0, 0)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(157, 27, 45, 45)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(158, 58, 45, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:37];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else if ( hoursUntil < 10 && minutesUntil < 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(147, 34, 60, 36)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 59, 30, 21)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(193, 34, 32, 36)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(183, 59, 38, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:28];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else if ( hoursUntil < 10 && minutesUntil >= 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(147, 34, 45, 36)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 59, 30, 21)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(186, 34, 32, 36)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(183, 59, 38, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:28];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else if ( hoursUntil >= 10 && minutesUntil < 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(141, 34, 33, 36)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 59, 30, 21)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(192, 34, 32, 36)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(183, 59, 38, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:28];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(141, 34, 33, 36)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 59, 30, 21)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(186, 34, 32, 36)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(183, 59, 38, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:28];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        }
        
    } else {
        
        if ( hoursUntil == 0 && minutesUntil < 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(164, 105, 0, 0)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(157, 122, 0, 0)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(166, 85, 24, 42)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(158, 116, 45, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:37];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else if ( hoursUntil == 0 && minutesUntil >=10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(164, 105, 0, 0)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(157, 122, 0, 0)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(157, 85, 45, 45)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(158, 116, 45, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:37];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else if ( hoursUntil < 10 && minutesUntil < 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(147, 87, 34, 36)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 111, 30, 22)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(193, 87, 32, 36)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(183, 111, 38, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:28];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else if ( hoursUntil < 10 && minutesUntil >= 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(147, 87, 34, 36)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 111, 30, 22)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(186, 87, 32, 36)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(183, 111, 38, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:28];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else if ( hoursUntil >= 10 && minutesUntil < 10 ) {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(141, 87, 33, 36)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 111, 30, 21)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(193, 87, 32, 36)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(183, 111, 38, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:28];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        } else {
            
            NSValue* hoursFrame = [NSValue valueWithCGRect:CGRectMake(141, 87, 33, 36)];
            NSValue* hoursLabelFrame = [NSValue valueWithCGRect:CGRectMake(142, 111, 30, 21)];
            NSValue* minutesFrame = [NSValue valueWithCGRect:CGRectMake(187, 87, 32, 36)];
            NSValue* minutesLabelFrame = [NSValue valueWithCGRect:CGRectMake(183, 111, 38, 21)];
            NSNumber* fontSize = [NSNumber numberWithInt:28];
            
            [frames addObject:hoursFrame];
            [frames addObject:hoursLabelFrame];
            [frames addObject:minutesFrame];
            [frames addObject:minutesLabelFrame];
            [frames addObject:fontSize];
            
        }
    }
    return frames;
}


- (IBAction)backToMainMenu:(id)sender
{
    
    self.mainMenuTitle.text = @"Main Menu";
    self.mainMenuTitle.font = [UIFont fontWithName:@"Avenir" size:15];
    self.menu.layer.opacity = 1;
    self.menuItemMessage.layer.opacity = 0;
    self.menuItemMessage.layer.opacity = 0;
    self.backToMainMenuButton.layer.opacity = 0;
    NSRange top = NSRangeFromString(@"0");
    [self.menuItemMessage scrollRangeToVisible:top];
    [UIView transitionWithView:self.menuPane duration:0.1 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        
    }completion:nil];
}

- (void) displayMenuItemAtIndex:(NSIndexPath*)indexPath
{
    
    self.mainMenuTitle.text = [self.menu cellForRowAtIndexPath:indexPath].textLabel.text;
    self.mainMenuTitle.font = [UIFont fontWithName:@"Avenir" size:15];
    self.menu.layer.opacity = 0;
    self.menuItemMessage.layer.opacity = 1;
    self.backToMainMenuButton.layer.opacity = 1;
    self.menuItemMessage.layer.opacity = 1;
    self.menuItemMessage.font = [UIFont fontWithName:@"Avenir" size:14];
    [self resizeMenuMessageToHeight:self.menuPane.frame.size.height - (self.menuHeaderDivider.frame.origin.y + self.menuHeaderDivider.frame.size.height)];
    if ( indexPath.row == 0 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nThe Bard College Shuttle is a shuttle bus that operates every day of the week for most of the day, during the school year. It travels from the town of Tivoli (which is to the north of campus) down to campus, and then it continues further south to the town of Red Hook. At certain times of day it goes on to Hannaford supermarket in south Red Hook. It then turns around and goes back up to campus, and then to Tivoli to start over again. As of April 2014 Bard Transportation has instituted an ID check upon boarding the shuttle. In order to be allowed to ride the shuttle, you must display a valid Bard ID, or if you are a visitor you must be registered with the school. Beginning in the Fall 2014 semester there will be a machine on board which will require you to swipe your ID to be granted access to the shuttle.\n\nTo view the schedule for the Bard Shuttle, return to the main (green) screen and drag your finger from the stop you will be departing from to the stop you wish to arrive at. The schedule is different for different days of the week so be sure to select the day you wish to see the schedule for. To see which shuttle stop each icon represents tap the icon you are wondering about and the name of the stop will appear to the right of the icon.\n\n";
    } else if ( indexPath.row == 1 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nThe Golf Cart is driven by a student every night of the week. It will pick up Bard students anywhere on campus and drive them to any other location on campus. Its hours of operation are 9:00 PM to 1:00 AM. To request the Golf Cart call Bard Security at (845) 758-7460. The Golf Cart is brought to you by Bard's Student Resource Group.\n\n*The Golf Cart does not operate in the rain or snow, or when it is exceptionally cold outside.\n\n";
    } else if ( indexPath.row == 2 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nTivoli Delivery (A.K.A. Tiv-Deliv) is a school service which brings residents of Tivoli from campus to their houses on Thursday, Friday and Saturday nights from 12:00 AM to 3:00 AM. Tivoli Delivery cars leave from the security building (the Old Gym) every half hour. To be taken to Tivoli, you must be a registered resident of Tivoli. Tivoli Delivery is brought to you by Bard's Student Resource Group.\n\n\n\n\n\n\n";
    } else if ( indexPath.row == 3 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nSafe Ride is a school service which will pick up Bard Students who live on campus from anywhere in the surrounding area and drop them at their dorm. This service is available on Thursday, Friday and Saturday nights from 12:00 AM to 3:00 AM. To request Safe Ride call (845) 752-4444. Safe Ride is brought to you by Bard's Student Resource Group.\n\n\n\n\n\n\n";
    } else if ( indexPath.row == 4 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nSmart Times is the name of the upper one of the two white windows in this menu. It uses your location to figure out which of the Bard College shuttle stops is closest to you and then displays the amount of time until the next shuttle leaves from that stop. The top half of the window displays the amount of time until the next northbound shuttle leaves from that stop, while the lower half of the window displays the amount of time until the next southbound shuttle leaves.\n\n\nTROUBLESHOOTING\n\nIf the Smart Times window seems to think that you are somewhere other than you are, it is probably due to the inaccuracy of the iPhone\'s GPS system. However, this can usually be fixed without even leaving the app. Simply swipe up from the very bottom of the screen to open the control center and tap the Wi-Fi symbol to turn on your phone\'s Wi-Fi. Then tap the Smart Times window to refresh it. Turning on Wi-Fi on your phone dramatically improves the accuracy of location data. This will work regardless of your proximity to an actual Wi-Fi network.\n\nIf the Smart Times window is displaying the message, \"Smart Times cannot access your location[...]\", then first make sure there isn\'t a litle airplane symbol in the upper left corner of the screen. If there is, then swipe up from the very bottom of the screen to open the control center. The airplane icon on the left should be white. Tap it and it should turn black. If your phone is not on airplane mode and you are still getting the error message then you probably did not grant the app permission to access your location when you first installed it. To fix this you must exit the app and go to the Settings app. Once there you go to Privacy/Location Services and make sure the switch next to the word \"Location Services\" at the top is on. Finally, scroll down until you see Bard Transportation in the list of apps and switch on the switch that is next to it. Return to the Bard Transport app and Smart Times should be working.\n";
    } else if ( indexPath.row == 5 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nBard Area \n\nVillage Taxi: (845) 757-2244 \n-------------------------------------------\n\nKingston Area\n\nKingston Kabs: (845) 331-8294\nSonia Cabs: (845) 336-2400\n-------------------------------------------\n\nDutchess Area\n\n Kingston Kabs: (845) 758-8294\n\n";
    } else if ( indexPath.row == 6 ) {
        
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nBard Security is available 24/7. They can be reached at their main line: (845) 758-7460 or at their emergency line: (845) 758-7777. If you have locked yourself out of your room in the middle of the night, call security and they will let you back in. Note: $10 will be charged to your student account for every lock-out. In certain situations security will also give you a ride (if you, for example, are injured or need to transport a heavy item). If you ever feel unsafe on campus, call Bard Security and they will help you. The security office is located in the Old Gym next to Olin.";
    } else if ( indexPath.row == 7 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nBard provides shuttles between campus andr various surrounding areas. The schedule for these shuttles for the Fall 2014 semester is as follows:\n\n\nHUDSON VALLEY MALL IN KINGSTON\n\nA shuttle leaves from the Kline shuttle stop going to Kingston at 6:00 PM, most Wednesdays of the year. This shuttle stays in Kingston 9:00 PM, and then departs for Bard. The shuttle leaves from the same place in Kingston as it drops you off. This shuttle runs every Wednesday between September 3rd and December 17th EXCEPT FOR November 26th.\n\nThere is also a shuttle to Kingston on Saturdays which departs from the Kline shuttle stop at 2:00 PM and returns at 5:00 PM. This shuttle runs every Saturday between September 6th and December 14th EXCEPT FOR November 29th and December 6th.\n\n\nDOWNTOWN RHINEBECK\n\nBard provides shuttle service to Rhinebeck most Fridays of the year. There are seven shuttles to Rhinebeck on these days, leaving from Kline at 12:00 PM, 1:00 PM, 2:00 PM, 3:00 PM, 4:00 PM, 5:00 PM and 6:00 PM. The seven returning shuttles depart at 12:30 PM, 1:30 PM, 2:30 PM, 3:30 PM, 4:30 PM, 5:30 PM and 6:30 PM. Upon boarding the shuttle students may request to be dropped at the Rhinecliff train station. This shuttle runs every Friday between September 5th and Decemeber 12th EXCEPT FOR November 28th.\n\n\nWOODBURY COMMONS MALL\n\nFinally, there is a single trip to the Woodbury Commons Mall each semester. In the Fall 2014 semester this trip is on Saturday, December 6th. It leaves at 1:00 PM and returns at 7:00 PM.\n";
    } else if (indexPath.row == 8 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nBard provides weekend shuttle service which is coordinated with certain Amtrak and Metro-North trains. The weekend train shuttle service for the Fall 2014 semester is as follows:\n\n\nAMTRAK TRAINS FROM THE RHINECLIFF STATION\n\nFRIDAYS\n\nOn Fridays the shuttle to the Rhinecliff station is the same as the Area Shuttle to Downtown Rhinebeck. The shuttle travels from Kline to the Municipal Lot in Rhinebeck, and then, if someone has requested it, to the train station. You can board the shuttle either on campus, or in Rhinebeck, but to be taken to the train station YOU MUST REQUEST IT. Otherwise the shuttle will go straight back to campus. You can board the shuttle at Kline at 12:00 PM, 1:00 PM, 2:00 PM. 3:00 PM, 4:00 PM, 5:00 PM and 6:00 PM. You can also board the shuttle at its stop in Downtown Rhinebeck at 12:30 PM, 1:30 PM, 2:30 PM, 3:30 PM, 4:30 PM, 5:30 PM and 6:30 PM.\n\nSATURDAYS\n\nThere are no shuttles to the Rhinecliff train station on Saturdays. A cab from campus to the station is about $20.\n\nSUNDAYS\n\nOn Sundays there are two shuttles that go to the Rhinecliff station, which leave Kline at 5:45 PM and 8:15 PM. They arrive at the station with time to board the 6:26 PM and 9:01 PM trains to New York, respectively. They wait to pick up students arriving on the 6:55 PM and 8:55 PM trains respectively.\n\n\nMETRO-NORTH TRAINS FROM THE POUGHKEEPSIE STATION\n\nFRIDAYS\n\nOn Fridays there are three shuttles from campus to the Poughkeepsie train station. They depart from Kline at 1:45 PM, 3:45 PM and 5:45 PM, arriving at the station with enough time to board the 2:54 PM, 4:54 PM and 6:56 PM trains respectively. They wait to pick up students arriving on the 2:30 PM, 4:30 PM and 6:40 PM trains respectively. All trains are Metro-North.\n\nSATURDAYS\n\nOn Saturdays there is a morning bus and two evening buses to the Poughkeepsie train station which leave at 9:45 AM, 6:45 PM and 8:45 PM, arriving in time for students to board the 10:54 AM, 7:54 PM and 9:54 PM trains respectively. They wait to pick up students arriving on the 10:43 AM, 7:30 PM and 9:30 PM trains respectively. All trains are Metro-North.\n\nSUNDAYS\n\nOn Sundays there are three shuttles to the Poughkeepsie station. They leave Kline at 4:45 PM, 6:45 PM and 8:45 PM, dropping students off with enough time to board the 5:54 PM, 7:54 PM and 9:54 PM trains respectively. They wait to pick up students arriving on the 5:30 PM, 7:30 PM and 9:30 PM trains respectively. All trains are Metro-North.\n";
    } else if ( indexPath.row == 9 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nIf you've cracked your iPhone screen and want it repaired now you can contact Uncracked™ iPhone Repair. They repair the screens of the iPhone 4, 4S, 5, 5C and 5S.\n Email uncrackedrepair@gmail.com or call (646) 241-8438 for more information.\n\n\n\n\n\n\n";
    } else if ( indexPath.row == 10 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nQuestions, comments, or suggestions about the app? Email them to bardtransportapp@gmail.com\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    } else if ( indexPath.row == 11 ) {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"\nDeveloped by\nJeremy Bannister\n\nGraphic Design by\nSonja Tsypin\n\nBotstein's bow tie courtesy of\nLeo Kaupert\n\nWith special thanks to\nMo King and Maja Nguyen\n\nLast but not least, the App Guru, for when all hope was lost. Thank you Greg Lanweber.\n\n";
    } else {
        self.menuItemMessage.text = nil;
        self.menuItemMessage.text = @"Menu Item Coming Soon";
    }
    
    [UIView transitionWithView:self.menuPane duration:0.1 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        
    }completion:nil];
}

- (void) resizeMenuMessageToHeight:(double)height {
    CGRect messageFrame = self.menuItemMessage.frame;
    messageFrame.size.height = height;
    self.menuItemMessage.frame = messageFrame;
    
    CGSize scrollContentSize;
    if ( height < self.menuItemMessage.frame.size.height + 20 ) {
        scrollContentSize = CGSizeMake(self.menuItemMessage.frame.size.width, self.menuItemMessage.frame.size.height + 20);
    } else {
        scrollContentSize = CGSizeMake(self.menuItemMessage.contentSize.width, height);
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.backgroundColor = [UIColor clearColor];
    }
    switch ( indexPath.row ) {
        case 0:
            cell.textLabel.text = @"Bard College Shuttle";
            break;
        case 1:
            cell.textLabel.text = @"Golf Cart";
            break;
        case 2:
            cell.textLabel.text = @"Tivoli Delivery";
            break;
        case 3:
            cell.textLabel.text = @"Safe Ride";
            break;
        case 4:
            cell.textLabel.text = @"Smart Times";
            break;
        case 5:
            cell.textLabel.text = @"Local Cab Services";
            break;
        case 6:
            cell.textLabel.text = @"Bard Security";
            break;
        case 7:
            cell.textLabel.text = @"Area Shuttles";
            break;
        case 8:
            cell.textLabel.text = @"Weekend Train Shuttles";
            break;
        case 9:
            cell.textLabel.text = @"Campus iPhone Repair";
            break;
        case 10:
            cell.textLabel.text = @"App Feedback";
            break;
        case 11:
            cell.textLabel.text = @"Credits";
            break;
        default:
            cell.textLabel.text = @"Default";
            break;
            
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"Avenir" size:16];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self displayMenuItemAtIndex:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return NO;
}



@end
