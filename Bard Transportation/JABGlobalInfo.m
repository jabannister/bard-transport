//
//  JABGlobalInfo.m
//  JAB2048
//
//  Created by Jeremy Bannister on 3/25/14.
//  Copyright (c) 2014 Jeremy. All rights reserved.
//

#import "JABGlobalInfo.h"

double screenWidth = -1;
double screenHeight = -1;
float iOS = 0;

@implementation JABGlobalInfo



+ (void) getScreenDimensions
{
    screenWidth = [[UIScreen mainScreen] bounds].size.width;
    screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
}


+ (void) getOperatingSystem
{
    iOS = [[[UIDevice currentDevice] systemVersion] floatValue];
}


@end
