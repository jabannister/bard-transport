//
//  BARDSchedule.h
//  Bard Transportation
//
//  Created by Muhsin King on 10/18/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
// Header file, contains interface, declarations

#import <Foundation/Foundation.h>

@interface BARDSchedule : NSObject


/**
Returns an array of times for the given origin going in a certain direction as determined by the destination with reference to the origin.
*/
+ (NSMutableArray*) timeTableForDay:(NSString*) day origin: (NSString*) origin destination: (NSString*) destination options: (NSString*) options;

@end
