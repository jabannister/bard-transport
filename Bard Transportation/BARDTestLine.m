//
//  BARDTestLine.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 5/7/14.
//  Copyright (c) 2014 Bard College. All rights reserved.
//

#import "BARDTestLine.h"

@implementation BARDTestLine

#pragma -mark Initialize Line

- (void) initializeViewWithSuperview:(UIView *)superview position:(double)position vertical:(BOOL)vertical
{
    
    self.testLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    CGRect frameOfLine = self.testLineView.frame;
    
    if ( vertical ) {
        frameOfLine.size.width = 1;
        frameOfLine.size.height = superview.frame.size.height;
        frameOfLine.origin.x = position;
        frameOfLine.origin.y = 0;
    } else {
        frameOfLine.size.width = superview.frame.size.width;
        frameOfLine.size.height = 1;
        frameOfLine.origin.x = 0;
        frameOfLine.origin.y = position;
    }
    
    self.vertical = vertical;
    self.position = position;
    self.width = 1;
    self.color = [UIColor redColor];
    
    
    
    self.testLineView.frame = frameOfLine;
    self.testLineView.backgroundColor = [UIColor redColor];
    [superview addSubview:self.testLineView];
    
}


#pragma -mark Alter Line

- (void) changeVertical:(BOOL)vertical
{
    CGRect newTestLineFrame = self.testLineView.frame;
    
    if ( vertical ) {
        newTestLineFrame.size.width = self.width;
        newTestLineFrame.size.height = self.superview.frame.size.height;
        newTestLineFrame.origin.x = self.position;
        newTestLineFrame.origin.y = 0;
    } else {
        newTestLineFrame.size.width = self.superview.frame.size.width;
        newTestLineFrame.size.height = self.width;
        newTestLineFrame.origin.x = 0;
        newTestLineFrame.origin.y = self.position;
    }
    
    self.testLineView.frame = newTestLineFrame;
    self.vertical = vertical;
}

- (void) toggleVertical
{
    [self changeVertical:!self.vertical];
}

- (void) changePosition:(double)newPosition
{
    CGRect newTestLineFrame = self.testLineView.frame;
    
    if ( self.vertical ) {
        NSLog(@"vertical");
        newTestLineFrame.origin.x = newPosition;
    } else {
        NSLog(@"Not vertical");
        newTestLineFrame.origin.y = newPosition;
    }
    
    self.testLineView.frame = newTestLineFrame;
    self.position = newPosition;
}

- (void) changeWidth:(double)newWidth
{
    CGRect newTestLineFrame = self.testLineView.frame;
    
    if ( self.vertical ) {
        newTestLineFrame.size.width = newWidth;
    } else {
        newTestLineFrame.size.height = newWidth;
    }
    
    self.testLineView.frame = newTestLineFrame;
    self.width = newWidth;
}

- (void) changeColor:(UIColor *)newColor
{
    self.testLineView.backgroundColor = newColor;
    self.color = newColor;
}



@end
