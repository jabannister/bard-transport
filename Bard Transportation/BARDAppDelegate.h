//
//  BARDAppDelegate.h
//  Bard Transportation
//
//  Created by Muhsin King on 9/20/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BARDNavigationController;

@interface BARDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BARDNavigationController* navigator;


@end
