//
//  BARDScheduleSelectorViewController.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/18/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDScheduleSelectorViewController.h"
#import "BARDMapViewController.h"
#import "BARDNavigationController.h"
#import "BARDSchedule.h"
#import "BARDShuttleStop.h"
#import "BARDSelectorStop.h"
#import "BARD2DVector.h"
#import "BARDNavigatedViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface BARDScheduleSelectorViewController () {
  
  double pi;
  
  CGRect offScreenFrame;
  CGRect onScreenFrame;
  CGRect belowScreenFrame;
  
  CGRect scheduleFrame;
  CGRect scheduleTableFrame;
  
  CGRect menuFrame;
  
  CGRect helpFrameOpen;
  CGRect helpFrameClosed;
  CGRect helpMessageFrameOpen;
  CGRect helpMessageFrameClosed;
  CGRect helpCloseButtonFrameOpen;
  CGRect helpCloseButtonFrameClosed;
  CGRect helpCloseButtonImageFrameOpen;
  CGRect helpCloseButtonImageFrameClosed;
  
  double leftSideOfShuttleStopColumn;
  double rightSideOfShuttleStopColumn;
  
  CGPoint initialTouchLocation;
  
  double deltaX;
  double deltaY;
  
  double previousDeltaX;
  double previousDeltaY;
  
  
  double selectionAnimationDuration;
  
  BOOL outOfScheduleTapInitiated;
  BOOL outOfHelpTapInitiated;
  BOOL helpCloseButtonPressed;
  BOOL menuPanTriggered;
  BOOL mapPanTriggered;
  BOOL navigationPanTriggered;
  BOOL passedOffToStack;
  
  BOOL noMoreShuttlesToday;
  
  double numberOfTimesGestureMethodCalled;
  
  double distanceTraveledByFinger;
  
  double distanceFromTopOfScreenToTopOfSchedule;
  
  UIColor *selectorColor;
}

@end

@implementation BARDScheduleSelectorViewController

#pragma -mark Super

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (id) init
{
  self = [super init];
  if (self){
    
  }
  
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  NSLog(@"Schedule Selector View Did Load");
  // Do any additional setup after loading the view.
  
  
  pi = 3.14159265;
  selectionAnimationDuration = 0.3;
  
  numberOfTimesGestureMethodCalled = 0;
  
  self.allStopsDisplayed = NO;
  
  //CREATE SHUTTLE SCHEDULE
  
  self.schedule = [[BARDSchedule alloc] init];
  
  self.view.clipsToBounds = YES;
  
  
  if ( self.navigator.ipad ) {
    distanceFromTopOfScreenToTopOfSchedule = 193;
  } else {
    distanceFromTopOfScreenToTopOfSchedule = 130;
  }
  
  
  onScreenFrame = CGRectMake(0, 0, screenWidth, screenHeight);
  offScreenFrame = CGRectMake(screenWidth, 0, screenWidth, screenHeight);
  belowScreenFrame = CGRectMake(0, screenHeight, screenWidth, screenHeight);
  scheduleFrame = CGRectMake(0, distanceFromTopOfScreenToTopOfSchedule, screenWidth, screenHeight);
  scheduleTableFrame = CGRectMake(0, 0, screenWidth, 0);
  menuFrame = CGRectMake(menuFrame.origin.x = -(screenWidth - self.navigator.menuController.menu.frame.size.width) + 15, 0, screenWidth, screenHeight);
  
  
  //CREATE GESTURE RECOGNIZER
  
  UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressDetected:)];
  longPressRecognizer.delegate = self;
  longPressRecognizer.minimumPressDuration = 0.01;
  longPressRecognizer.allowableMovement = screenHeight;
  [self.view addGestureRecognizer:longPressRecognizer];
  
  UIColor *fluidColor;
  
  long daysSinceJanuaryFirst = [BARDToolkit daysSinceJanuaryFirst];
  if ( daysSinceJanuaryFirst < 81 || daysSinceJanuaryFirst > 354) {
    selectorColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:255 B:255]];
    fluidColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:255 B:255]];
  } else if ( daysSinceJanuaryFirst > 79 && daysSinceJanuaryFirst < 172 ) {
    selectorColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:126 B:247]];
    fluidColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:126 B:247]];
  } else if ( daysSinceJanuaryFirst > 171 && daysSinceJanuaryFirst < 264 ) {
    selectorColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:166 B:9]];
    fluidColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:255 G:166 B:9]];
  } else {
    selectorColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:213 G:68 B:8]];
    fluidColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:213 G:68 B:8]];
  }
  //
  
  
  
  
  self.shadeLayer = [[UIView alloc] initWithFrame:onScreenFrame];
  self.shadeLayer.layer.opacity = 0;
  self.shadeLayer.backgroundColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:40 G:40 B:40]];
  self.shadeLayer.userInteractionEnabled = NO;
  [self.view addSubview:self.shadeLayer];
  
  
  self.view.userInteractionEnabled = YES;
  
  
  
  //Create stack
  
  CGRect stackFrame = onScreenFrame;
  stackFrame.size.width = screenWidth/3;
  stackFrame.origin.x = screenWidth/3;
  
  double unselectedGreyColor =  200;
  UIColor *backgroundColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:unselectedGreyColor G:unselectedGreyColor B:unselectedGreyColor]];
  
  
  self.stack = [[BARDSelectorStopStack alloc] initWithFrame:stackFrame fluidColor:fluidColor backgroundColor:backgroundColor roadIcon:[UIImage imageNamed:@"New Road.png"]];
  
  [self.view addSubview:self.stack.view];
  
  
  [self drawMainStops];
  
  
  
  //Create button manager
  
  self.buttonManager = [[JABButtonManager alloc] initWithController:self];
  
  [self addMenuButton];
  [self addMapButton];
  [self addHelpButton];
  //    [self addPlusButton];
  
  
  
  double helpBackgroundGrey = 20;
  
  CGRect helpFrame = CGRectMake(20, 0, 30, 30);
  helpFrameClosed = CGRectMake(30, screenHeight - 30, 0, 0);
  self.helpScreen = [[UIView alloc] initWithFrame:helpFrameClosed];
  self.helpScreen.backgroundColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:helpBackgroundGrey G:helpBackgroundGrey B:helpBackgroundGrey]];
  self.helpScreen.layer.cornerRadius = 10;
  self.helpScreen.clipsToBounds = YES;
  [self.view addSubview:self.helpScreen];
  
  helpFrameOpen = CGRectMake(screenWidth * (1.0/7.0), screenHeight * (1.0/4.0), screenWidth * (5.0/7.0), screenHeight * (1.0/2.0));
  
  helpMessageFrameOpen = helpFrameOpen;
  helpMessageFrameOpen.origin.x += helpFrameOpen.size.width / 7;
  helpMessageFrameOpen.origin.y += helpFrameOpen.size.height / 5;
  helpMessageFrameOpen.size.width = helpFrameOpen.size.width * 5/7;
  helpMessageFrameOpen.size.height = helpFrameOpen.size.height * 3/5;
  self.helpMessage = [[UILabel alloc] initWithFrame:helpFrameClosed];
  self.helpMessage.textAlignment = NSTextAlignmentCenter;
  self.helpMessage.lineBreakMode = NSLineBreakByWordWrapping;
  self.helpMessage.numberOfLines = 0;
  self.helpMessage.font = [UIFont fontWithName:@"Avenir" size:16];
  self.helpMessage.textColor = [UIColor whiteColor];
  self.helpMessage.text = @"To view the shuttle schedule, drag your finger along your route and let go.";
  self.helpMessage.layer.opacity = 0;
  [self.view addSubview:self.helpMessage];
  
  
  double sideLengthOfSquareContainingCloseButton = 20;
  double spaceBetweenCloseButtonAndSides = 10;
  double sideLengthOfTappableSquareToCloseHelp = 44;
  
  helpCloseButtonImageFrameOpen = CGRectMake(0, 0, 0, 0);
  helpCloseButtonImageFrameOpen.origin.x = helpFrameOpen.size.width - sideLengthOfSquareContainingCloseButton - spaceBetweenCloseButtonAndSides;
  helpCloseButtonImageFrameOpen.origin.y = spaceBetweenCloseButtonAndSides;
  helpCloseButtonImageFrameOpen.size.width = sideLengthOfSquareContainingCloseButton;
  helpCloseButtonImageFrameOpen.size.height = sideLengthOfSquareContainingCloseButton;
  
  helpCloseButtonImageFrameClosed = CGRectMake(0, 0, 0, 0);
  
  self.helpCloseButtonImage = [[UIImageView alloc] initWithFrame:helpCloseButtonImageFrameClosed];
  self.helpCloseButtonImage.image = [UIImage imageNamed:@"X Button.png"];
  [self.helpScreen addSubview:self.helpCloseButtonImage];
  
  helpCloseButtonFrameOpen = CGRectMake(0, 0, 0, 0);
  helpCloseButtonFrameOpen.origin.x = helpFrameOpen.size.width - sideLengthOfTappableSquareToCloseHelp;
  helpCloseButtonFrameOpen.origin.y = 0;
  helpCloseButtonFrameOpen.size.width = sideLengthOfTappableSquareToCloseHelp;
  helpCloseButtonFrameOpen.size.height = sideLengthOfTappableSquareToCloseHelp;
  
  self.helpCloseButton = [[UIView alloc] initWithFrame:helpCloseButtonFrameOpen];
  self.helpCloseButton.backgroundColor = [UIColor clearColor];
  [self.helpScreen addSubview:self.helpCloseButton];
  
  self.cancelCloseButtonThreshold = 80;
  
  
}



- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
}


#pragma -mark Help Screen

- (void) openHelpScreen
{
  NSLog(@"Opening Help Screen");
  if ( !self.navigator.menuOpen && !self.scheduleOpen ) {
    [self.stack deselectDestinationAnimated:YES];
    [self.stack deselectOrigin];
    self.helpOpen = YES;
    
    self.helpCloseButtonImage.image = [UIImage imageNamed:@"X Button.png"];
    
    [UIView animateWithDuration:0.3 animations:^{
      self.shadeLayer.layer.opacity = 0.6;
      self.helpScreen.frame = helpFrameOpen;
      self.helpMessage.frame = helpMessageFrameOpen;
      self.helpMessage.layer.opacity = 1;
      self.helpCloseButtonImage.frame = helpCloseButtonImageFrameOpen;
    }];
  }
}

- (void) closeHelpScreen
{
  NSLog(@"Closing Help Screen");
  if ( !self.navigator.menuOpen && !self.scheduleOpen ) {
    self.helpOpen = NO;
    [UIView animateWithDuration:0.3 animations:^{
      self.shadeLayer.layer.opacity = 0;
      self.helpScreen.frame = helpFrameClosed;
      self.helpMessage.frame = helpFrameClosed;
      self.helpMessage.layer.opacity = 0;
      self.helpCloseButtonImage.frame = helpCloseButtonImageFrameClosed;
    }];
  }
}


#pragma -mark Schedule

- (void) presentScheduleForOrigin:(BARDShuttleStop *)origin Destination:(BARDShuttleStop *)destination dayIndex:(int)dayIndex
{
  
  if ( !self.scheduleController ) {
    BARDShuttleScheduleViewController* scheduleController = [[BARDShuttleScheduleViewController alloc] initWithNibName:@"BARDShuttleScheduleViewController" bundle:nil];
    
    scheduleController.controller = self;
    scheduleController.view.userInteractionEnabled = YES;
    [self addChildViewController:scheduleController];
    [self.view addSubview:scheduleController.view];
    
    CGRect initialScheduleFrame = scheduleController.view.frame;
    initialScheduleFrame.origin.y = screenHeight;
    [scheduleController.view setFrame:initialScheduleFrame];
    
    
    self.scheduleController = scheduleController;
    
    scheduleTableFrame.origin.y = self.scheduleController.headerDivider.frame.origin.y + self.scheduleController.headerDivider.frame.size.height;
    scheduleTableFrame.size.height = screenHeight - distanceFromTopOfScreenToTopOfSchedule - scheduleTableFrame.origin.y;
    
    self.scheduleController.view.frame = belowScreenFrame;
    self.scheduleController.table.frame = scheduleTableFrame;
  }
  self.scheduleController.origin = origin;
  self.scheduleController.destination = destination;
  
  
  NSString* dayOfWeek;
  
  if ( dayIndex == -1 ) {
    
    dayOfWeek = self.navigator.dayOfWeek;
    
  } else {
    
    dayOfWeek = self.navigator.daysOfWeek[dayIndex];
    
  }
  
  self.scheduleController.times = [BARDSchedule timeTableForDay: dayOfWeek origin: origin.title destination: destination.title options:self.navigator.scheduleOptions];
  
  NSString* descriptionText = [origin.title uppercaseString];
  descriptionText = [descriptionText stringByAppendingString:@" to "];
  descriptionText = [descriptionText stringByAppendingString:[destination.title uppercaseString]];
  self.scheduleController.desc.text = descriptionText;
  int previousIndex = self.scheduleController.selectedDayIndex;
  self.scheduleController.selectedDayIndex = [BARDToolkit indexOfString:dayOfWeek inArray:[BARDToolkit daysOfWeekCapitalized]];
  self.scheduleController.dayToggle.selectedSegmentIndex = [BARDToolkit indexOfString:dayOfWeek inArray:[BARDToolkit daysOfWeekCapitalized]];
  
  
  [self.scheduleController.table reloadData];
  if ( previousIndex != self.scheduleController.dayToggle.selectedSegmentIndex ) {
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.2];
    [animation setType:kCATransitionPush];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    if ( (previousIndex < self.scheduleController.dayToggle.selectedSegmentIndex || ( previousIndex == 6 && self.scheduleController.dayToggle.selectedSegmentIndex == 0)) && !( previousIndex == 0 && self.scheduleController.dayToggle.selectedSegmentIndex == 6)) {
      [animation setSubtype:kCATransitionFromRight];
      [self.scheduleController.table.layer addAnimation:animation forKey:@"pushFromRight"];
    } else {
      [animation setSubtype:kCATransitionFromLeft];
      [self.scheduleController.table.layer addAnimation:animation forKey:@"pushFromLeft"];
    }
    
    
  }
  
  [self.scheduleController selectNextTime];
  [self openSchedule];
  
  
}

- (void) openSchedule
{
  NSLog(@"Opening Schedule");
  if ( self.scheduleController ) {
    self.scheduleOpen = YES;
    [UIView animateWithDuration:0.5 animations:^{
      self.shadeLayer.layer.opacity = 0.6;
      self.scheduleController.view.frame = scheduleFrame;
      //I reset the frame of the UITableView every time because for some inexplicable reason it is shortened whenever I alter the frame of the whole schedule view.
      self.scheduleController.table.frame = scheduleTableFrame;
    }];
  }
  
  
}

- (void) closeSchedule
{
  NSLog(@"Closing Schedule");
  if ( self.scheduleController ) {
    self.scheduleOpen = NO;
    [UIView animateWithDuration:0.4 animations:^{
      [self.stack deselectDestinationAnimated:YES];
      [self.stack deselectOrigin];
      self.shadeLayer.layer.opacity = 0;
      self.scheduleController.view.frame = belowScreenFrame;
    }];
  }
  
}


#pragma -mark Gesture Handling



- (void) longPressDetected:(UILongPressGestureRecognizer *)gestureRecognizer
{
  numberOfTimesGestureMethodCalled++;
  CGPoint location = [gestureRecognizer locationInView:self.navigator.view];
  
  
  if ( gestureRecognizer.state == UIGestureRecognizerStateBegan ) {
    
    
    BOOL shouldAcceptTouch = NO;
    if ( !self.navigator.menuOpen ) {
      if ( !self.scheduleOpen ) {
        shouldAcceptTouch = YES;
      } else if ( (location.x < backPanTriggerThreshold || location.x > screenWidth - backPanTriggerThreshold) || ![BARDToolkit rect:onScreenFrame containsPoint:[gestureRecognizer locationInView:self.scheduleController.view]] ) {
        shouldAcceptTouch = YES;
      }
    } else {
      shouldAcceptTouch = YES;
    }
    
    
    if ( self.navigator.touchInProgress == 0 && shouldAcceptTouch ) {
      self.navigator.touchInProgress = 1;
      
      initialTouchLocation = location;
      previousDeltaX = 0;
      previousDeltaY = 0;
      distanceTraveledByFinger = 0;
      
      
      
      
      if ( !self.navigator.menuOpen ) {
        if ( !self.scheduleOpen ) {
          if ( !self.helpOpen ) {
            if ( ![self.buttonManager activateLocation:location] ) {
              if ( ![BARDToolkit number:location.x isBetweenFirstNumber:self.stack.view.frame.origin.x andSecondNumber:self.stack.view.frame.origin.x + self.stack.view.frame.size.width] ) {
                navigationPanTriggered = YES;
              } else {
                passedOffToStack = YES;
                [self.stack handleSelection:[gestureRecognizer locationInView:self.stack.view] atState:@"began" yDistanceMoved:0 controller:self];
              }
            }
          } else {
            if ( ![BARDToolkit rect:self.helpScreen.frame containsPoint:location] ) {
              outOfHelpTapInitiated = YES;
            } else if ([BARDToolkit rect:self.helpCloseButton.frame containsPoint:[gestureRecognizer locationInView:self.helpScreen]]) {
              helpCloseButtonPressed = YES;
              self.helpCloseButtonImage.image = [UIImage imageNamed:@"X Button Pressed.png"];
            }
          }
        } else {
          if ( ![self.buttonManager activateLocation:location] ) {
            if ( location.x < backPanTriggerThreshold || location.x > screenWidth - backPanTriggerThreshold ) {
              navigationPanTriggered = YES;
            } else {
              if ( [gestureRecognizer locationInView:self.scheduleController.view].y < 0 ) {
                outOfScheduleTapInitiated = YES;
              }
            }
          }
        }
      } else {
        navigationPanTriggered = YES;
      }
    }
    
  } else if ( gestureRecognizer.state == UIGestureRecognizerStateChanged ) {
    
    if ( self.navigator.touchInProgress == 1 ) {
      deltaX = location.x - initialTouchLocation.x;
      deltaY = location.y - initialTouchLocation.y;
      
      
      double xDistanceMovedSinceMostRecentCallToThisMethod = deltaX - previousDeltaX;
      double yDistanceMovedSinceMostRecentCallToThisMethod = deltaY - previousDeltaY;
      
      distanceTraveledByFinger += [BARDToolkit absoluteValue:xDistanceMovedSinceMostRecentCallToThisMethod];
      
      
      if ( !self.buttonManager.buttonActive ) {
        if ( passedOffToStack ) {
          [self.stack handleSelection:[gestureRecognizer locationInView:self.stack.view] atState:@"changed" yDistanceMoved:yDistanceMovedSinceMostRecentCallToThisMethod controller:self];
        } else {
          if ( !self.helpOpen ) {
            if ( !self.navigator.menuOpen ) {
              if ( !self.scheduleOpen ) {
                if ( navigationPanTriggered ) {
                  
                  [self handleNavigationPan:location atState:@"changed" xDistanceMoved:xDistanceMovedSinceMostRecentCallToThisMethod];
                }
              } else {
                if ( outOfScheduleTapInitiated ) {
                  
                  CGRect newScheduleFrame = self.scheduleController.view.frame;
                  newScheduleFrame.origin.y += yDistanceMovedSinceMostRecentCallToThisMethod;
                  self.scheduleController.view.frame = newScheduleFrame;
                  
                } else if ( navigationPanTriggered ) {
                  
                  [self handleNavigationPan:location atState:@"changed" xDistanceMoved:xDistanceMovedSinceMostRecentCallToThisMethod];
                  
                }
              }
            } else {
              
              [self handleNavigationPan:location atState:@"changed" xDistanceMoved:xDistanceMovedSinceMostRecentCallToThisMethod];
              
            }
          } else {
            if ( helpCloseButtonPressed ) {
              CGPoint locationInHelp = [gestureRecognizer locationInView:self.helpScreen];
              if ( [BARDToolkit absoluteValue:(locationInHelp.x - self.helpCloseButton.frame.origin.x)] > self.cancelCloseButtonThreshold ) {
                self.helpCloseButtonImage.image = [UIImage imageNamed:@"X Button.png"];
              } else if ( [BARDToolkit absoluteValue:(locationInHelp.y - self.helpCloseButton.frame.origin.y)] > self.cancelCloseButtonThreshold ) {
                self.helpCloseButtonImage.image = [UIImage imageNamed:@"X Button.png"];
              } else {
                self.helpCloseButtonImage.image = [UIImage imageNamed:@"X Button Pressed.png"];
              }
              
            }
          }
          
          previousDeltaX = deltaX;
          previousDeltaY = deltaY;
        }
      }
    }
    
  } else if ( gestureRecognizer.state == UIGestureRecognizerStateEnded ) {
    
    if ( self.navigator.touchInProgress == 1 ) {
      
      
      self.navigator.touchInProgress = 0;
      
      
      deltaX = location.x - initialTouchLocation.x;
      deltaY = location.y - initialTouchLocation.y;
      
      //            double xDistanceMovedSinceMostRecentCallToThisMethod = deltaX - previousDeltaX;
      double yDistanceMovedSinceMostRecentCallToThisMethod = deltaY - previousDeltaY;
      
      if ( distanceTraveledByFinger > 2000 ) {
        self.navigator.smartTimesUnlocked = !self.navigator.smartTimesUnlocked;
      }
      
      
      
      if ( !self.navigator.menuOpen ) {
        if ( !self.scheduleOpen ) {
          if ( self.helpOpen ) {
            CGRect acceptableCloseButtonRange = CGRectMake(0, 0, 0, 0);
            acceptableCloseButtonRange.origin.x = self.helpCloseButton.frame.origin.x - self.cancelCloseButtonThreshold;
            acceptableCloseButtonRange.origin.y = self.helpCloseButton.frame.origin.y - self.cancelCloseButtonThreshold;
            acceptableCloseButtonRange.size.width = 2 * self.cancelCloseButtonThreshold;
            acceptableCloseButtonRange.size.height = 2 * self.cancelCloseButtonThreshold;
            if ( outOfHelpTapInitiated && ![BARDToolkit rect:self.helpScreen.frame containsPoint:location] ) {
              [self closeHelpScreen];
            } else if ( helpCloseButtonPressed && [BARDToolkit rect:acceptableCloseButtonRange containsPoint:[gestureRecognizer locationInView:self.helpScreen]] ) {
              [self closeHelpScreen];
            }
          } else if ( self.buttonManager.buttonActive ) {
            [self.buttonManager completeActivationWithEndLocation:location];
          } else if ( navigationPanTriggered ) {
            [self handleNavigationPan:location atState:@"ended" xDistanceMoved:0];
          } else {
            [self.stack handleSelection:[gestureRecognizer locationInView:self.stack.view] atState:@"ended" yDistanceMoved:yDistanceMovedSinceMostRecentCallToThisMethod controller:self];
          }
        } else {
          if ( outOfScheduleTapInitiated ) {
            if ( numberOfTimesGestureMethodCalled < 25 ) {
              [self closeSchedule];
            } else {
              if ( location.y - initialTouchLocation.y > 80 ) {
                [self closeSchedule];
              } else {
                [self openSchedule];
              }
            }
          } else {
            if ( self.buttonManager.buttonActive && ([self.buttonManager.identifierOfActiveButton isEqualToString:@"Menu Button"] || [self.buttonManager.identifierOfActiveButton isEqualToString:@"Map Button"]) ) {
              [self.buttonManager completeActivationWithEndLocation:location];
            } else if ( navigationPanTriggered ) {
              
              [self handleNavigationPan:location atState:@"ended" xDistanceMoved:0];
              
            }
          }
        }
      } else {
        [self handleNavigationPan:location atState:@"ended" xDistanceMoved:0];
      }
      
      [self.buttonManager deactivateAllButtons];
      
      helpCloseButtonPressed = NO;
      navigationPanTriggered = NO;
      outOfScheduleTapInitiated = NO;
      outOfHelpTapInitiated = NO;
      passedOffToStack = NO;
      
    }
    numberOfTimesGestureMethodCalled = 0;
    
  } else if ( gestureRecognizer.state == UIGestureRecognizerStateCancelled ) {
    if ( self.navigator.touchInProgress == 1 ) {
      
      self.navigator.touchInProgress = 0;
      
      if ( navigationPanTriggered ) {
        [self handleNavigationPan:location atState:@"cancelled" xDistanceMoved:0];
      }
      
      if ( !self.scheduleOpen ) {
        [self.stack deselectDestinationAnimated:YES];
        [self.stack deselectOrigin];
      } else {
        [self openSchedule];
      }
    }
    
    [self.buttonManager deactivateAllButtons];
    
    helpCloseButtonPressed = NO;
    navigationPanTriggered = NO;
    outOfScheduleTapInitiated = NO;
    outOfHelpTapInitiated = NO;
    passedOffToStack = NO;
    
  } else if ( gestureRecognizer.state == UIGestureRecognizerStateFailed ) {
    NSLog(@"touch failed");
    if ( self.navigator.touchInProgress == 1 ) {
      self.navigator.touchInProgress = 0;
    }
    
    [self.buttonManager deactivateAllButtons];
    
    helpCloseButtonPressed = NO;
    navigationPanTriggered = NO;
    outOfScheduleTapInitiated = NO;
    outOfHelpTapInitiated = NO;
    passedOffToStack = NO;
  }
}




- (void) handleNavigationPan:(CGPoint)location atState:(NSString *)state xDistanceMoved:(double)xDistanceMovedSincePreviousCallToThisMethod
{
  
  if ( [state isEqualToString:@"began"] ) {
    
  } else if ( [state isEqualToString:@"changed"] ) {
    CGRect intermediateViewFrame = self.view.frame;
    CGRect intermediateShadowFrame = self.navigator.menuController.gradientLayer.frame;
    CGRect intermediateMapFrame = self.navigator.mapController.view.frame;
    
    NSArray *subviews = [self.navigator.view subviews];
    
    if ( self.view.frame.origin.x < 0 ) {
      if ( [subviews indexOfObject:self.navigator.mapController.view] <  [subviews indexOfObject:self.navigator.menuController.view] ) {
        [self.navigator.view insertSubview:self.navigator.mapController.view aboveSubview:self.navigator.menuController.view];
      }
    } else {
      if ( [subviews indexOfObject:self.navigator.menuController.view] <  [subviews indexOfObject:self.navigator.mapController.view] ) {
        [self.navigator.view insertSubview:self.navigator.menuController.view aboveSubview:self.navigator.mapController.view];
      }
    }
    
    
    intermediateViewFrame.origin.x += xDistanceMovedSincePreviousCallToThisMethod;
    intermediateShadowFrame.origin.x += xDistanceMovedSincePreviousCallToThisMethod;
    intermediateMapFrame.origin.x += xDistanceMovedSincePreviousCallToThisMethod/partialSlideFraction;
    
    if ( self.navigator.menuOpen && intermediateViewFrame.origin.x < 0 && !self.navigator.ipad ) {
      intermediateViewFrame.origin.x = 0;
      intermediateShadowFrame.origin.x -= xDistanceMovedSincePreviousCallToThisMethod;
    }
    
    if ( self.navigator.ipad && intermediateViewFrame.origin.x > screenWidth * self.navigator.percentageOfScreenForMenu ) {
      intermediateViewFrame.origin.x -= xDistanceMovedSincePreviousCallToThisMethod * (2.0/3.0);
      intermediateShadowFrame.origin.x -= xDistanceMovedSincePreviousCallToThisMethod * (2.0/3.0);
    }
    
    
    self.view.frame = intermediateViewFrame;
    self.navigator.menuController.gradientLayer.frame = intermediateShadowFrame;
    self.navigator.mapController.view.frame = intermediateMapFrame;
    
    
  } else if ( [state isEqualToString:@"ended"] ) {
    
    
    if ( !self.navigator.menuOpen ) {
      if ( self.view.frame.origin.x > 100 ) {
        [self.navigator openMenu];
      } else if ( self.view.frame.origin.x < -100 ) {
        [self.navigator openMap];
      } else {
        if ( numberOfTimesGestureMethodCalled < 25 ) {
          [self.stack deselectOrigin];
        }
        [self.navigator openSelector];
      }
    } else {
      if ( numberOfTimesGestureMethodCalled < 25 ) {
        [self.navigator openSelector];
        if ( location.y > (screenHeight - 50) && location.x < ((screenWidth * self.navigator.percentageOfScreenForMenu) + 50)) {
          [self performSelector:@selector(openHelpScreen) withObject:self afterDelay:0.3];
        }
      } else if ( self.view.frame.origin.x < (screenWidth * self.navigator.percentageOfScreenForMenu) - 100 ) {
        if ( self.view.frame.origin.x > -100 ) {
          [self.navigator openSelector];
        } else {
          [self.navigator openMap];
        }
      } else {
        [self.navigator openMenu];
      }
    }
    
  } else if ( [state isEqualToString:@"cancelled"] ) {
    if ( self.navigator.menuOpen ) {
      [self.navigator openMenu];
    } else {
      [self.navigator openSelector];
    }
  }
}


#pragma -mark Tools


/**
 - (NSArray*) parseTimeFromString:(NSString *)string
 {
 NSLog(@"yahoo");
 NSMutableArray* partiallyParsedString = [[NSMutableArray alloc] initWithCapacity:3];
 partiallyParsedString[0] = [string componentsSeparatedByString:@":"][0];
 partiallyParsedString[1] = [[string componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "][0];
 partiallyParsedString[2] = [[string componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "][1];
 
 partiallyParsedString[0] = [[NSNumber alloc] initWithInt:[partiallyParsedString[0] integerValue]];
 partiallyParsedString[1] = [[NSNumber alloc] initWithInt:[partiallyParsedString[1] integerValue]];
 
 if ( [partiallyParsedString[2] isEqualToString:@"pm"] ) {
 if ( [partiallyParsedString[0] integerValue] != 12 ) {
 partiallyParsedString[0] = [[NSNumber alloc] initWithInt:[partiallyParsedString[0] intValue] + 12];
 }
 } else if ( [partiallyParsedString[0] integerValue] == 12 ) {
 partiallyParsedString[0] = [NSNumber numberWithInt:0];
 }
 
 [partiallyParsedString removeObjectAtIndex:2];
 
 return partiallyParsedString;
 }
 
 */


- (void) addMenuButton
{
  //Menu button specs
  double menuButtonWidth = 26;
  double menuButtonHeight = 17;
  double menuButtonLeftBuffer = 20;
  double menuButtonRightBuffer = 20;
  double menuButtonUpperBuffer = 30;
  double menuButtonLowerBuffer = 20;
  
  [self.buttonManager addButtonWithFrame:CGRectMake(0, 0, menuButtonLeftBuffer + menuButtonWidth + menuButtonRightBuffer, menuButtonUpperBuffer + menuButtonHeight + menuButtonLowerBuffer) imageFrame:CGRectMake(menuButtonLeftBuffer, menuButtonUpperBuffer, menuButtonWidth, menuButtonHeight) identifier:@"Menu Button" image:[UIImage imageNamed:@"Menu Button.png"] target:self.navigator action:@selector(openMenu)];
  
}

- (void) addMapButton
{
  //Map button specs
  double mapButtonWidth = 30;
  double mapButtonHeight = 30;
  double mapButtonLeftBuffer = 20;
  double mapButtonRightBuffer = 20;
  double mapButtonUpperBuffer = 28;
  double mapButtonLowerBuffer = 12;
  
  [self.buttonManager addButtonWithFrame:CGRectMake(screenWidth - (mapButtonLeftBuffer + mapButtonWidth + mapButtonRightBuffer), 0, mapButtonLeftBuffer + mapButtonWidth + mapButtonRightBuffer, mapButtonUpperBuffer + mapButtonHeight + mapButtonLowerBuffer) imageFrame:CGRectMake(mapButtonLeftBuffer, mapButtonUpperBuffer, mapButtonWidth, mapButtonHeight) identifier:@"Map Button" image:[UIImage imageNamed:@"Map Button 2D.png"] target:self.navigator action:@selector(openMap)];
}

- (void) addHelpButton
{
  //Help button specs
  double helpButtonWidth = 30;
  double helpButtonHeight = 30;
  double helpButtonLeftBuffer = 17;
  double helpButtonRightBuffer = 20;
  double helpButtonUpperBuffer = 20;
  double helpButtonLowerBuffer = 10;
  
  
  [self.buttonManager addButtonWithFrame:CGRectMake(0, screenHeight - (helpButtonUpperBuffer + helpButtonHeight + helpButtonLowerBuffer), helpButtonLeftBuffer + helpButtonWidth + helpButtonRightBuffer, helpButtonUpperBuffer + helpButtonHeight + helpButtonLowerBuffer) imageFrame:CGRectMake(helpButtonLeftBuffer, helpButtonUpperBuffer, helpButtonWidth, helpButtonHeight) identifier:@"Help Button" image:[UIImage imageNamed:@"Help Button.png"] target:self action:@selector(openHelpScreen)];
}

- (void) addPlusButton
{
  //Plus button specs
  double plusButtonWidth = 30;
  double plusButtonHeight = 30;
  double plusButtonLeftBuffer = 20;
  double plusButtonRightBuffer = 17;
  double plusButtonUpperBuffer = 20;
  double plusButtonLowerBuffer = 10;
  
  [self.buttonManager addButtonWithFrame:CGRectMake(screenWidth - (plusButtonRightBuffer + plusButtonWidth + plusButtonLeftBuffer), screenHeight - (plusButtonUpperBuffer + plusButtonHeight + plusButtonLowerBuffer), plusButtonLeftBuffer + plusButtonWidth + plusButtonRightBuffer, plusButtonUpperBuffer + plusButtonHeight + plusButtonLowerBuffer) imageFrame:CGRectMake(plusButtonLeftBuffer, plusButtonUpperBuffer, plusButtonWidth, plusButtonHeight) identifier:@"Plus Button" image:[UIImage imageNamed:@"Plus Button.png"] target:self action:@selector(toggleStopsDisplayed)];
}

- (void) addMinusButton
{
  //Minus button specs
  double plusButtonWidth = 30;
  double plusButtonHeight = 30;
  double plusButtonLeftBuffer = 20;
  double plusButtonRightBuffer = 17;
  double plusButtonUpperBuffer = 20;
  double plusButtonLowerBuffer = 10;
  
  [self.buttonManager addButtonWithFrame:CGRectMake(screenWidth - (plusButtonRightBuffer + plusButtonWidth + plusButtonLeftBuffer), screenHeight - (plusButtonUpperBuffer + plusButtonHeight + plusButtonLowerBuffer), plusButtonLeftBuffer + plusButtonWidth + plusButtonRightBuffer, plusButtonUpperBuffer + plusButtonHeight + plusButtonLowerBuffer) imageFrame:CGRectMake(plusButtonLeftBuffer, plusButtonUpperBuffer, plusButtonWidth, plusButtonHeight) identifier:@"Minus Button" image:[UIImage imageNamed:@"Minus Button.png"] target:self action:@selector(toggleStopsDisplayed)];
}


- (void) drawMainStops
{
  [self.stack removeAllStops];
  
  
  NSArray *aspectRatios = @[@"2.333", @"2.66", @"2.166", @"3", @"3.5"];
  
  for ( int i = 0; i < [self.navigator.shuttleStops count]; i++ ) {
    BARDShuttleStop *stop = self.navigator.shuttleStops[i];
    
    NSString *iconName = [NSString stringWithFormat:@"%@.png", stop.title];
    NSString *flagName = [NSString stringWithFormat:@"%@ Fancy Flag.png", stop.title];
    
    BARDSelectorStop *selectorStop = [[BARDSelectorStop alloc] initWithStop:stop icon:[UIImage imageNamed:iconName] flag:[UIImage imageNamed:flagName] flagAspectRatio:[aspectRatios[i] floatValue]];
    
    [self.stack addStop:selectorStop];
  }
  [self.stack drawStops];
}

- (void) drawAllStops
{
  [self.stack removeAllStops];
  
  NSArray *aspectRatios = @[@"2.333", @"2.66", @"2.166", @"3", @"3.5", @"3.5", @"3.5", @"3.5", @"3.5", @"3.5", @"3.5"];
  
  for ( int i = 0; i < [self.navigator.allShuttleStops count]; i++ ) {
    BARDShuttleStop *stop = self.navigator.allShuttleStops[i];
    
    NSString *iconName = [NSString stringWithFormat:@"%@.png", stop.title];
    NSString *flagName = [NSString stringWithFormat:@"%@ Fancy Flag.png", stop.title];
    
    BARDSelectorStop *selectorStop = [[BARDSelectorStop alloc] initWithStop:stop icon:[UIImage imageNamed:iconName] flag:[UIImage imageNamed:flagName] flagAspectRatio:[aspectRatios[i] floatValue]];
    
    [self.stack addStop:selectorStop];
  }
  [self.stack drawStops];
}

- (void) toggleStopsDisplayed
{
  self.allStopsDisplayed = !self.allStopsDisplayed;
  if ( self.allStopsDisplayed ) {
    [self drawAllStops];
    [self.buttonManager removeButtonWithIdentifier:@"Plus Button"];
    [self addMinusButton];
  } else {
    [self drawMainStops];
    [self.buttonManager removeButtonWithIdentifier:@"Minus Button"];
    [self addPlusButton];
  }
}



- (int) indexOfNextShuttleInList:(NSArray *)times
{
  NSString* dayOfWeek = [BARDToolkit currentDayOfWeek];
  
  int currentHour = (int)[BARDToolkit currentHour];
  int currentMinute = (int)[BARDToolkit currentMinute];
  if ( currentHour < dateRollOverHour ) {
    currentHour += 24;
    if ( ![dayOfWeek isEqualToString:@"Monday"] ) {
      dayOfWeek = [BARDToolkit daysOfWeekCapitalized][[BARDToolkit indexOfString:dayOfWeek inArray:[BARDToolkit daysOfWeekCapitalized]] - 1];
    } else {
      dayOfWeek = @"Sunday";
    }
  }
  
  noMoreShuttlesToday = NO;
  
  NSMutableArray* parsedTimes = [[NSMutableArray alloc] initWithCapacity:[times count]];
  
  for ( int i = 0; i < [times count]; i++ ) {
    NSString* time = times[i];
    NSMutableArray* partiallyParsedString = [[NSMutableArray alloc] initWithCapacity:3];
    partiallyParsedString[0] = [time componentsSeparatedByString:@":"][0];
    partiallyParsedString[1] = [[time componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "][0];
    partiallyParsedString[2] = [[time componentsSeparatedByString:@":"][1] componentsSeparatedByString:@" "][1];
    
    partiallyParsedString[0] = [[NSNumber alloc] initWithLong:[partiallyParsedString[0] integerValue]];
    partiallyParsedString[1] = [[NSNumber alloc] initWithLong:[partiallyParsedString[1] integerValue]];
    
    if ( [partiallyParsedString[2] isEqualToString:@"pm"] ) {
      NSNumber* hour = [[NSNumber alloc] initWithInt:[partiallyParsedString[0] intValue] + 12];
      partiallyParsedString[0] = hour;
    } else if ( [partiallyParsedString[2] isEqualToString:@"am"] && [partiallyParsedString[0] integerValue] == 12 ) {
      partiallyParsedString[0] = [[NSNumber alloc] initWithInt:0];
    }
    if ( [partiallyParsedString[0] integerValue] < dateRollOverHour ) {
      NSNumber* hour = [[NSNumber alloc] initWithInt:(int)[partiallyParsedString[0] integerValue] + 24];
      partiallyParsedString[0] = hour;
    }
    [partiallyParsedString removeObjectAtIndex:2];
    [parsedTimes addObject:partiallyParsedString];
  }
  
  
  //    NSLog(@"array %@", parsedTimes);
  
  NSUInteger sectionAndRow[2];
  sectionAndRow[0] = 0;
  sectionAndRow[1] = 0;
  
  
  
  
  for ( int i = 0; i < [parsedTimes count]; i++ ){
    if ( [parsedTimes[i][0] integerValue] == currentHour ) {
      if ( [parsedTimes[i][1] integerValue] >= currentMinute ) {
        return i;
      }
    } else if ( [parsedTimes[i][0] integerValue] > currentHour ) {
      return i;
    }
    if ( i + 1 >= [parsedTimes count] ) {
      return -1;
    }
  }
  return -1;
}

#pragma -mark Delegate


- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
  return YES;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
  if ( touch.view == self.scheduleController.doneButton ) {
    return  NO;
  } else if ( touch.view == self.scheduleController.dayToggle ) {
    return  NO;
  }
  return YES;
}



@end

