//
//  BARDShuttleScheduleViewController.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/20/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDShuttleScheduleViewController.h"
#import "BARDScheduleSelectorViewController.h"
#import "BARDNavigationController.h"
#import "BARDSelectorStop.h"


@interface BARDShuttleScheduleViewController () {
    
    CGPoint initialTouchLocation;
    CGPoint finalTouchLocation;
    
    CGPoint initialTouchLocationInSchedule;
    
    double deltaX;
    double deltaY;
    
    double previousDeltaX;
    double previousDeltaY;
    
    BOOL navigationSwipeDetected;
    BOOL leftSideSwipeDetected;
    BOOL rightSideSwipeDetected;
    
    CGRect onScreenFrame;
    CGRect offScreenFrameRight;
    CGRect offScreenFrameSlightlyRight;
    CGRect offScreenFrameLeft;
    
    
    
}

@end

@implementation BARDShuttleScheduleViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.table reloadData];
}
    
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    tableData = [NSArray arrayWithObjects:@"Tivoli", @"Robbins", @"Kline", @"Red Hook", nil];

    
    self.backPanTriggerThresholdTemp = 25;

    
    
    
    onScreenFrame = CGRectMake(0, 0, screenWidth, screenHeight);
    offScreenFrameLeft = CGRectMake(-screenWidth, 0, screenWidth, screenHeight);
    offScreenFrameRight = CGRectMake(screenWidth, 0, screenWidth, screenHeight);
    offScreenFrameSlightlyRight = CGRectMake(screenWidth/partialSlideFraction, 0, screenWidth, screenHeight);
    

    self.longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressDetected:)];
    self.longPressRecognizer.delegate = self;
    self.longPressRecognizer.minimumPressDuration = 0.01;
    self.longPressRecognizer.allowableMovement = screenHeight;
    [self.view addGestureRecognizer:self.longPressRecognizer];
    
    CGRect tableFrame = self.table.frame;
    tableFrame.origin.y = self.headerDivider.frame.origin.y + self.headerDivider.frame.size.height;
    tableFrame.size.height = screenHeight - 130 - (self.headerDivider.frame.origin.y + self.headerDivider.frame.size.height);
    
    self.table.frame = tableFrame;
    self.table.allowsSelection = NO;
    
    
    UIFont *font = [UIFont fontWithName:@"Avenir" size:14];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:UITextAttributeFont];
    [self.dayToggle setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    self.dayToggle.tintColor = [UIColor blackColor];
    
    self.view.userInteractionEnabled = YES;
    
    [self.dayToggle addTarget:self action:@selector(presentNewSchedule) forControlEvents:UIControlEventValueChanged];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkIfSelectionUpdateNecessary) userInfo:self repeats:YES];
    
    
    if ( self.controller.navigator.ipad ) {
        
        CGRect changedFrame = self.dayToggle.frame;
        changedFrame.size.width = screenWidth*(1.0/2.0);
        changedFrame.origin.x = screenWidth*(1.0/4.0);
        self.dayToggle.frame = changedFrame;
        
        changedFrame = self.headerDivider.frame;
        changedFrame.size.width = screenWidth;
        self.headerDivider.frame = changedFrame;
        
        changedFrame = self.desc.frame;
        changedFrame.size.width = screenWidth;
        self.desc.frame = changedFrame;
        
        changedFrame = self.table.frame;
        changedFrame.size.height = 240;
        self.table.frame = changedFrame;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)done:(id)sender
{
    [self.controller closeSchedule];
}


- (void) checkIfSelectionUpdateNecessary
{
    long currentHour = [BARDToolkit currentHour];
    long currentMinute = [BARDToolkit currentMinute];
    
    if ( currentHour < dateRollOverHour ) {
        currentHour += 24;
    }
    
    if ( currentHour > [self.nextTime[0] integerValue] ) {
        [self selectNextTime];
    } else if ( currentHour == [self.nextTime[0] integerValue] ) {
        if ( currentMinute > [self.nextTime[1] integerValue] ) {
            [self selectNextTime];
        }
    }
    
}

- (void) selectNextTime
{
    NSLog(@"Selecting new time in schedule");
    NSString* dayOfWeek = [BARDToolkit currentDayOfWeek];
    int indexOfToday = [BARDToolkit indexOfString:dayOfWeek inArray:[BARDToolkit daysOfWeekCapitalized]];

    long currentHour = [BARDToolkit currentHour];
    long currentMinute = [BARDToolkit currentMinute];
    if ( currentHour < dateRollOverHour ) {
        currentHour += 24;
        if ( !indexOfToday == 0 ) {
            indexOfToday--;
        } else {
            indexOfToday = 6;
        }
    }


    
    if ( self.dayToggle.selectedSegmentIndex == indexOfToday ) {
        
        

        NSMutableArray* times = [self.times mutableCopy];
        
        
        for ( int i = 0; i < [times count]; i++ ) {
            NSMutableArray* time = [BARDToolkit convertTimeStringTo24HourTime:times[i]];
            if ( [time[0] integerValue] < dateRollOverHour ) {
                time[0] = [NSNumber numberWithLong:[time[0] integerValue] + 24];
            }
            times[i] = time;
        }
        
        NSArray* currentTime = @[[NSNumber numberWithLong:currentHour], [NSNumber numberWithLong:currentMinute]];
        
        
        
        int indexOfNextTime = -1;
        BOOL done = NO;
        int counter = 0;
        while ( !done ) {
            if ( counter == [times count] ) {
                done = YES;
            } else {
                NSArray* timeInterval = [BARDToolkit timeIntervalBetweenTime:currentTime andTime:times[counter]];
                if ( [timeInterval[0] integerValue] >= 0 && [timeInterval[1] integerValue] >= 0 ) {
                    done = YES;
                    indexOfNextTime = counter;
                }
            }
            counter++;
        }
        
        self.nextTimeIndex = indexOfNextTime;
        
        [self.table reloadData];
        
        
        NSUInteger sectionAndRow[2];
        sectionAndRow[0] = 0;
        sectionAndRow[1] = indexOfNextTime;
        
        if ( indexOfNextTime != -1 ) {
            self.nextTime = times[indexOfNextTime];
            [self.table scrollToRowAtIndexPath:[[NSIndexPath alloc] initWithIndexes:sectionAndRow length:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            if ( indexOfNextTime != 0 ) {
                sectionAndRow[1] = indexOfNextTime - 1;
            }
            [self.table scrollToRowAtIndexPath:[[NSIndexPath alloc] initWithIndexes:sectionAndRow length:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        } else {
            NSArray* largeTime = @[[NSNumber numberWithLong:40], [NSNumber numberWithLong:0]];
            self.nextTime = [largeTime mutableCopy];
            sectionAndRow[1] = [self.table numberOfRowsInSection:0] - 1;
            if ( sectionAndRow[1] != -1 ) {
                [self.table scrollToRowAtIndexPath:[[NSIndexPath alloc] initWithIndexes:sectionAndRow length:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
            
        }
    }
    
}



- (void) presentNewSchedule
{
    [self.controller presentScheduleForOrigin:self.origin Destination:self.destination dayIndex:(int)self.dayToggle.selectedSegmentIndex];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void) resetViewFrame
{
    CGRect offRightFrame = CGRectMake(320, 0, 320, self.view.frame.size.height);
    [self.view setFrame:offRightFrame];
}


- (void) longPressDetected:(UIPanGestureRecognizer *)longPressRecognizer
{
    CGPoint location = [longPressRecognizer locationInView:self.controller.navigator.view];
    
    
    if ( !self.controller.navigator.menuOpen ) {
        if ( longPressRecognizer.state == UIGestureRecognizerStateBegan ) {
            
            BOOL shouldAcceptTouch = NO;
            if ( !self.controller.navigator.menuOpen ) {
                if ( !(location.x < backPanTriggerThreshold || location.x > screenWidth - backPanTriggerThreshold) && [BARDToolkit rect:onScreenFrame containsPoint:[longPressRecognizer locationInView:self.view]] ) {
                    shouldAcceptTouch = YES;
                }
            }
            
            
            if ( self.controller.navigator.touchInProgress == 0 && shouldAcceptTouch ) {
                
                self.controller.navigator.touchInProgress = 2;
                
                
                navigationSwipeDetected = NO;
                leftSideSwipeDetected = NO;
                rightSideSwipeDetected = NO;
                
                if ( location.x <= self.backPanTriggerThresholdTemp ) {
                    navigationSwipeDetected = YES;
                    leftSideSwipeDetected = YES;
                } else if ( location.x >= screenWidth - self.backPanTriggerThresholdTemp ) {
                    navigationSwipeDetected = YES;
                    rightSideSwipeDetected = YES;
                }
                
                initialTouchLocation = location;
                initialTouchLocationInSchedule = [longPressRecognizer locationInView:self.view];
                
                previousDeltaX = 0;
                previousDeltaY = 0;
            }
            
            
        } else if ( longPressRecognizer.state == UIGestureRecognizerStateChanged ) {
            
            if ( self.controller.navigator.touchInProgress == 2 ) {
                
                deltaX = location.x - initialTouchLocation.x;
                deltaY = location.y - initialTouchLocation.y;
                
                double xDistanceMovedSinceMostRecentCallToThisMethod = deltaX - previousDeltaX;
                double yDistanceMovedSinceMostRecentCallToThisMethod = deltaY - previousDeltaY;
                
                if ( initialTouchLocationInSchedule.y < self.headerDivider.frame.origin.y + self.headerDivider.frame.size.height ) {
                    CGRect newFrame;
                    if ( !leftSideSwipeDetected && !rightSideSwipeDetected ) {
                        newFrame = self.view.frame;
                    } else {
                        newFrame = self.controller.view.frame;
                    }
                    
                    CGRect mapFrame = self.controller.navigator.mapController.view.frame;
                    
                    if ( leftSideSwipeDetected ) {
                        
                        //            newFrame.origin.x += xDistanceMovedSinceMostRecentCallToThisMethod;
                        
                    } else if ( rightSideSwipeDetected ) {
                        
                        newFrame.origin.x += xDistanceMovedSinceMostRecentCallToThisMethod;
                        mapFrame.origin.x += xDistanceMovedSinceMostRecentCallToThisMethod/partialSlideFraction;
                        
                    } else {
                        newFrame.origin.y += yDistanceMovedSinceMostRecentCallToThisMethod;
                        if ( newFrame.origin.y < 0 ) {
                            newFrame.origin.y = 0;
                        }
                        [self.view setFrame:newFrame];
                    }
                    
                    if ( leftSideSwipeDetected ) {
                        
                    } else if ( rightSideSwipeDetected ) {
                        self.controller.view.frame = newFrame;
                        self.controller.navigator.mapController.view.frame = mapFrame;
                    }
                }
                
                previousDeltaX = deltaX;
                previousDeltaY = deltaY;
                
            }
            
        } else if ( longPressRecognizer.state == UIGestureRecognizerStateEnded ) {
            
            if ( self.controller.navigator.touchInProgress == 2 ) {
                
                self.controller.navigator.touchInProgress = 0;
                
                deltaX = location.x - initialTouchLocation.x;
                deltaY = location.y - initialTouchLocation.y;
                
                if ( leftSideSwipeDetected ) {
                    
                } else if ( rightSideSwipeDetected ) {
                    
                    if ( deltaX < -130 ) {
                        [UIView animateWithDuration:0.3 animations:^{
                            self.controller.view.frame = offScreenFrameLeft;
                            self.controller.navigator.mapController.view.frame = onScreenFrame;
                        }];
                        
                    } else {
                        [UIView animateWithDuration:0.3 animations:^{
                            self.controller.view.frame = onScreenFrame;
                            self.controller.navigator.mapController.view.frame = offScreenFrameSlightlyRight;
                        }];
                    }
                    
                } else {
                    if ( initialTouchLocationInSchedule.y < self.headerDivider.frame.origin.y + self.headerDivider.frame.size.height ) {
                        if ( deltaY > 120 ) {
                            [self done:self];
                        } else {
                            [self.controller openSchedule];
                        }
                    } else {
                        if ( deltaX > 80 ) {
                            if ( self.dayToggle.selectedSegmentIndex != 0 ) {
                                self.dayToggle.selectedSegmentIndex--;
                                [self presentNewSchedule];
                            } else {
                                self.dayToggle.selectedSegmentIndex = 6;
                                [self presentNewSchedule];
                            }
                        } else if ( deltaX < -80) {
                            if ( self.dayToggle.selectedSegmentIndex != 6 ) {
                                self.dayToggle.selectedSegmentIndex++;
                                [self presentNewSchedule];
                            } else {
                                self.dayToggle.selectedSegmentIndex = 0;
                                [self presentNewSchedule];
                            }
                        }
                    }
                }
            }
        } else if ( longPressRecognizer.state == UIGestureRecognizerStateCancelled ) {
            self.controller.navigator.touchInProgress = 0;
        }
    }
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( [self.times count] != 0 ) {
        return [self.times count];
    } else {
        return 3;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( [self.times count] != 0 ) {
        
        static NSString *MyIdentifier = @"MyReuseIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
        }
        
        cell.textLabel.text = self.times[indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"Avenir" size:16];
        UIView* cellBackground = [[UIView alloc] init];
        if ( (indexPath.row == self.nextTimeIndex) && (self.selectedDayIndex == self.controller.navigator.dayOfWeekIndex) ) {
            cellBackground.backgroundColor = [UIColor colorFromHexString:[BARDToolkit hexcodeFromR:130 G:130 B:130]];
            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth - 165, 0, 150, 44)];
            label.text = @"Next Shuttle";
            label.font = [UIFont fontWithName:@"Avenir Heavy" size:16];
            label.textAlignment = NSTextAlignmentRight;
            [cellBackground addSubview:label];
        } else {
            cellBackground.backgroundColor = [UIColor whiteColor];
        }
        
        cell.backgroundView = cellBackground;
        
        return cell;
    } else {
        
        static NSString *MyIdentifier = @"MyReuseIdentifierMessageCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
        }
        
        if ( indexPath.row == 2 ) {
            cell.textLabel.text = @"There are no shuttles on this day";
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:@"Avenir Heavy" size:16];
        }
        return cell;
    }
    
    
    
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ( touch.view == self.dayToggle ) {
        
        return NO;
        
    } else if ( [touch locationInView:self.controller.navigator.view].x >= screenWidth - self.backPanTriggerThresholdTemp ) {
        
        return NO;
        
    } else if ( [touch locationInView:self.controller.navigator.view].x <= self.backPanTriggerThresholdTemp ) {
        
        return NO;
        
    } else if ( touch.view == self.doneButton ) {
        
        return NO;
        
    }
//    else if ( [BARDToolkit rect:self.table.frame containsPoint:[touch locationInView: self.view]] ) {
//        
//        return NO;
//        
//    }
    return YES;
}


@end
