//
//  BARDMapViewController.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 9/20/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BARDScheduleSelectorViewController.h"
#import "BARDShuttle.h"

@class BARDNavigationController;

/**
 This controls the map.
 */
@interface BARDMapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) BARDNavigationController* navigator;

@property (weak, nonatomic) IBOutlet UIImageView *menuButtonImage;
@property (weak, nonatomic) IBOutlet UIButton *backToCampusButton;
@property (weak, nonatomic) IBOutlet MKMapView *map;
//@property (strong, nonatomic) NSDictionary *shuttleStops;
@property (strong, nonatomic) NSDictionary *mapAnnotationViews;
@property (strong, nonatomic) BARDShuttleStop *shuttle;
@property (strong, nonatomic) UILongPressGestureRecognizer *longPressRecognizer;
@property (strong, nonatomic) UIGestureRecognizer *menuButtonTapRecognizer;
@property (strong, nonatomic) UIGestureRecognizer *clockButtonTapRecognizer;
@property (strong, nonatomic) UIGestureRecognizer *supplementalRecognizer;
@property (strong, nonatomic) NSString *shadowColor;
@property (nonatomic) MKCoordinateRegion initialRegion;

@property (nonatomic) BOOL shuttleDisplayed;

@property (nonatomic) BOOL followShuttle;
@property (weak, nonatomic) IBOutlet UILabel *followShuttleToggleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *followShuttleToggle;

@property (strong, nonatomic) UILabel *noGPSNotice;

@property (strong, nonatomic) NSMutableArray *mainShuttleStopPoints;



//- (IBAction)toLocation:(UIButton *)sender;
- (IBAction)backToBard:(id)sender;

- (void) generateStaticAnnotations;


- (void) moveShuttle;

- (void) testMethod;




@end