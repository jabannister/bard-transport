//
//  BARDNavigationController.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/24/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "BARDAppDelegate.h"
#import "JABGlobalInfo.h"

#import "BARDShuttleScheduleViewController.h"
#import "BARDMenuViewController.h"
#import "BARDMapViewController.h"

#import "BARDShuttleStop.h"



extern int partialSlideFraction;
extern int backPanTriggerThreshold;
extern int dateRollOverHour;
extern int menuOriginX;

@interface BARDNavigationController : UIViewController <CLLocationManagerDelegate>


//App Parameters
@property (strong, nonatomic) NSString *versionNumber;
@property (nonatomic) double partialSlideFactor;
@property (nonatomic) double percentageOfScreenForMenu;
@property (nonatomic) double shadowWidth;
@property (nonatomic) CGRect closedMenuSelectorShadowFrame;

//App State
@property (nonatomic) NSString *scheduleOptions;
@property (nonatomic) BOOL menuOpen;
@property (nonatomic) BOOL mapOpen;
@property (nonatomic) BOOL ipad;
@property (nonatomic) BOOL smartTimesUnlocked;
@property (nonatomic) int touchInProgress;


@property (strong, nonatomic) NSString *currentVersionNumber;
@property (nonatomic) BOOL isAppLockedDown;


//Date and Time
@property (strong, nonatomic) NSString* dayOfWeek;
@property (nonatomic) int dayOfWeekIndex;
@property (nonatomic) long currentHour;
@property (nonatomic) long currentMinute;
@property (nonatomic) long currentSecond;



@property (strong, nonatomic) NSArray* daysOfWeek;


//Navigated Child ViewControllers
@property (strong, nonatomic) BARDMenuViewController* menuController;
@property (strong, nonatomic) BARDMapViewController* mapController;
@property (strong, nonatomic) BARDScheduleSelectorViewController* scheduleSelectorController;


//Shuttle Stops
@property (strong, nonatomic) NSArray *shuttleStops;
@property (strong, nonatomic) NSArray *allShuttleStops;



//Location Stuff
@property (strong, nonatomic) CLLocationManager* locationManager;
@property (nonatomic) CLLocation* mostRecentUserLocation;
@property (nonatomic) BOOL mostRecentUserLocationIsValid;
@property (nonatomic) BOOL newLocation;



//Tweets
@property (nonatomic) CLLocationCoordinate2D latestLocationTweet;
@property (strong, nonatomic) NSArray *timeOfLatestLocationTweet;
@property (nonatomic) CLLocationCoordinate2D latestAnnouncementTweet;




//Update Notification Components
@property (strong, nonatomic) UIView *updateNotificationScreen;
@property (strong, nonatomic) UIView *updateButton;
@property (strong, nonatomic) UILabel *updateButtonLabel;
@property (strong, nonatomic) UIView *updateButtonTopLine;
@property (strong, nonatomic) UIView *screenDarkener;


//Lockdown Notification Components
@property (strong, nonatomic) UIView *lockdownNotificationScreen;
@property (strong, nonatomic) UILabel *lockdownLabel;


@property (nonatomic) BOOL test;





- (id) init;

- (void) updateDateAndTime;


- (void) openMap;
- (void) openMenu;
- (void) openSelector;


- (void) openUpdateNotificationScreen;
- (void) closeUpdateNotificationScreen;

- (void) screenDarkenerLongPressDetected: (UILongPressGestureRecognizer*) gestureRecognizer;

- (void) checkIfAppIsUpToDate;
- (void) updateCurrentVersion;

- (void) checkIfProperTimeToQuery;
- (void) queryTwitter;


- (void) forceLocationUpdate;


@end
