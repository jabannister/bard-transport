//
//  BARDMenuViewController.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/11/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BARDShuttleStop.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@class BARDNavigationController;

@interface BARDMenuViewController : UIViewController <UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView* smartTimesPane;

@property (weak, nonatomic) IBOutlet UIView* smartTimesTapField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView* updatingLocationIndicatorView;
@property (weak, nonatomic) IBOutlet UILabel* updatingLabel;

@property (weak, nonatomic) IBOutlet UILabel* northboundTitle;
@property (weak, nonatomic) IBOutlet UILabel* northboundOriginLabel;
@property (weak, nonatomic) IBOutlet UILabel* northboundHours;
@property (weak, nonatomic) IBOutlet UILabel* northboundHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel* northboundMinutes;
@property (weak, nonatomic) IBOutlet UILabel* northboundMinutesLabel;
@property (strong, nonatomic) IBOutlet UILabel* northMessage;

@property (weak, nonatomic) IBOutlet UIView *smartTimesDivider;
@property (weak, nonatomic) IBOutlet UIView *menuHeaderDivider;

@property (weak, nonatomic) IBOutlet UILabel* southboundTitle;
@property (weak, nonatomic) IBOutlet UILabel* southboundOriginLabel;
@property (weak, nonatomic) IBOutlet UILabel* southboundHours;
@property (weak, nonatomic) IBOutlet UILabel* southboundHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel* southboundMinutes;
@property (weak, nonatomic) IBOutlet UILabel* southboundMinutesLabel;
@property (strong, nonatomic) IBOutlet UILabel* southMessage;


@property (strong, nonatomic) IBOutlet UILabel* smartTimesErrorMessage;

@property (nonatomic) BOOL tooFarFromCampusMessageDisplayed;
@property (nonatomic) BOOL noLocationDataMessageDisplayed;

@property (strong, nonatomic) UIView *gradientLayer;

@property (nonatomic) double visibleRange;

@property (nonatomic) long hourOfMostRecentUpdate;
@property (nonatomic) long minuteOfMostRecentUpdate;


@property (weak, nonatomic) IBOutlet UILabel* mainMenuTitle;

@property (weak, nonatomic) IBOutlet UIButton* backToMainMenuButton;


@property (weak, nonatomic) IBOutlet UIView* menuPane;
@property (weak, nonatomic) IBOutlet UITableView* menu;

@property (strong, nonatomic) UITextView* menuItemMessage;


@property (strong, nonatomic) BARDNavigationController* navigator;

- (IBAction) backToMainMenu: (id) sender;

- (void) checkIfUpdateNecessary;

- (void) smartTimesPaneTouched: (UILongPressGestureRecognizer*) gestureRecognizer;

- (void) startLocationUpdateAnimation;
- (void) stopLocationUpdateAnimation;

- (void) updateSmartTimesPaneSentFromLocationManager:(BOOL) sentFromLocationManager;
- (void) loadSmartTimesPaneWithClosestNorthboundStop: (BARDShuttleStop*) closestNorthboundStop closestSouthboundStop: (BARDShuttleStop*) closestSouthboundStop hoursUntilSouth: (int) hoursUntilSouth minutesUntilSouth: (int) minutesUntilSouth hoursUntilNorth: (int) hoursUntilNorth minutesUntilNorth: (int) minutesUntilNorth;

- (void) displayNoLocationDataMessage;
- (void) displayTooFarFromCampusMessage;
- (void) displayNoWeekendSummerShuttlesMessage;


- (NSArray*) labelFramesForHoursUntil: (int) hoursUntil minutesUntil: (int) minutesUntil northbound:(BOOL) northbound;

- (void) displayMenuItemAtIndex: (NSIndexPath*) indexPath;
- (void) resizeMenuMessageToHeight: (double) height;

@end
