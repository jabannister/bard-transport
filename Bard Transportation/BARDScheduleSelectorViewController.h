//
//  BARDScheduleSelectorViewController.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/18/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BARDSchedule.h"
#import "BARDShuttleStop.h"
#import "BARDSelectorStopStack.h"
#import "BARDSelectorStop.h"
#import "JABButtonManager.h"
#import "BARDCanvas.h"
#import "BARDTestLine.h"
#import "BARDShuttleScheduleViewController.h"

@class BARDNavigationController;

@interface BARDScheduleSelectorViewController : UIViewController <UIGestureRecognizerDelegate>

@property (strong, nonatomic) BARDNavigationController *navigator;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (strong, nonatomic) BARDSelectorStopStack *stack;
@property (strong, nonatomic) JABButtonManager *buttonManager;

@property (strong, nonatomic) NSTimer *scheduleUpdater;

@property (nonatomic) CGRect selectorMenuOpenFrame;

@property (strong, nonatomic) BARDShuttleScheduleViewController *scheduleController;
@property (nonatomic) BOOL scheduleOpen;

@property (strong, nonatomic) IBOutlet UIView *shadeLayer;

@property (strong, nonatomic) UIView *helpScreen;
@property (strong, nonatomic) UILabel *helpMessage;
@property (strong, nonatomic) UIView *helpCloseButton;
@property (strong, nonatomic) UIImageView *helpCloseButtonImage;
@property (nonatomic) double cancelCloseButtonThreshold;
@property (nonatomic) BOOL helpOpen;

@property (strong, nonatomic) UILabel *summerLabel;

@property (strong, nonatomic) BARDSchedule *schedule;

@property (nonatomic) BOOL allStopsDisplayed;


- (void) openHelpScreen;
- (void) closeHelpScreen;

- (void) presentScheduleForOrigin: (BARDShuttleStop*) origin Destination: (BARDShuttleStop*) destination dayIndex: (int) dayIndex;
- (void) openSchedule;
- (void) closeSchedule;

- (void) longPressDetected: (UILongPressGestureRecognizer*) gestureRecognizer;
- (void) handleNavigationPan: (CGPoint) location atState: (NSString*) state xDistanceMoved: (double) xDistanceMovedSincePreviousCallToThisMethod;

- (void) addMenuButton;
- (void) addMapButton;
- (void) addHelpButton;
- (void) addPlusButton;
- (void) addMinusButton;

- (void) drawMainStops;
- (void) drawAllStops;
- (void) toggleStopsDisplayed;

//- (NSArray*) parseTimeFromString: (NSString*) string;
- (int) indexOfNextShuttleInList:(NSArray*) times;





@end
