//
//  BARDSelectorStopStack.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 6/6/14.
//  Copyright (c) 2014 Bard College. All rights reserved.
//

#import "BARDSelectorStopStack.h"
#import "BARDScheduleSelectorViewController.h"

@interface BARDSelectorStopStack () {
}

- (void) clearContent;

@end

@implementation BARDSelectorStopStack



- (id) initWithFrame:(CGRect)frame fluidColor:(UIColor *)fluidColor backgroundColor:(UIColor *)backgroundColor roadIcon:(UIImage *)roadIcon
{
    
    self = [super init];
    
    if ( self ) {
        
        CGRect subviewFrame = frame;
        subviewFrame.origin.x = 0;
        
        self.view = [[UIView alloc] initWithFrame:frame];
        self.view.backgroundColor = [UIColor clearColor];
        
        
        self.maskLayer = [[UIView alloc] initWithFrame:subviewFrame];
        self.maskLayer.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.maskLayer];
        
        
        self.background = [[UIView alloc] initWithFrame:subviewFrame];
        self.background.backgroundColor = backgroundColor;
        [self.maskLayer addSubview:self.background];
        
        
        self.selectionFluid = [[UIView alloc] initWithFrame:subviewFrame];
        self.selectionFluid.backgroundColor = fluidColor;
        self.selectionFluid.layer.opacity = 0;
        [self.maskLayer addSubview:self.selectionFluid];
        
        
        self.canvas = [[BARDCanvas alloc] initWithFrame:subviewFrame];
        self.canvas.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.canvas];
        
        
        self.stack = [[NSMutableArray alloc] initWithCapacity:11];
        
        _circleThickness = 2;
        
        self.roadIcon = roadIcon;
        self.roadViews = [[NSMutableArray alloc] initWithCapacity:10];
    }
    
    return self;
    
}



- (void) addStop:(BARDSelectorStop *)stop
{
    stop.index = [self.stack count];
    [self.stack addObject:stop];
}

- (void) insertStop:(BARDSelectorStop *)stop atIndex:(int)index
{
    [self.stack insertObject:stop atIndex:index];
}

- (void) removeStopAtIndex:(int)index
{
    if ( index < [self.stack count] ) {
        BARDSelectorStop *stop = self.stack[index];
        stop.index = -1;
        [stop removeFromSuperview];
        [self.stack removeObject:stop];
        
        for ( int i = index; i < [self.stack count]; i++ ) {
            BARDSelectorStop *stop = self.stack[i];
            stop.index--;
        }
        
    }
    
}

- (void) removeAllStops
{
    for ( int i = [self.stack count] - 1; i >= 0; i-- ) {
        [self removeStopAtIndex:i];
    }
}


- (void) setCircleThickness:(double)circleThickness
{
    self.circleThickness = circleThickness;
}



- (void) clearContent
{
    for ( BARDSelectorStop *stop in self.stack ) {
        [stop.flagMask removeFromSuperview];
        [stop removeFromSuperview];
    }
    for ( UIImageView *roadView in self.roadViews ) {
        [roadView removeFromSuperview];
    }
    [self.roadViews removeAllObjects];
    
    [self.canvas clearCanvas];
    [self.canvas removeFromSuperview];
    [self.selectionFluid removeFromSuperview];
    [self.background removeFromSuperview];
    [self.maskLayer removeFromSuperview];
    
    for ( UIImageView *roadView in self.roadViews ) {
        [roadView removeFromSuperview];
    }
}


- (void) drawStops
{
    [self clearContent];
    
    if ( [self.stack count] > 0 ) {
        for ( BARDSelectorStop *stop in self.stack ) {
            [self.view addSubview:stop.flagMask];
        }
        
        for ( int i = 0; i < [self.stack count] - 1; i++ ) {
            UIImageView *roadView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            roadView.image = self.roadIcon;
            [self.roadViews addObject:roadView];
            [self.view addSubview:roadView];
        }
        
        [self.view addSubview:self.maskLayer];
        [self.maskLayer addSubview:self.background];
        [self.maskLayer addSubview:self.selectionFluid];
        [self.view addSubview:self.canvas];
        
        
        //Calculate dimensions of circles
        int numberOfStops = [self.stack count];
        double distanceFromTopToFirstStop = 50;
        double distanceFromLastStopToBottom = 30;
        double heightOfView = self.view.frame.size.height;
        double heightOfStopColumn = heightOfView - (distanceFromTopToFirstStop + distanceFromLastStopToBottom);
        double numberRadiiInStopColumn = (2 * numberOfStops) + (numberOfStops - 1);
        double radius = heightOfStopColumn/numberRadiiInStopColumn;
        double heightOfCircle = 2 * radius;
        double halfWidthOfView = self.view.frame.size.width/2;
        
        double pi = 3.14159;
        
        
        //Draw circles
        UIBezierPath* path = [UIBezierPath bezierPath];
        
        
        for ( int i = 0; i < numberOfStops; i++ ) {
            [path addArcWithCenter:CGPointMake(halfWidthOfView, distanceFromTopToFirstStop + (radius * ((3*i) + 1))) radius:radius startAngle:-pi/2 endAngle:3*pi/2 clockwise:YES];
            [path moveToPoint:CGPointMake(halfWidthOfView, distanceFromTopToFirstStop + ((i + 1)*3*radius))];
        }
        path.lineWidth = self.circleThickness;
        
        [self.canvas addPath:path withKey:@"circles" strokeColor:[UIColor blackColor] fillColor:[UIColor clearColor]];
        
        //Use circles as mask
        CAShapeLayer *mask = [CAShapeLayer layer];
        mask.path = path.CGPath;
        self.maskLayer.layer.mask = mask;
        
        
        double flagHeightDividedByCircleHeight = 0.42;
        
        for ( int i = 0; i < numberOfStops; i++ ) {
            
            BARDSelectorStop *stop = self.stack[i];
            
            [self.view addSubview:stop];
            [stop changeXValue:halfWidthOfView - radius];
            [stop changeYValue:distanceFromTopToFirstStop + (i*3*radius)];
            [stop changeSideLength:heightOfCircle];
            [stop setIconInset:5];
            
            
            
            double heightOfFlag = heightOfCircle * flagHeightDividedByCircleHeight;
            double widthOfFlag = stop.flagAspectRatio * heightOfFlag;
            double yValueOfFlag = stop.frame.origin.y + ((heightOfCircle - heightOfFlag)/2);
            double fractionOfFlagCoveredByCircle = 0.15;
            double backwardsOffsetForFlag = widthOfFlag * fractionOfFlagCoveredByCircle;
            
            [stop setFlagMaskFrame:CGRectMake(halfWidthOfView + radius - backwardsOffsetForFlag, yValueOfFlag, 0, heightOfFlag)];
            
        }
        
        double roadWidthDividedByCircleHeight = 0.27;
        double widthOfRoad = roadWidthDividedByCircleHeight * heightOfCircle;
        double roadAspectRatio = 3.3;
        
        for ( int i = 0; i < [self.roadViews count]; i++ ) {
            BARDSelectorStop *stop = self.stack[i];
            
            CGRect roadFrame = CGRectMake(halfWidthOfView - widthOfRoad/2, [BARDToolkit bottomOfRect:stop.frame] - 10, widthOfRoad, roadAspectRatio * widthOfRoad);
            [self.roadViews[i] setFrame:roadFrame];
            
        }
    }
    
}



- (void) handleSelection:(CGPoint)location atState:(NSString *)state yDistanceMoved:(double)yDistanceMovedSincePreviousCallToThisMethod controller:(id)scheduleSelector
{
    
    [self.colorFillingTimer invalidate];
    if ( [state isEqualToString:@"began"] ) {
        if ( !self.selectedOrigin ) {
            for ( BARDSelectorStop *stop in self.stack ) {
                if ( [BARDToolkit number:location.y isBetweenFirstNumber:stop.frame.origin.y andSecondNumber:stop.frame.origin.y + stop.frame.size.height] ) {
                    if ( [BARDToolkit number:location.x isBetweenFirstNumber:stop.frame.origin.x andSecondNumber:stop.frame.origin.x + stop.frame.size.width] ) {
                        [self selectOrigin:[BARDToolkit indexOfObject:stop inArray:self.stack]];
                    }
                }
            }
        } else {
            for ( BARDSelectorStop *stop in self.stack ) {
                if ( [BARDToolkit number:location.y isBetweenFirstNumber:stop.frame.origin.y andSecondNumber:stop.frame.origin.y + stop.iconView.frame.size.height] ) {
                    if ( [BARDToolkit indexOfObject:stop inArray:self.stack] != [BARDToolkit indexOfObject:self.selectedOrigin inArray:self.stack] ) {
                        [self selectDestination:[BARDToolkit indexOfObject:stop inArray:self.stack]];
                    }
                }
            }
            if ( self.selectedDestination ) {
                self.colorFillingTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(fillCurrentDestination) userInfo:self repeats:NO];
            }
        }
    } else if ( [state isEqualToString:@"changed"] ) {
        if ( self.selectedDestination ) {
            self.colorFillingTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(fillCurrentDestination) userInfo:self repeats:NO];
        }
        
        if ( self.selectedOrigin ) {
            CGRect selectionFluidFrame = self.selectionFluid.frame;
            
            if ( location.y > [BARDToolkit bottomOfRect:self.selectedOrigin.frame] ) {
                selectionFluidFrame.size.height = location.y - [BARDToolkit topOfRect:self.selectedOrigin.frame];
            } else if ( location.y < [BARDToolkit topOfRect:self.selectedOrigin.frame] ) {
                selectionFluidFrame.origin.y = location.y;
                selectionFluidFrame.size.height = [BARDToolkit bottomOfRect:self.selectedOrigin.frame] - location.y;
            }
            
            [UIView animateWithDuration:0.15 animations:^{
                self.selectionFluid.frame = selectionFluidFrame;
            }];
            
            
            for ( BARDSelectorStop *stop in self.stack ) {
                if ( [BARDToolkit number:location.y isBetweenFirstNumber:[BARDToolkit topOfRect:stop.frame] andSecondNumber:[BARDToolkit bottomOfRect:stop.frame]] ) {
                    
                    if ( stop != self.selectedDestination ) {
                        if ( stop != self.selectedOrigin ) {
                            [self selectDestination:stop.index];
                        } else {
                            [self deselectDestinationAnimated:NO];
                        }
                    }
                }
            }
        } else {
            for ( BARDSelectorStop *stop in self.stack ) {
                if ( [BARDToolkit number:location.y isBetweenFirstNumber:[BARDToolkit topOfRect:stop.frame] andSecondNumber:[BARDToolkit bottomOfRect:stop.frame]] ) {
                    
                    [self selectOrigin:stop.index];
                }
            }
        }
        
        
    } else if ( [state isEqualToString:@"ended"] ) {
        if ( self.selectedOrigin ) {
            if ( self.selectedDestination ) {
                [self fillCurrentDestination];
                [scheduleSelector presentScheduleForOrigin:self.selectedOrigin.stop Destination:self.selectedDestination.stop dayIndex:-1];
            } else {
                if ( self.selectedOrigin.standby ) {
                    if ( [BARDToolkit number:location.y isBetweenFirstNumber:self.selectedOrigin.frame.origin.y andSecondNumber:self.selectedOrigin.frame.origin.y + self.selectedOrigin.frame.size.height] ) {
                        [self deselectOrigin];
                    }
                    
                } else {
                    self.selectedOrigin.standby = YES;
                }
            }
        }
    }
}




- (void) selectOrigin:(int)index
{
    [self deselectOrigin];
    self.selectedOrigin = self.stack[index];
    NSLog(@"Selecting Origin: %@", self.selectedOrigin.stop.title);
    self.selectedOrigin.selected = YES;
    self.selectedOrigin.fullyFilled = YES;
    
    CGRect newFlagMaskFrame = self.selectedOrigin.flagMask.frame;
    newFlagMaskFrame.size.width = newFlagMaskFrame.size.height * self.selectedOrigin.flagAspectRatio;
    
    CGRect colorFrame = CGRectMake(0, self.selectedOrigin.frame.origin.y, screenWidth, self.selectedOrigin.frame.size.height);
    self.selectionFluid.frame = colorFrame;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.selectionFluid.layer.opacity = 1;
        
        self.selectedOrigin.originIndicator.layer.opacity = 1;
    }];
    
    [self.selectedOrigin animateFlagMaskFrame:newFlagMaskFrame];
    
}

- (void) deselectOrigin
{
    if ( self.selectedOrigin ) {
        NSLog(@"Deselecting Origin: %@", self.selectedOrigin.stop.title);
        self.selectedOrigin.selected = NO;
        self.selectedOrigin.standby = NO;
        self.selectedOrigin.fullyFilled = NO;
        
        CGRect newFlagMaskFrame = self.selectedOrigin.flagMask.frame;
        newFlagMaskFrame.size.width = 0;
        
        [self.selectedOrigin animateFlagMaskFrame:newFlagMaskFrame];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.selectionFluid.layer.opacity = 0;
            
        }];
        self.selectedOrigin.originIndicator.layer.opacity = 0;
    }
    
    self.selectedOrigin = nil;
}

- (void) selectDestination:(int)index
{
    [self deselectDestinationAnimated:NO];
    self.selectedDestination = self.stack[index];
    NSLog(@"Selecting Destination: %@", self.selectedDestination.stop.title);
    self.selectedDestination.selected = YES;
    
    CGRect newFlagMaskFrame = self.selectedDestination.flagMask.frame;
    newFlagMaskFrame.size.width = newFlagMaskFrame.size.height * self.selectedDestination.flagAspectRatio;
    
    [self.selectedDestination animateFlagMaskFrame:newFlagMaskFrame];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.selectedDestination.destinationIndicator.layer.opacity = 1;
    }];
}

- (void) deselectDestinationAnimated:(BOOL)animated
{
    if ( self.selectedDestination ) {
        NSLog(@"Deselecting Destination: %@", self.selectedDestination.stop.title);
        self.selectedDestination.selected = NO;
        self.selectedDestination.standby = NO;
        self.selectedDestination.fullyFilled = NO;
        
        CGRect newFlagMaskFrame = self.selectedDestination.flagMask.frame;
        newFlagMaskFrame.size.width = 0;
        
        [self.selectedDestination animateFlagMaskFrame:newFlagMaskFrame];
        
        if ( animated ) {
            [self fillToIndex:[BARDToolkit indexOfObject:self.selectedOrigin inArray:self.stack]];
        }
        
        self.selectedDestination.destinationIndicator.layer.opacity = 0;
    }
    
    self.selectedDestination = nil;
    
}

- (void) fillToIndex:(int)index
{
    BARDSelectorStop *stop = self.stack[index];
    CGRect newSelectionFluidFrame = self.selectionFluid.frame;
    stop.fullyFilled = YES;
    
    double currentHeight = newSelectionFluidFrame.size.height;
    if ( self.selectedOrigin ) {
        if ( [BARDToolkit indexOfObject:self.selectedOrigin inArray:self.stack] < index ) {
            newSelectionFluidFrame.origin.y = self.selectedOrigin.frame.origin.y;
            newSelectionFluidFrame.size.height = stop.frame.origin.y + stop.frame.size.height - self.selectedOrigin.frame.origin.y;
        } else {
            newSelectionFluidFrame.origin.y = stop.frame.origin.y;
            newSelectionFluidFrame.size.height = self.selectedOrigin.frame.origin.y + self.selectedOrigin.frame.size.height - stop.frame.origin.y;
        }
    } else {
        newSelectionFluidFrame.origin.y = stop.frame.origin.y;
        newSelectionFluidFrame.size.height = stop.frame.size.height;
    }
    
    double timeTakenToFill = (1.0/150.0* [BARDToolkit absoluteValue:(newSelectionFluidFrame.size.height - currentHeight)]);
    if ( timeTakenToFill < 0.1 ) {
        timeTakenToFill = 0.1;
    }
    
    [UIView animateWithDuration: 0.25 animations:^{
        self.selectionFluid.frame = newSelectionFluidFrame;
    }];
}

- (void) fillCurrentDestination
{
    [self fillToIndex:self.selectedDestination.index];
}

- (void) animateToYValue:(double)yValue fromTop:(BOOL)fromTop
{
    CGRect newSelectionFluidFrame = self.selectionFluid.frame;
    double currentYValue = newSelectionFluidFrame.origin.y;
    if ( fromTop ) {
        newSelectionFluidFrame.origin.y = yValue;
        newSelectionFluidFrame.size.height += currentYValue - yValue;
    } else {
        newSelectionFluidFrame.size.height += yValue - currentYValue;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        self.selectionFluid.frame = newSelectionFluidFrame;
    }];
}



@end
