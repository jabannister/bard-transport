//
//  BARDToolkit.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/19/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface BARDToolkit : NSObject




+ (BOOL) number: (double) number isBetweenFirstNumber: (double) first andSecondNumber: (double) second;

//Rect Manipulations
+ (BOOL) rect: (CGRect) rect containsPoint: (CGPoint) point;


+ (double) topOfRect: (CGRect) rect;
+ (double) bottomOfRect: (CGRect) rect;
+ (double) leftOfRect: (CGRect) rect;
+ (double) rightOfRect: (CGRect) rect;
+ (CGPoint) centerOfRect: (CGRect) rect;

+ (double) distanceFromPoint: (CGPoint) point toCenterOfRect: (CGRect) rect;

+ (CGRect) slideRect: (CGRect) rect byAmount: (double) distance;
+ (CGRect) raiseRect: (CGRect) rect byAmount: (double) distance;
+ (CGRect) widenRect: (CGRect) rect byAmount: (double) amount;
+ (CGRect) heightenRect: (CGRect) rect byAmount: (double) amount;

+ (CGRect) changeXValueOfRect: (CGRect) rect toValue: (double) xValue;
+ (CGRect) changeYValueOfRect: (CGRect) rect toValue: (double) yValue;
+ (CGRect) changeWidthOfRect: (CGRect) rect toValue: (double) width;
+ (CGRect) changeHeightOfRect: (CGRect) rect toValue: (double) height;


+ (double) distanceBetweenPoint1: (CGPoint) point1 point2: (CGPoint) point2;

+ (double) degreesToRadians: (double) degrees;
+ (double) radiansToDegrees: (double) radians;


+ (double) averageOfX: (double) x AndY: (double) y;

+ (double) absoluteValue: (double) number;

+ (int) roundDownDouble: (double) number;

+ (int) highestPowerOfBase: (int) base thatFitsIntoNumber: (int) number;

+ (double) minimumOfValues: (NSArray*) values;

+ (double) raiseBase: (double) base toPower: (int) power;

+ (NSString*) hexidecimalStringFromInt: (int) number;

+ (NSString*) hexcodeFromR: (int) r G: (int) g B: (int) b;

+ (NSString*) joinArrayOfStrings: (NSArray*) array;

+ (NSString*) capitalizeLetter: (NSString*) letter;

+ (NSString*) capitalizeWords: (NSString*) words;

+ (NSArray*) daysOfWeek;

+ (NSArray*) daysOfWeekCapitalized;

+ (UILabel*) copyLabel: (UILabel*) label;

+ (int) indexOfPointInArray: (NSArray*) points closestToPoint: (CGPoint) point;

+ (double) distanceFromPoint: (CGPoint) point toClosestPointInArray: (NSArray*) points;

+ (int) indexOfLowestNumberInArray: (NSArray*) array;

+ (BOOL) versionNumber: (NSString*) versionNumber1 isLessThanVersionNumber: (NSString*) versionNumber2;

+ (BOOL) array: (NSArray*) array isUniformlyOfType: (NSString*) type;

+ (int) indexOfString: (NSString*) string inArray: (NSArray*) array;

+ (int) indexOfObject: (id) object inArray: (NSArray*) array;

+ (int) indexOfEarliestTimeInArray: (NSArray*) times;

+ (NSMutableArray*) convertTimeStringTo24HourTime: (NSString*) time;

+ (NSMutableArray*) timeIntervalBetweenTime: (NSArray*) time1 andTime: (NSArray*) time2;

+ (NSArray*) daysOfTheWeek;

+ (long) timeIntervalInSecondsBetweenTime: (NSArray*) time1 andTime: (NSArray*) time2;

+ (long) daysSinceJanuaryFirst;

+ (long) currentYear;

+ (long) currentMonth;

+ (long) currentDayOfMonth;

+ (NSString*) currentDayOfWeek;

+ (long) currentHour;

+ (long) currentMinute;

+ (long) currentSecond;

+ (NSArray*) currentTime;

+ (void) logTime;

+ (void) logTimeWithMessage: (NSString*) message;

+ (void) printFrame: (CGRect) frame withTitle: (NSString*) title;


@end


@interface UIColor (BARDToolkit)

+ (UIColor*) colorFromHexString: (NSString*)hex;
- (BOOL) isEqualToColor: (UIColor*) otherColor;

@end

@interface NSArray (SPDeepCopy)

- (NSArray*) deepCopy;
- (NSMutableArray*) mutableDeepCopy;

@end

@interface NSDictionary (SPDeepCopy)

- (NSDictionary*) deepCopy;
- (NSMutableDictionary*) mutableDeepCopy;

@end