//
//  JABGlobalInfo.h
//  JAB2048
//
//  Created by Jeremy Bannister on 3/25/14.
//  Copyright (c) 2014 Jeremy. All rights reserved.
//

extern double screenWidth;
extern double screenHeight;
extern float iOS;

#import <Foundation/Foundation.h>
#include <stdlib.h>

@interface JABGlobalInfo : NSObject


+ (void) getScreenDimensions;
+ (void) getOperatingSystem;

@end
