//
//  BARDSelectorStopStack.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 6/6/14.
//  Copyright (c) 2014 Bard College. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BARDSelectorStop.h"
#import "BARDCanvas.h"
#import "JABGlobalInfo.h"

@class BARDScheduleSelectorViewController;

@interface BARDSelectorStopStack : NSObject

@property (strong, nonatomic) UIView *view;

@property (strong, nonatomic) NSMutableArray *stack;

@property (strong, nonatomic) UIView *maskLayer;
@property (strong, nonatomic) UIView *background;
@property (strong, nonatomic) UIView *selectionFluid;
@property (strong, nonatomic) BARDCanvas *canvas;

@property (nonatomic) double circleThickness;

@property (nonatomic, strong) UIImage *roadIcon;
@property (nonatomic, strong) NSMutableArray *roadViews;

@property (strong, nonatomic) BARDSelectorStop *selectedOrigin;
@property (strong, nonatomic) BARDSelectorStop *selectedDestination;

@property (strong, nonatomic) NSTimer *colorFillingTimer;



- (id) initWithFrame: (CGRect) frame fluidColor: (UIColor*) fluidColor backgroundColor: (UIColor*) backgroundColor roadIcon: (UIImage*) roadIcon;

- (void) addStop: (BARDSelectorStop*) stop;
- (void) insertStop: (BARDSelectorStop*) stop atIndex: (int) index;
- (void) removeStopAtIndex: (int) index;
- (void) removeAllStops;

- (void) drawStops;

- (void) setCircleThickness:(double)circleThickness;


- (void) handleSelection: (CGPoint) location atState: (NSString*) state yDistanceMoved: (double) yDistanceMovedSincePreviousCallToThisMethod controller: (BARDScheduleSelectorViewController*) scheduleSelector;

- (void) selectOrigin: (int) index;
- (void) deselectOrigin;
- (void) selectDestination: (int) index;
- (void) deselectDestinationAnimated: (BOOL) animated;
- (void) fillToIndex: (int) index;
- (void) fillCurrentDestination;
- (void) animateToYValue: (double) yValue fromTop: (BOOL) fromTop;


@end
