//
//  BARDShuttleScheduleViewController.h
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/20/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BARDScheduleSelectorViewController;
@class BARDNavigationController;
@class BARDShuttleStop;


@interface BARDShuttleScheduleViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) BARDScheduleSelectorViewController* controller;

@property (weak, nonatomic) IBOutlet UISegmentedControl* dayToggle;

@property (strong, nonatomic) BARDShuttleStop* origin;
@property (strong, nonatomic) BARDShuttleStop* destination;
@property (strong, nonatomic) UILongPressGestureRecognizer* longPressRecognizer;
@property (strong, nonatomic) NSArray* times;
@property (weak, nonatomic) IBOutlet UITableView* table;
@property (weak, nonatomic) IBOutlet UINavigationBar* bar;
@property (weak, nonatomic) IBOutlet UILabel* desc;
@property (weak, nonatomic) IBOutlet UIButton* doneButton;
@property (nonatomic) int selectedDayIndex;
@property (nonatomic) int nextTimeIndex;

@property (weak, nonatomic) IBOutlet UIView* headerDivider;

@property (nonatomic) double backPanTriggerThresholdTemp;


@property (strong, nonatomic) NSMutableArray* nextTime;


- (IBAction)done:(id)sender;
- (void) checkIfSelectionUpdateNecessary;
- (void) selectNextTime;
- (void) presentNewSchedule;
- (void) resetViewFrame;
- (void) longPressDetected:(UIPanGestureRecognizer*) longPressRecognizer;

@end
