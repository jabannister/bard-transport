//
//  BARDSchedule.m
//  Bard Transportation
//
//  Created by Muhsin King on 10/18/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
// Implemenation, contains main code.

#import "BARDSchedule.h"
#import "BARDShuttleStop.h"

@implementation BARDSchedule
- (id) init{
    self = [super init];
    
    if (self){
        
    }
    
    return self;
    
}

+ (NSMutableArray*) timeTableForDay:(NSString*) day origin: (NSString*) origin destination: (NSString*) destination options:(NSString *)options
{
    
    // order: tivoli robbins kline redhook hannaford
    NSArray* shuttleStopNames = [BARDShuttleStop allShuttleStopNames];
    
    NSString *path;
    if ( [options isEqualToString:@"summer"] ) {
        path = [[NSBundle mainBundle] pathForResource:@"SummerSchedule" ofType:@"plist"];
    } else {
        path = [[NSBundle mainBundle] pathForResource:@"newSchedule" ofType:@"plist"];
    }
    
    NSDictionary *schedule = [NSDictionary dictionaryWithContentsOfFile:path];
    
    int sentinel = 0;
    int originIndex = -1;
    int destinationIndex = -1;
    NSString* originDirection = origin;
    NSString* dayList;
    
    
    // get the day list
    NSDictionary *dayDictionary;
    dayDictionary = @{@"Monday":@"mtw",
                      @"Tuesday":@"mtw",
                      @"Wednesday":@"mtw",
                      @"Thursday":@"thursdayfriday",
                      @"Friday":@"thursdayfriday",
                      @"Saturday":@"saturday",
                      @"Sunday":@"sunday"};
    
    dayList = dayDictionary[day];
    
    // find indices of origin and destination
    NSLog(@"origin is %@", origin);
    while(originIndex == -1 || destinationIndex == -1) {
        if ([origin isEqualToString: shuttleStopNames[sentinel]]){
            originIndex = sentinel;
        }
        if ([destination isEqualToString: shuttleStopNames[sentinel]]){
            destinationIndex = sentinel;
        }
        
        
        NSAssert(sentinel < [shuttleStopNames count],@"Shuttle stop not found in array");
        
        if (sentinel >= [shuttleStopNames count]){
            return [@[] mutableCopy];
        }
        sentinel++;
        
    }
    
    
    
    // convert origin+destination to origin+direction
    if (originIndex < destinationIndex){
        originDirection = [originDirection stringByAppendingString:@"-south"];
        
    } else if (originIndex > destinationIndex){
        originDirection = [originDirection stringByAppendingString:@"-north"];
        
    } else {
        return [@[] mutableCopy];
    }
    NSArray* initialArray = [schedule[dayList][originDirection] mutableCopy];
    NSMutableArray* finalArray = [[NSMutableArray alloc] initWithCapacity:[initialArray count]];
    for ( int i = 0; i < [initialArray count]; i++ ) {
        NSString* time = initialArray[i][@"time"];
        NSString* parameterKey = @"no";
        parameterKey = [parameterKey stringByAppendingString:destination];
        if ( !initialArray[i][parameterKey] ) {
            [finalArray addObject:time];
        }
        
    }
    
    
    return finalArray;
    
}
@end
