//
//  BARDCanvas.m
//  Bard Transportation
//
//  Created by Jeremy Bannister on 10/19/13.
//  Copyright (c) 2013 Bard College. All rights reserved.
//

#import "BARDCanvas.h"

@implementation BARDCanvas

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

*/

- (void)drawRect:(CGRect)rect
{
    NSLog(@"Drawing Rect...");
    
    for (int i = 0; i < [self.keys count]; i++ ) {
        [self.strokeAndFillColorPairs[self.keys[i]][0] setStroke];
        [self.strokeAndFillColorPairs[self.keys[i]][1] setFill];
        [self.paths[self.keys[i]] fill];
        [self.paths[self.keys[i]] stroke];
    }

}


- (void) addPath:(UIBezierPath *)path withKey:(NSString *)key strokeColor:(UIColor *)strokeColor fillColor:(UIColor *)fillColor
{
    if ( !self.paths ) {
        self.paths = [[NSMutableDictionary alloc] init];
    }
    if ( !self.strokeAndFillColorPairs ) {
        self.strokeAndFillColorPairs = [[NSMutableDictionary alloc] init];
    }
    if ( !self.keys ) {
        self.keys = [[NSMutableArray alloc] init];
    }
    [self.paths setObject:path forKey:key];
    [self.strokeAndFillColorPairs setObject: @[strokeColor, fillColor] forKey:key];
    [self.keys addObject:key];
    
}

- (void) addPath:(UIBezierPath *)path withKey:(NSString *)key strokeHex:(NSString *)strokeHex fillHex:(NSString *)fillHex
{
    if ( !self.paths ) {
        self.paths = [[NSMutableDictionary alloc] init];
    }
    if ( !self.strokeAndFillColorPairs ) {
        self.strokeAndFillColorPairs = [[NSMutableDictionary alloc] init];
    }
    if ( !self.keys ) {
        self.keys = [[NSMutableArray alloc] init];
    }
    [self.paths setObject:path forKey:key];
    [self.strokeAndFillColorPairs setObject: @[[UIColor colorFromHexString:strokeHex], [UIColor colorFromHexString:fillHex]] forKey:key];
    [self.keys addObject:key];
    
}


- (void) removeFromCanvasPathWithKey:(NSString *)key
{
    if (self.paths[key]) {
        [self.paths removeObjectForKey:key];
        [self.strokeAndFillColorPairs removeObjectForKey:key];
        for (int i = 0; i < [self.keys count]; i++) {
            if ([[self.keys objectAtIndex:i] isEqualToString:key]) {
                [self.keys removeObjectAtIndex:i];
            }
        }
    }
}


- (void) clearCanvas
{
    self.paths = [[NSMutableDictionary alloc] init];
    self.strokeAndFillColorPairs = [[NSMutableDictionary alloc] init];
}

@end
